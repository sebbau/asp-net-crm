﻿using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.ViewModels.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers
{
    public interface IClientControllerHelper
    {
        NewClientViewModel GetNewClientViewModelAndLogExceptionOnAddMethod(ClientExistsException clientExistsException, ControllerContext controllerContext, ClientSaveDto clientSaveDto);

        IndividualClientEditViewModel GetIndividualClientEditViewModelAndLogExceptionOnUpdateMethod(ClientExistsException clientExistsException, ControllerContext controllerContext, ClientUpdateDto clientUpdateDto);

        BusinessClientEditViewModel GetBusinessClientEditViewModelAndLogExceptionOnUpdateMethod(ClientExistsException clientExistsException, ControllerContext controllerContext, ClientUpdateDto clientUpdateDto);
    }
}
