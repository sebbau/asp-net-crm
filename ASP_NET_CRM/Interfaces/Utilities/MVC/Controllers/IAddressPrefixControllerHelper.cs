﻿using ASP_NET_CRM.DTO.AddressPrefix;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.ViewModels.AddressPrefixes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers
{
    public interface IAddressPrefixControllerHelper
    {
        void LogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext);

        Task<AddressPrefixEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(AddressPrefixUpdateDto addressPrefixUpdateDto);
    }
}
