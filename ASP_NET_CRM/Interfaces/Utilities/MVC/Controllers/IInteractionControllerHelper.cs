﻿using ASP_NET_CRM.DTO.Interaction;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.ViewModels.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASP_NET_CRM.Interfaces.Utilities.Controllers
{
    public interface IInteractionControllerHelper
    {
        Task<NewInteractionViewModel> GetNewInteractionViewModelWhenModelStateIsInvalidForAddMethod(InteractionSaveDto interactionSaveDto);

        Task<NewInteractionViewModel> GetNewInteractionViewModelAndLogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext, InteractionSaveDto interactionSaveDto);

        Task<InteractionEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(InteractionUpdateDto interactionUpdateDto);
    }
}
