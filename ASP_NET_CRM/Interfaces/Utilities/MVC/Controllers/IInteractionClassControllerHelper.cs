﻿using ASP_NET_CRM.DTO.InteractionClass;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.InteractionClass;
using ASP_NET_CRM.ViewModels.InteractionClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers
{
    public interface IInteractionClassControllerHelper
    {
        Task<NewInteractionClassViewModel> GetViewModelWhenModelStateIsInvalidForAddMethod();

        Task<NewInteractionClassViewModel> GetViewModelAndLogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext);

        Task<InteractionClassEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(InteractionClassUpdateDto interactionClassUpdateDto);

        Task<InteractionClassEditViewModel> GetViewModelAndLogExceptionForUpdateMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext, InteractionClassUpdateDto interactionClassUpdateDto);
    }
}
