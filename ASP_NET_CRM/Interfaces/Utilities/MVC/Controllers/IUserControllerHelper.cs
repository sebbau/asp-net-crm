﻿using ASP_NET_CRM.DTO.User;
using ASP_NET_CRM.ViewModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers
{
    public interface IUserControllerHelper
    {
        Task<UserEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(UserUpdateDto userUpdateDto);
    }
}
