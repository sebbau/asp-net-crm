﻿using ASP_NET_CRM.DTO.Consent;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Consent;
using ASP_NET_CRM.ViewModels.Consents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers
{
    public interface IConsentControllerHelper
    {
        void LogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext);

        Task<ConsentEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(ConsentUpdateDto consentUpdateDto);

        ConsentEditViewModel GetViewModelAndLogExceptionForUpdateMethod(ConsentExistsException consentExistsException, ControllerContext controllerContext, ConsentUpdateDto consentUpdateDto);
    }
}
