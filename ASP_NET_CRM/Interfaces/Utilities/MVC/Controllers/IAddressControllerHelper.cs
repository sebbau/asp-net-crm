﻿using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.ViewModels.Addresses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers
{
    public interface IAddressControllerHelper
    {
        Task<NewAddressViewModel> GetNewAddressViewModelWhenModelStateIsInvalidForAddMethod(AddressSaveDto addressSaveDto);

        Task<NewAddressViewModel> GetNewAddressViewModelAndLogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext, AddressSaveDto interactionSaveDto);

        Task<AddressEditViewModel> GetAddressEditViewModelWhenModelStateIsInvalidForUpdateMethod(AddressUpdateDto addressUpdateDto);
    }
}
