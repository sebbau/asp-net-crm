﻿using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.ViewModels.Notes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers
{
    public interface INoteControllerHelper
    {
        Task<NewNoteViewModel> GetNewNoteViewModelWhenModelStateIsInvalidForAddMethod(NoteSaveDto noteSaveDto);

        Task<NoteEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(NoteUpdateDto noteUpdateDto);
    }
}
