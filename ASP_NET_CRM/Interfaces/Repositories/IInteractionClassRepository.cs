﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Repositories
{
    public interface IInteractionClassRepository : IRepository<InteractionClass>
    {
    }
}
