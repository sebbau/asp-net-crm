﻿using ASP_NET_CRM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<ApplicationUser>> GetRecordsWithPaginationAndSearchAsync(Expression<Func<ApplicationUser, string>> orderPredicate, Expression<Func<ApplicationUser, bool>> searchPredicate, int recordsToSkip, int recordsToTake);

        Task<IEnumerable<ApplicationUser>> GetRecordsWithPaginationAsync(Expression<Func<ApplicationUser, string>> predicate, int recordsToSkip, int recordsToTake);

        Task<ApplicationUser> GetUserWithIncludesAsync(Expression<Func<ApplicationUser, bool>> predicate, params Expression<Func<ApplicationUser, object>>[] includes);
    }
}
