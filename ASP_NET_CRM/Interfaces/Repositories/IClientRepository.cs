﻿using ASP_NET_CRM.Models.Entities;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Repositories
{
    public interface IClientRepository : IRepository<Client>
    {
    }
}