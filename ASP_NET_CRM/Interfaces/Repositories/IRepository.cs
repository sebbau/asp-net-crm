﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Repositories
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        Task<TEntity> GetEntityAsync(int entityId);

        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        Task<IEnumerable<TEntity>> GetAllAsync();

        Task<IEnumerable<TEntity>> GetAllWhereAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> GetEntityWithIncludesAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);

        Task<IEnumerable<TEntity>> GetRecordsWithPaginationAndSearchAsync(Expression<Func<TEntity, int>> orderPredicate, Expression<Func<TEntity, bool>> searchPredicate, int recordsToSkip, int recordsToTake);

        Task<IEnumerable<TEntity>> GetRecordsWithPaginationAndSearchAsync(Expression<Func<TEntity, int>> orderPredicate, Expression<Func<TEntity, bool>> searchPredicate, int recordsToSkip, int recordsToTake, params Expression<Func<TEntity, object>>[] includes);

        Task<IEnumerable<TEntity>> GetRecordsWithPaginationAsync(Expression<Func<TEntity, int>> predicate, int recordsToSkip, int recordsToTake);

        Task<IEnumerable<TEntity>> GetRecordsWithPaginationAsync(Expression<Func<TEntity, int>> predicate, int recordsToSkip, int recordsToTake, params Expression<Func<TEntity, object>>[] includes);

        Task<IEnumerable<TEntity>> GetRecordsWithPaginationAndOrderByDescendingAsync(Expression<Func<TEntity, int>> predicate, int recordsToSkip, int recordsToTake);

        Task<IEnumerable<TEntity>> GetRecordsWithWhereAndPaginationAndOrderByDescendingAsync(Expression<Func<TEntity, bool>> searchPredicate, Expression<Func<TEntity, int>> orderPredicate, int recordsToSkip, int recordsToTake);

        void Add(TEntity entity);

        void AddRange(IEnumerable<TEntity> entities);

        void Remove(TEntity entity);

        void RemoveRange(IEnumerable<TEntity> entities);
    }
}
