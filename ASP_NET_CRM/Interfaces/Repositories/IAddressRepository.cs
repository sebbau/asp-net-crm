﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Repositories
{
    public interface IAddressRepository : IRepository<Address>
    {
        IQueryable<Address> GetAddresses();

        Task<IEnumerable<Address>> GetAddressWithPaginationAndSearchAsync(IQueryable<Address> addressQuery, Expression<Func<Address, int>> orderPredicate, int recordsToSkip, int recordsToTake, params Expression<Func<Address, object>>[] includes);
    }
}
