﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Repositories
{
    public interface IRoleRepository
    {
        Task<IEnumerable<IdentityRole>> GetAllRolesAsync();

        Task<IEnumerable<IdentityRole>> GetRecordsWithPaginationAsync(Expression<Func<IdentityRole, string>> orderPredicate, int recordsToSkip, int recordsToTake);

        Task<IdentityRole> GetRoleAsync(string roleId);
    }
}
