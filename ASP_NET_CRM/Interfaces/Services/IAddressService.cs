﻿using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IAddressService
    {
        Task<IEnumerable<AddressDto>> GetAddressesAsync(int recordsToSkip, int recordsToTake, AddressPagination addressPagination, params Expression<Func<Address, object>>[] includes);

        Task<IEnumerable<AddressDto>> GetAddressesForPaginationAsync(AddressPagination addressPagination, params Expression<Func<Address, object>>[] includes);

        Task<IEnumerable<AddressDto>> GetAddressesByClientIdOrderByDescendingAsync(int clientId, int recordsToSkip, int recordsToTake);

        Task<IEnumerable<AddressDto>> GetAddressesByClientIdOrderByDescendingForPaginationAsync(int clientId, int currentPage, int recordsPerPage);

        Task<AddressDto> GetAddressWithIncludesAsync(Expression<Func<Address, bool>> predicate, params Expression<Func<Address, object>>[] includes);

        Task<AddressDto> GetAddressWithPredicate(Expression<Func<Address, bool>> predicate);

        Task<AddressDto> AddAddressAsync(AddressSaveDto addressSaveDto, string userFullName);

        Task<AddressDto> UpdateAddressAsync(AddressUpdateDto addressUpdateDto, string userFullName);

        Task<AddressDto> DeleteAddressAsync(int addressId);
    }
}
