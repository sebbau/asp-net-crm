﻿using ASP_NET_CRM.DTO.InteractionClass;
using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IInteractionClassService
    {
        Task<IEnumerable<InteractionClassDto>> GetAllInteractionClassesAsync();

        Task<IEnumerable<InteractionClassDto>> GetInteractionClassesForInteractionTypeAsync(int interactionTypeId);

        Task<IEnumerable<InteractionClassDto>> GetInteractionClassesAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy, params Expression<Func<InteractionClass, object>>[] includes);

        Task<IEnumerable<InteractionClassDto>> GetInteractionClassesForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy, params Expression<Func<InteractionClass, object>>[] includes);

        Task<InteractionClassDto> GetInteractionClassWithIncludesAsync(Expression<Func<InteractionClass, bool>> predicate, params Expression<Func<InteractionClass, object>>[] includes);

        Task<InteractionClassDto> AddInteractionClassAsync(InteractionClassSaveDto interactionClassSaveDto, string userFullName);

        Task<InteractionClassDto> UpdateInteractionClassAsync(InteractionClassUpdateDto interactionClassUpdateDto, string userFullName);

        Task<InteractionClassDto> DeleteInteractionClassAsync(int interactionClassId);
    }
}
