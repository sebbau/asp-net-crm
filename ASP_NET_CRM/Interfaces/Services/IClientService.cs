﻿using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IClientService
    {
        Task<IEnumerable<ClientDto>> GetClientsAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy);

        Task<ClientDto> GetClientWithIncludesAsync(Expression<Func<Client, bool>> predicate, params Expression<Func<Client, object>>[] includes);

        Task<IEnumerable<ClientDto>> GetClientsForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy);

        Task<ClientDto> AddClientAsync(ClientSaveDto clientSaveDto, string userFullName);

        Task<ClientDto> GetClientAsync(int id);

        Task<ClientDto> UpdateClientAsync(ClientUpdateDto clientUpdateDto, string userFullName);

        Task<ClientDto> DeleteClientAsync(int clientId);

    }
}
