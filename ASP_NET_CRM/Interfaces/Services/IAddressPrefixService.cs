﻿using ASP_NET_CRM.DTO.AddressPrefix;
using Microsoft.Owin.BuilderProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IAddressPrefixService
    {
        Task<IEnumerable<AddressPrefixDto>> GetAllAddressPrefixes();

        Task<IEnumerable<AddressPrefixDto>> GetAddressPrefixesAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy);

        Task<IEnumerable<AddressPrefixDto>> GetAddressPrefixesForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy);

        Task<AddressPrefixDto> GetAddressPrefixAsync(int addressPrefixId);

        Task<AddressPrefixDto> AddAddressPrefixAsync(AddressPrefixSaveDto addressPrefixSaveDto, string userFullName);

        Task<AddressPrefixDto> UpdateAddressPrefixAsync(AddressPrefixUpdateDto addressPrefixUpdateDto, string userFullName);

        Task<AddressPrefixDto> DeleteAddressPrefixAsync(int addressPrefixId);
    }
}
