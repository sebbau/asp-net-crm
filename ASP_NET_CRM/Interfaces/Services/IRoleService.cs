﻿using ASP_NET_CRM.DTO.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IRoleService
    {
        Task<IEnumerable<RoleDto>> GetAllRolesAsync();

        Task<IEnumerable<RoleDto>> GetRolesAsync(int recordsToSkip, int recordsToTake);

        Task<IEnumerable<RoleDto>> GetRolesForPaginationAsync(int currentPage, int recordsPerPage);

        Task<RoleDto> GetRoleAsync(string roleId);
    }
}
