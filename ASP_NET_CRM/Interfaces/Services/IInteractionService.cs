﻿using ASP_NET_CRM.DTO.Interaction;
using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IInteractionService
    {
        Task<IEnumerable<InteractionDto>> GetInteractionsAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy, params Expression<Func<Interaction, object>>[] includes);

        Task<IEnumerable<InteractionDto>> GetInteractionsByClientIdAndOrderByDescendingAsync(int clientId, int recordsToSkip, int recordsToTake);

        Task<IEnumerable<InteractionDto>> GetInteractionsForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy, params Expression<Func<Interaction, object>>[] includes);

        Task<IEnumerable<InteractionDto>> GetInteractionsByClientIdAndOrderByDescendingForPaginationAsync(int clientId, int currentPage, int recordsPerPage);

        Task<InteractionDto> GetInteractionWithIncludesAsync(Expression<Func<Interaction, bool>> predicate, params Expression<Func<Interaction, object>>[] includes);

        Task<InteractionDto> AddInteractionAsync(InteractionSaveDto interactionSaveDto, string userFullName);

        Task<InteractionDto> UpdateInteractionAsync(InteractionUpdateDto interactionUpdateDto, string userFullName);

        Task<InteractionDto> DeleteInteractionAsync(int interactionId);


    }
}
