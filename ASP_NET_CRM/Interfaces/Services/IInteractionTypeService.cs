﻿using ASP_NET_CRM.DTO.InteractionType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IInteractionTypeService
    {
        public Task<IEnumerable<InteractionTypeDto>> GetAllInteractionTypesAsync();

        Task<IEnumerable<InteractionTypeDto>> GetInteractionTypesAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy);

        Task<IEnumerable<InteractionTypeDto>> GetInteractionTypesForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy);

        Task<InteractionTypeDto> GetInteractionTypeAsync(int interactionTypeId);

        Task<InteractionTypeDto> AddInteractionTypeAsync(InteractionTypeSaveDto interactionTypeSaveDto, string userFullName);

        Task<InteractionTypeDto> UpdateInteractionTypeAsync(InteractionTypeUpdateDto interactionTypeUpdateDto, string userFullName);

        Task<InteractionTypeDto> DeleteInteractionTypeAsync(int interactionTypeId);
    }
}
