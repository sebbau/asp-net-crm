﻿using ASP_NET_CRM.DTO.Consent;
using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IConsentService
    {
        Task<IEnumerable<ConsentDto>> GetAllConsents();

        Task<IEnumerable<ConsentDto>> GetConsentsAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy);

        Task<IEnumerable<ConsentDto>> GetConsentsForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy);

        Task<ConsentDto> GetConsentAsync(int consentId);

        Task<ConsentDto> AddConsentAsync(ConsentSaveDto consentSaveDto, string userFullName);

        Task<ConsentDto> UpdateConsentAsync(ConsentUpdateDto consentUpdateDto, string userFullName);

        Task<ConsentDto> DeleteConsentAsync(int consentId);

        Task<IEnumerable<ConsentDto>> UpdateConsentsForClient(int clientId, IEnumerable<ConsentClientUpdateDto> clientSelectedConsents);
    }
}
