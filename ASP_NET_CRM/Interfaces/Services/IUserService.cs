﻿using ASP_NET_CRM.DTO.Role;
using ASP_NET_CRM.DTO.User;
using ASP_NET_CRM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> GetUsersAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy);

        Task<IEnumerable<UserDto>> GetUsersForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy);

        Task<UserDto> GetUserAsync(string userId);

        Task<UserDto> GetUserWithIncludesAsync(Expression<Func<ApplicationUser, bool>> predicate, params Expression<Func<ApplicationUser, object>>[] includes);

        Task<UserDto> UpdateUserAsync(UserUpdateDto userUpdateDto, string userFullName);

        Task<UserDto> DeleteUserAsync(string userId);

        Task<IEnumerable<RoleDto>> UpdateUserRolesAsync(string userId, IEnumerable<UserRoleUpdateDto> selectedRoles);
    }
}
