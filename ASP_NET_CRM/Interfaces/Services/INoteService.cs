﻿using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface INoteService
    {
        Task<IEnumerable<NoteDto>> GetNotesAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy, params Expression<Func<Note, object>>[] includes);

        Task<IEnumerable<NoteDto>> GetNotesForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy, params Expression<Func<Note, object>>[] includes);

        Task<IEnumerable<NoteDto>> GetNotesByClientIdOrderByDescendingAsync(int clientId, int recordsToSkip, int recordsToTake);

        Task<IEnumerable<NoteDto>> GetNotesByClientIdOrderByDescendingForPaginationAsync(int clientId, int currentPage, int recordsPerPage);

        Task<NoteDto> GetNoteWithIncludesAsync(Expression<Func<Note, bool>> predicate, params Expression<Func<Note, object>>[] includes);

        Task<NoteDto> AddNoteAsync(NoteSaveDto noteSaveDto, string userFullName);

        Task<NoteDto> UpdateNoteAsync(NoteUpdateDto noteUpdateDto, string userFullName);

        Task<NoteDto> DeleteNoteAsync(int noteId);
    }
}
