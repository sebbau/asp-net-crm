﻿using ASP_NET_CRM.DTO.ApplicationError;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.Services
{
    public interface IApplicationErrorService
    {
        Task<IEnumerable<ApplicationErrorDto>> GetApplicationErrorsAsync(int recordsToSkip, int recordsToTake);

        Task<IEnumerable<ApplicationErrorDto>> GetApplicationErrorsForPaginationAsync(int currentPage, int recordsPerPage);

        Task<ApplicationErrorDto> GetApplicationErrorAsync(int applicationErrorId);

        void AddApplicationError(ApplicationErrorSaveDto applicationErrorSaveDto, string userFullName);
    }
}
