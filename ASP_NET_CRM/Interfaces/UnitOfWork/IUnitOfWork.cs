﻿using ASP_NET_CRM.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Interfaces.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IClientRepository Clients { get; }

        IConsentRepository Consents { get; }

        IInteractionRepository Interactions { get; }

        IInteractionTypeRepository InteractionTypes { get; }

        IInteractionClassRepository InteractionClasses { get; }

        IApplicationErrorRepository ApplicationErrors { get; }

        IAddressPrefixRepository AddressPrefixes { get; }

        IAddressRepository Addresses { get; }

        INoteRepository Notes { get; }

        IRoleRepository Roles { get; }

        IUserRepository Users { get; }

        Task<int> SaveChanges();
    }
}
