﻿using ASP_NET_CRM.Constants.Alerts.AddressPrefix;
using ASP_NET_CRM.Constants.Exceptions.Models.AddressPrefix;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.AddressPrefix;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.AddressPrefix;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.AddressPrefixes;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("AddressPrefix")]
    public class AddressPrefixController : Controller
    {
        private readonly IAddressPrefixService _addressPrefixService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IAddressPrefixControllerHelper _addressPrefixControllerHelper;
        private readonly IMapper _mapper;

        public AddressPrefixController(
            IAddressPrefixService addressPrefixService,
            IApplicationErrorService applicationErrorService,
            IAddressPrefixControllerHelper addressPrefixControllerHelper,
            IMapper mapper
            )
        {
            _addressPrefixService = addressPrefixService;
            _applicationErrorService = applicationErrorService;
            _addressPrefixControllerHelper = addressPrefixControllerHelper;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(PaginationExtended paginationRequest)
        {
            try
            {
                paginationRequest.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationRequest.RecordsPerPage);

                IEnumerable<AddressPrefixDto> addressPrefixDtos = await _addressPrefixService.GetAddressPrefixesAsync(
                    paginationRequest.CurrentPage * paginationRequest.RecordsPerPage - paginationRequest.RecordsPerPage,
                    paginationRequest.RecordsPerPage,
                    paginationRequest.SearchString,
                    paginationRequest.SearchBy
                );

                IEnumerable<AddressPrefixDto> addressPrefixDtosForPagination = await _addressPrefixService.GetAddressPrefixesForPaginationAsync(
                    paginationRequest.CurrentPage,
                    paginationRequest.RecordsPerPage,
                    paginationRequest.SearchString,
                    paginationRequest.SearchBy
                );

                Pagination paginationBase = PagingUtilities.Pagination(paginationRequest.CurrentPage, paginationRequest.RecordsPerPage, addressPrefixDtos, addressPrefixDtosForPagination);

                AddressPrefixIndexViewModel viewModel = new AddressPrefixIndexViewModel
                {
                    AddressPrefixes = addressPrefixDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(paginationBase, paginationRequest)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(int id)
        {
            try
            {
                AddressPrefixDto addressPrefixDto = await _addressPrefixService.GetAddressPrefixAsync(id);

                AddressPrefixDetailsViewModel viewModel = _mapper.Map<AddressPrefixDto, AddressPrefixDetailsViewModel>(addressPrefixDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("New")]
        public ActionResult NewView()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddressPrefixSaveDto addressPrefixSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View("NewView");

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                AddressPrefixDto addressPrefixDto = await _addressPrefixService.AddAddressPrefixAsync(addressPrefixSaveDto, userFullName);

                TempData["Success"] = AddressPrefixAlertMessages.AddressPrefixAddSuccessMessage;

                return RedirectToAction("DetailsView", new { id = addressPrefixDto.Id });
            }
            catch (AddressPrefixExistsException exception)
            {
                ModelState.AddModelError(AddressPrefixExceptionMessages.AddressPrefixKey, exception.Message);

                _addressPrefixControllerHelper.LogExceptionForAddMethod(exception, this.ControllerContext);

                return View("NewView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<ActionResult> EditView(int id)
        {
            try
            {
                AddressPrefixDto addressPrefixDto = await _addressPrefixService.GetAddressPrefixAsync(id);

                AddressPrefixEditViewModel viewModel = _mapper.Map<AddressPrefixDto, AddressPrefixEditViewModel>(addressPrefixDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(AddressPrefixUpdateDto addressPrefixUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    AddressPrefixEditViewModel viewModel = await _addressPrefixControllerHelper.GetViewModelWhenModelStateIsInvalidForUpdateMethod(addressPrefixUpdateDto);

                    return View("EditView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                AddressPrefixDto addressPrefixDto = await _addressPrefixService.UpdateAddressPrefixAsync(addressPrefixUpdateDto, userFullName);

                TempData["Success"] = AddressPrefixAlertMessages.AddressPrefixUpdateSuccessMessage;

                return RedirectToAction("DetailsView", new { id = addressPrefixDto.Id });
            }
            catch (AddressPrefixExistsException exception)
            {
                ModelState.AddModelError(AddressPrefixExceptionMessages.AddressPrefixKey, exception.Message);

                _addressPrefixControllerHelper.LogExceptionForAddMethod(exception, this.ControllerContext);

                AddressPrefixEditViewModel viewModel = new AddressPrefixEditViewModel { Id = addressPrefixUpdateDto.Id };
                
                return View("EditView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                AddressPrefixDto addressPrefixDto = await _addressPrefixService.DeleteAddressPrefixAsync(id);

                TempData["Success"] = AddressPrefixAlertMessages.AddressPrefixDeleteSuccessMessage;

                return RedirectToAction("IndexView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}