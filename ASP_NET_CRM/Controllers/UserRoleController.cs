﻿using ASP_NET_CRM.Constants.Exceptions.Controllers;
using ASP_NET_CRM.Constants.Exceptions.Models.User;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Role;
using ASP_NET_CRM.DTO.User;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Role;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.Users;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ASP_NET_CRM.Constants.Alerts.UserRole;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("UserRole")]
    public class UserRoleController : Controller
    {
        private readonly IUserService _userService;
        private readonly IRoleService _roleService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public UserRoleController(
            IUserService userService,
            IRoleService roleService,
            IApplicationErrorService applicationErrorService,
            IMapper mapper
            )
        {
            _userService = userService;
            _roleService = roleService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(PaginationExtended paginationExtended)
        {
            try
            {
                paginationExtended.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationExtended.RecordsPerPage);

                IEnumerable<UserDto> userDtos = await _userService.GetUsersAsync(
                    paginationExtended.CurrentPage * paginationExtended.RecordsPerPage - paginationExtended.RecordsPerPage,
                    paginationExtended.RecordsPerPage,
                    paginationExtended.SearchString,
                    paginationExtended.SearchBy
                );

                IEnumerable<UserDto> userDtosForPagination = await _userService.GetUsersForPaginationAsync(
                    paginationExtended.CurrentPage,
                    paginationExtended.RecordsPerPage,
                    paginationExtended.SearchString,
                    paginationExtended.SearchBy
                );

                Pagination pagination = PagingUtilities.Pagination(paginationExtended.CurrentPage, paginationExtended.RecordsPerPage, userDtos, userDtosForPagination);

                UserIndexViewModel viewModel = new UserIndexViewModel
                {
                    Users = userDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(pagination, paginationExtended)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("User/{id}")]
        public async Task<ActionResult> DetailsView(string id)
        {
            try
            {
                UserDto userDto = await _userService.GetUserWithIncludesAsync(u => u.Id.Equals(id), u => u.Roles);

                IEnumerable<RoleDto> roleDtos = await _roleService.GetAllRolesAsync();

                UserRolesDetailsViewModel viewModel = _mapper.Map<UserDto, UserRolesDetailsViewModel>(userDto);
                viewModel.Roles = roleDtos;

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("User/Edit/{id}")]
        public async Task<ActionResult> EditView(string id)
        {
            try
            {
                UserDto userDto = await _userService.GetUserWithIncludesAsync(u => u.Id.Equals(id), u => u.Roles);

                List<RoleDto> roleDtos = (List<RoleDto>)await _roleService.GetAllRolesAsync();

                UserRolesEditViewModel viewModel = _mapper.Map<UserDto, UserRolesEditViewModel>(userDto);
                viewModel.Roles = _mapper.Map<List<RoleDto>, List<UserRoleUpdateDto>>(roleDtos);

                if (viewModel.Roles.Count() == 0)
                    return View(viewModel);

                foreach (var role in viewModel.Roles)
                {
                    if (userDto.RolesId.Any(r => r.Equals(role.Id)))
                        role.Selected = true;
                }

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateUserRoles(string id, IEnumerable<UserRoleUpdateDto> roles)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new UserRoleInvalidException(UserExceptionMessages.UserRolesInvalidExceptionMessage, ControllerNameConstants.UserRoleController, ControllerActionNameConstants.UserRoleControllerUpdateUserRoles);

                IEnumerable<UserRoleUpdateDto> selectedRoles = roles.Where(r => r.Selected == true);

                IEnumerable<RoleDto> roleDtos = await _userService.UpdateUserRolesAsync(id, selectedRoles);

                TempData["Success"] = UserRoleAlertMessages.UserRolesUpdateSuccessMessage;

                return RedirectToAction("DetailsView", new { id = id });
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}