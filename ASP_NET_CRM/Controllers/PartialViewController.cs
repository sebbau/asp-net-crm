﻿using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    public class PartialViewController : Controller
    {
        public ActionResult GetPartialView(string partialViewName)
        {
            return PartialView(partialViewName);
        }
    }
}