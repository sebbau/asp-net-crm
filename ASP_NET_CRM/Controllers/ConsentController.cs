﻿using ASP_NET_CRM.Constants.Alerts.Consent;
using ASP_NET_CRM.Constants.Exceptions.Controllers;
using ASP_NET_CRM.Constants.Exceptions.Models.Consent;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.DTO.Consent;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Consent;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.Consents;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("Consent")]
    public class ConsentController : Controller
    {
        private readonly IConsentService _consentService;
        private readonly IClientService _clientService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IConsentControllerHelper _consentControllerHelper;
        private readonly IMapper _mapper;

        public ConsentController(
            IConsentService consentService,
            IClientService clientService,
            IApplicationErrorService applicationErrorService,
            IConsentControllerHelper consentControllerHelper,
            IMapper mapper
            )
        {
            _consentService = consentService;
            _clientService = clientService;
            _applicationErrorService = applicationErrorService;
            _consentControllerHelper = consentControllerHelper;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(PaginationExtended paginationRequest)
        {
            try
            {
                paginationRequest.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationRequest.RecordsPerPage);

                IEnumerable<ConsentDto> consentDtos = await _consentService.GetConsentsAsync(
                    paginationRequest.CurrentPage * paginationRequest.RecordsPerPage - paginationRequest.RecordsPerPage,
                    paginationRequest.RecordsPerPage,
                    paginationRequest.SearchString,
                    paginationRequest.SearchBy
                );

                IEnumerable<ConsentDto> consentDtosForPagination = await _consentService.GetConsentsForPaginationAsync(
                    paginationRequest.CurrentPage,
                    paginationRequest.RecordsPerPage,
                    paginationRequest.SearchString,
                    paginationRequest.SearchBy
                );

                Pagination paginationBase = PagingUtilities.Pagination(paginationRequest.CurrentPage, paginationRequest.RecordsPerPage, consentDtos, consentDtosForPagination);

                ConsentIndexViewModel viewModel = new ConsentIndexViewModel
                {
                    Consents = consentDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(paginationBase, paginationRequest)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(int id)
        {
            try
            {
                ConsentDto consentDto = await _consentService.GetConsentAsync(id);

                ConsentDetailsViewModel viewModel = _mapper.Map<ConsentDto, ConsentDetailsViewModel>(consentDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("New")]
        public ActionResult NewView()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(ConsentSaveDto consentSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View("NewView");

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ConsentDto consentDto = await _consentService.AddConsentAsync(consentSaveDto, userFullName);

                TempData["Success"] = ConsentAlertMessages.ConsentAddSuccessMessage;

                return RedirectToAction("DetailsView", new { id = consentDto.Id });
            }
            catch (ConsentExistsException exception)
            {
                ModelState.AddModelError(ConsentExceptionMessages.ConsentKey, exception.Message);

                _consentControllerHelper.LogExceptionForAddMethod(exception, this.ControllerContext);

                return View("NewView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<ActionResult> EditView(int id)
        {
            try
            {
                ConsentDto consentDto = await _consentService.GetConsentAsync(id);

                ConsentEditViewModel viewModel = _mapper.Map<ConsentDto, ConsentEditViewModel>(consentDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(ConsentUpdateDto consentUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ConsentEditViewModel viewModel = await _consentControllerHelper.GetViewModelWhenModelStateIsInvalidForUpdateMethod(consentUpdateDto);

                    return View("EditView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ConsentDto consentDto = await _consentService.UpdateConsentAsync(consentUpdateDto, userFullName);

                TempData["Success"] = ConsentAlertMessages.consentsUpdateSuccessMessage;

                return RedirectToAction("DetailsView", new { id = consentDto.Id });
            }
            catch (ConsentExistsException exception)
            {
                ModelState.AddModelError(ConsentExceptionMessages.ConsentKey, exception.Message);

                ConsentEditViewModel viewModel = _consentControllerHelper.GetViewModelAndLogExceptionForUpdateMethod(exception, this.ControllerContext, consentUpdateDto);
                
                return View("EditView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                ConsentDto consentDto = await _consentService.DeleteConsentAsync(id);

                TempData["Success"] = ConsentAlertMessages.ConsentDeleteSuccessMessage;

                return RedirectToAction("IndexView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("Client/{clientId}")]
        public async Task<ActionResult> GetConsentsForClientView(int clientId)
        {
            try
            {
                IEnumerable<ConsentDto> consents = await _consentService.GetAllConsents();

                ClientDto client = await _clientService.GetClientWithIncludesAsync(c => c.Id == clientId, c => c.Consents);

                ConsentsClientDetailsViewModel viewModel = _mapper.Map<ClientDto, ConsentsClientDetailsViewModel>(client);

                viewModel.Consents = consents;

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("Client/Edit/{clientId}")]
        public async Task<ActionResult> EditConsentsForClientView(int clientId)
        {
            try
            {
                List<ConsentDto> consents = (List<ConsentDto>) await _consentService.GetAllConsents();

                ClientDto client = await _clientService.GetClientWithIncludesAsync(c => c.Id == clientId, c => c.Consents);

                ConsentsClientEditViewModel viewModel = _mapper.Map<ClientDto, ConsentsClientEditViewModel>(client);
                viewModel.Consents = _mapper.Map<List<ConsentDto>, List<ConsentClientUpdateDto>>(consents);

                if (viewModel.Consents.Count() == 0)
                    return View(viewModel);

                foreach (var consent in viewModel.Consents)
                {
                    if (client.Consents.Any(c => c.Id == consent.Id))
                        consent.Selected = true;
                }

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        public async Task<ActionResult> UpdateConsentsForClient(int id, IEnumerable<ConsentClientUpdateDto> consents)
        {
            try
            {
                if (!ModelState.IsValid)
                    throw new ConsentInvalidException(ConsentExceptionMessages.ConsentInvalidExceptionMessage, ControllerNameConstants.ConsentController, ControllerActionNameConstants.ConsentControllerUpdateConsentsForClient);

                IEnumerable<ConsentClientUpdateDto> clientSelectedConsents = consents.Where(c => c.Selected == true);

                IEnumerable<ConsentDto> clientConsentsUpdate = await _consentService.UpdateConsentsForClient(id, clientSelectedConsents);

                TempData["Success"] = ConsentAlertMessages.consentsUpdateSuccessMessage;
                return RedirectToAction("GetConsentsForClientView", new { clientId = id });
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}