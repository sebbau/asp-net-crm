﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.Constants.Alerts.Interaction;
using ASP_NET_CRM.Constants.Exceptions.Controllers;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Exceptions.Models.Interaction;
using ASP_NET_CRM.Constants.Exceptions.Models.InteractionClass;
using ASP_NET_CRM.Constants.Exceptions.Models.InteractionType;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.DTO.Interaction;
using ASP_NET_CRM.DTO.InteractionType;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Exceptions.Interaction;
using ASP_NET_CRM.Exceptions.InteractionClass;
using ASP_NET_CRM.Exceptions.InteractionType;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.Controllers;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.Interactions;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("Interaction")]
    public class InteractionController : Controller
    {
        private readonly IInteractionService _interactionService;
        private readonly IClientService _clientService;
        private readonly IInteractionTypeService _interactionTypeService;
        private readonly IInteractionControllerHelper _interactionControllerHelper;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public InteractionController(
            IInteractionService interactionService, 
            IClientService clientService, 
            IInteractionTypeService interactionTypeService,
            IInteractionControllerHelper interactionControllerHelper,
            IApplicationErrorService applicationErrorService,
            IMapper mapper
        )
        {
            _interactionService = interactionService;
            _clientService = clientService;
            _interactionTypeService = interactionTypeService;
            _interactionControllerHelper = interactionControllerHelper;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(PaginationExtended paginationExtended)
        {
            try
            {
                paginationExtended.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationExtended.RecordsPerPage);

                IEnumerable<InteractionDto> interactions = await _interactionService.GetInteractionsAsync(
                    paginationExtended.CurrentPage * paginationExtended.RecordsPerPage - paginationExtended.RecordsPerPage, 
                    paginationExtended.RecordsPerPage, 
                    paginationExtended.SearchString, 
                    paginationExtended.SearchBy, 
                    i => i.Client
                );

                IEnumerable<InteractionDto> interactionsForPagination = await _interactionService.GetInteractionsForPaginationAsync(
                    paginationExtended.CurrentPage, 
                    paginationExtended.RecordsPerPage, 
                    paginationExtended.SearchString, 
                    paginationExtended.SearchBy, 
                    i => i.Client
                );

                Pagination paginationBase = PagingUtilities.Pagination(paginationExtended.CurrentPage, paginationExtended.RecordsPerPage, interactions, interactionsForPagination);

                InteractionIndexViewModel viewModel = new InteractionIndexViewModel
                {
                    Interactions = interactions,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(paginationBase, paginationExtended)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("Client/{clientId}")]
        public async Task<ActionResult> GetInteractionsForClientView(int clientId, int currentPage = 1)
        {
            try
            {
                int recordsPerPage = GlobalVariables.defaultRecordsPerPage;

                ClientDto client = await _clientService.GetClientAsync(clientId);

                IEnumerable<InteractionDto> interactions = await _interactionService.GetInteractionsByClientIdAndOrderByDescendingAsync(clientId, currentPage * recordsPerPage - recordsPerPage, recordsPerPage);
                IEnumerable<InteractionDto> interactionsForPagination = await _interactionService.GetInteractionsByClientIdAndOrderByDescendingForPaginationAsync(clientId, currentPage, recordsPerPage);

                Pagination pagination = PagingUtilities.Pagination(currentPage, recordsPerPage, interactions, interactionsForPagination);

                InteractionsClientDetailsViewModel viewModel = _mapper.Map<ClientDto, InteractionsClientDetailsViewModel>(client);
                viewModel.Interactions = interactions;
                viewModel.Pagination = pagination;

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Authorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("New")]
        public async Task<ActionResult> NewView()
        {
            try
            {
                IEnumerable<InteractionTypeDto> interactionTypes = await _interactionTypeService.GetAllInteractionTypesAsync();

                NewInteractionViewModel viewModel = new NewInteractionViewModel
                {
                    AvailableTypes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(interactionTypes)
                };
                viewModel.SetSearchClientValues();

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("New/Client/{id}")]
        public async Task<ActionResult> NewInteractionForClientView(int id)
        {
            try
            {
                ClientDto client = await _clientService.GetClientAsync(id);

                IEnumerable<InteractionTypeDto> interactionTypes = await _interactionTypeService.GetAllInteractionTypesAsync();

                NewInteractionViewModel viewModel = _mapper.Map<ClientDto, NewInteractionViewModel>(client);
                viewModel.AvailableTypes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(interactionTypes);

                return View("NewView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(int id)
        {
            try
            {
                InteractionDto interaction = await _interactionService.GetInteractionWithIncludesAsync(i => i.Id == id, i => i.Client);

                if (interaction.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ControllerNameConstants.InteractionController, ControllerActionNameConstants.InteractionControllerDetailsView);

                InteractionDetailsViewModel viewModel = _mapper.Map<InteractionDto, InteractionDetailsViewModel>(interaction);
                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(InteractionSaveDto interactionSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    NewInteractionViewModel viewModel = await _interactionControllerHelper.GetNewInteractionViewModelWhenModelStateIsInvalidForAddMethod(interactionSaveDto);

                    return View("NewView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                InteractionDto interaction = await _interactionService.AddInteractionAsync(interactionSaveDto, userFullName);

                TempData["Success"] = InteractionAlertMessages.InteractionAddSuccessMessage;
                return RedirectToAction("DetailsView", "Interaction", new { id = interaction.Id });
            }
            catch (ApplicationCustomException applicationCustomException)
            {
                if (applicationCustomException is InteractionTypeNotFoundException)
                    ModelState.AddModelError(InteractionTypeExceptionMessages.InteractionTypeKey, applicationCustomException.Message);
                else if (applicationCustomException is InteractionClassNotFoundException)
                    ModelState.AddModelError(InteractionClassExceptionMessages.InteractionClassKey, applicationCustomException.Message);
                else
                    throw;

                NewInteractionViewModel viewModel = await _interactionControllerHelper.GetNewInteractionViewModelAndLogExceptionForAddMethod(applicationCustomException, this.ControllerContext, interactionSaveDto);

                return View("NewView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<ActionResult> EditView(int id)
        {
            try
            {
                InteractionDto interaction = await _interactionService.GetInteractionWithIncludesAsync(i => i.Id == id, i => i.Client);

                if (interaction.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ControllerNameConstants.InteractionController, ControllerActionNameConstants.InteractionControllerEditView);

                if (interaction.Status == InteractionStatusEnum.Canceled || interaction.Status == InteractionStatusEnum.Closed)
                    throw new InteractionStatusEditException(InteractionExceptionMessages.InteractionStatusEditExceptionMessage, ControllerNameConstants.InteractionController, ControllerActionNameConstants.InteractionControllerEditView);

                InteractionStatusEnum[] availableInteractionStatuses = EnumUtilities.GetAvailableInteractionStatusesForCurrentStatus(interaction.Status);

                InteractionEditViewModel viewModel = _mapper.Map<InteractionDto, InteractionEditViewModel>(interaction);
                viewModel.AvailableStatuses = ListOfValuesUtilities.CastArrayOfEnumsToSelectListItem<InteractionStatusEnum>(availableInteractionStatuses, interaction.Status);

                return View("EditView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }          
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        public async Task<ActionResult> Update(InteractionUpdateDto interactionUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    InteractionEditViewModel viewModel = await _interactionControllerHelper.GetViewModelWhenModelStateIsInvalidForUpdateMethod(interactionUpdateDto);

                    return View("EditView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                InteractionDto interactionDto = await _interactionService.UpdateInteractionAsync(interactionUpdateDto, userFullName);

                TempData["Success"] = InteractionAlertMessages.InteractionUpdateSuccessMessage;
                return RedirectToAction("DetailsView", new { id = interactionDto.Id });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                InteractionDto interactionDto = await _interactionService.DeleteInteractionAsync(id);

                TempData["Success"] = InteractionAlertMessages.InteractionDeleteSuccessMessage;
                return RedirectToAction("IndexView", "Interaction");
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}