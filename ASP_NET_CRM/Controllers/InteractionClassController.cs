﻿using ASP_NET_CRM.Constants.Alerts.InteractionClass;
using ASP_NET_CRM.Constants.Exceptions.Models.InteractionClass;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.InteractionClass;
using ASP_NET_CRM.DTO.InteractionType;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.InteractionClass;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.InteractionClasses;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("InteractionClass")]
    public class InteractionClassController : Controller
    {
        private readonly IInteractionClassService _interactionClassService;
        private readonly IInteractionTypeService _interactionTypeService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IInteractionClassControllerHelper _interactionClassControllerHelper;
        private readonly IMapper _mapper;

        public InteractionClassController(
            IInteractionClassService interactionClassService,
            IInteractionTypeService interactionTypeService,
            IApplicationErrorService applicationErrorService,
            IInteractionClassControllerHelper interactionClassControllerHelper,
            IMapper mapper
            )
        {
            _interactionClassService = interactionClassService;
            _interactionTypeService = interactionTypeService;
            _applicationErrorService = applicationErrorService;
            _interactionClassControllerHelper = interactionClassControllerHelper;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(PaginationExtended paginationRequest)
        {
            try
            {
                paginationRequest.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationRequest.RecordsPerPage);

                IEnumerable<InteractionClassDto> interactionClassDtos = await _interactionClassService.GetInteractionClassesAsync(
                    paginationRequest.CurrentPage * paginationRequest.RecordsPerPage - paginationRequest.RecordsPerPage,
                    paginationRequest.RecordsPerPage,
                    paginationRequest.SearchString,
                    paginationRequest.SearchBy,
                    ic => ic.InteractionType
                );

                IEnumerable<InteractionClassDto> interactionClassDtosForPagination = await _interactionClassService.GetInteractionClassesForPaginationAsync(
                    paginationRequest.CurrentPage,
                    paginationRequest.RecordsPerPage,
                    paginationRequest.SearchString,
                    paginationRequest.SearchBy,
                    ic => ic.InteractionType
                );

                Pagination paginationBase = PagingUtilities.Pagination(paginationRequest.CurrentPage, paginationRequest.RecordsPerPage, interactionClassDtos, interactionClassDtosForPagination);

                InteractionClassIndexViewModel viewModel = new InteractionClassIndexViewModel
                {
                    InteractionClasses = interactionClassDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(paginationBase, paginationRequest)
                };

                return View("IndexView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("New")]
        public async Task<ActionResult> NewView()
        {
            try
            {
                IEnumerable<InteractionTypeDto> interactionTypes = await _interactionTypeService.GetAllInteractionTypesAsync();

                NewInteractionClassViewModel viewModel = new NewInteractionClassViewModel
                {
                    AvailableTypes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(interactionTypes)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(int id)
        {
            try
            {
                InteractionClassDto interactionClassDto = await _interactionClassService.GetInteractionClassWithIncludesAsync(ic => ic.Id == id, ic => ic.InteractionType);

                InteractionClassDetailsViewModel viewModel = _mapper.Map<InteractionClassDto, InteractionClassDetailsViewModel>(interactionClassDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<ActionResult> EditView(int id)
        {
            try
            {
                InteractionClassDto interactionClassDto = await _interactionClassService.GetInteractionClassWithIncludesAsync(ic => ic.Id == id, ic => ic.InteractionType);

                InteractionClassEditViewModel viewModel = _mapper.Map<InteractionClassDto, InteractionClassEditViewModel>(interactionClassDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(InteractionClassSaveDto interactionClassSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    NewInteractionClassViewModel viewModel = await _interactionClassControllerHelper.GetViewModelWhenModelStateIsInvalidForAddMethod();

                    return View("NewView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                InteractionClassDto interactionClassDto = await _interactionClassService.AddInteractionClassAsync(interactionClassSaveDto, userFullName);

                TempData["Success"] = InteractionClassAlertMessages.InteractionClassAddSuccessMessage;

                return RedirectToAction("DetailsView", new { id = interactionClassDto.Id });
            }
            catch (InteractionClassExistsException exception)
            {
                ModelState.AddModelError(InteractionClassExceptionMessages.InteractionClassKey, exception.Message);

                NewInteractionClassViewModel viewModel = await _interactionClassControllerHelper.GetViewModelAndLogExceptionForAddMethod(exception, this.ControllerContext);

                return View("NewView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(InteractionClassUpdateDto interactionClassUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    InteractionClassEditViewModel viewModel = await _interactionClassControllerHelper.GetViewModelWhenModelStateIsInvalidForUpdateMethod(interactionClassUpdateDto);

                    return View("EditView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                InteractionClassDto interactionClassDto = await _interactionClassService.UpdateInteractionClassAsync(interactionClassUpdateDto, userFullName);

                TempData["Success"] = InteractionClassAlertMessages.InteractionClassUpdateSuccessMessage;

                return RedirectToAction("DetailsView", new { id = interactionClassDto.Id });
            }
            catch (InteractionClassExistsException exception)
            {
                ModelState.AddModelError(InteractionClassExceptionMessages.InteractionClassKey, exception.Message);

                InteractionClassEditViewModel viewModel = await _interactionClassControllerHelper.GetViewModelAndLogExceptionForUpdateMethod(exception, this.ControllerContext, interactionClassUpdateDto);

                return View("EditView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                InteractionClassDto interactionClassDto = await _interactionClassService.DeleteInteractionClassAsync(id);

                TempData["Success"] = InteractionClassAlertMessages.InteractionClassDeleteSuccessMessage;

                return RedirectToAction("IndexView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}