﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.Constants.Exceptions.Controllers;
using ASP_NET_CRM.Constants.Exceptions.Models.Address;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.DTO.AddressPrefix;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Address;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.Addresses;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ASP_NET_CRM.Constants.Alerts.Address;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("Address")]
    public class AddressController : Controller
    {
        private readonly IAddressService _addressService;
        private readonly IClientService _clientService;
        private readonly IAddressPrefixService _addressPrefixService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IAddressControllerHelper _addressControllerHelper;
        private readonly IMapper _mapper;

        public AddressController(
            IAddressService addressService,
            IClientService clientService,
            IAddressPrefixService addressPrefixService,
            IApplicationErrorService applicationErrorService,
            IAddressControllerHelper addressControllerHelper,
            IMapper mapper
            )
        {
            _addressService = addressService;
            _clientService = clientService;
            _addressPrefixService = addressPrefixService;
            _applicationErrorService = applicationErrorService;
            _addressControllerHelper = addressControllerHelper;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(AddressPagination addressPagination)
        {
            try
            {
                addressPagination.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(addressPagination.RecordsPerPage);

                IEnumerable<AddressDto> addressDtos = await _addressService.GetAddressesAsync(
                    addressPagination.CurrentPage * addressPagination.RecordsPerPage - addressPagination.RecordsPerPage,
                    addressPagination.RecordsPerPage,
                    addressPagination, 
                    a => a.Client
                );

                IEnumerable<AddressDto> addressDtosForPagination = await _addressService.GetAddressesForPaginationAsync(addressPagination, a => a.Client);

                Pagination paginationBase = PagingUtilities.Pagination(addressPagination.CurrentPage, addressPagination.RecordsPerPage, addressDtos, addressDtosForPagination);

                AddressIndexViewModel viewModel = new AddressIndexViewModel
                {
                    Addresses = addressDtos,
                    AddressSearch = _mapper.Map<Pagination, AddressPagination>(paginationBase, addressPagination)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("Client/{clientId}")]
        public async Task<ActionResult> GetAddressesForClientView(int clientId, int currentPage = 1)
        {
            try
            {
                int recordsPerPage = GlobalVariables.defaultRecordsPerPage;

                ClientDto clientDto = await _clientService.GetClientAsync(clientId);

                IEnumerable<AddressDto> addressDtos = await _addressService.GetAddressesByClientIdOrderByDescendingAsync(clientId, currentPage * recordsPerPage - recordsPerPage, recordsPerPage);
                IEnumerable<AddressDto> addressDtosForPagination = await _addressService.GetAddressesByClientIdOrderByDescendingForPaginationAsync(clientId, currentPage, recordsPerPage);

                Pagination pagination = PagingUtilities.Pagination(currentPage, recordsPerPage, addressDtos, addressDtosForPagination);

                AddressesClientDetailsViewModel viewModel = _mapper.Map<ClientDto, AddressesClientDetailsViewModel>(clientDto);
                viewModel.Addresses = _mapper.Map<IEnumerable<AddressDto>, IEnumerable<AddressLightweightDto>>(addressDtos);
                viewModel.Pagination = pagination;

                return View(viewModel);
            }
            catch
            {
                throw;
            }
            
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(int id)
        {
            try
            {
                AddressDto addressDto = await _addressService.GetAddressWithIncludesAsync(a => a.Id == id, a => a.Client);

                if (addressDto.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ControllerNameConstants.AddressController, ControllerActionNameConstants.AddressControllerDetailsView);

                AddressDetailsViewModel viewModel = _mapper.Map<AddressDto, AddressDetailsViewModel>(addressDto);
                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("New")]
        public async Task<ActionResult> NewAddressView()
        {
            try
            {
                IEnumerable<AddressPrefixDto> addressPrefixes = await _addressPrefixService.GetAllAddressPrefixes();

                NewAddressViewModel viewModel = new NewAddressViewModel();
                viewModel.AvailablePrefixes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(addressPrefixes);
                viewModel.SetSearchClientValues();

                return View("NewView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("New/Client/{id}")]
        public async Task<ActionResult> NewAddressForClientView(int id)
        {
            try
            {
                ClientDto clientDto = await _clientService.GetClientAsync(id);

                IEnumerable<AddressPrefixDto> addressPrefixes = await _addressPrefixService.GetAllAddressPrefixes();

                NewAddressViewModel viewModel = _mapper.Map<ClientDto, NewAddressViewModel>(clientDto);
                viewModel.AvailablePrefixes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(addressPrefixes);

                return View("NewView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }           
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(AddressSaveDto addressSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    NewAddressViewModel viewModel = await _addressControllerHelper.GetNewAddressViewModelWhenModelStateIsInvalidForAddMethod(addressSaveDto);

                    return View("NewView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                AddressDto addressDto = await _addressService.AddAddressAsync(addressSaveDto, userFullName);

                TempData["Success"] = AddressAlertMessages.AddressAddSuccessMessage;
                return RedirectToAction("DetailsView", "Address", new { id = addressDto.Id });
            }
            catch (ApplicationCustomException applicationCustomException)
            {
                if (applicationCustomException is AddressMainForClientExistsException)
                {
                    ModelState.AddModelError(AddressExceptionMessages.AddressKey, applicationCustomException.Message);

                    NewAddressViewModel viewModel = await _addressControllerHelper.GetNewAddressViewModelAndLogExceptionForAddMethod(applicationCustomException, this.ControllerContext, addressSaveDto);

                    return View("NewView", viewModel);
                }
                else
                    throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<ActionResult> EditView(int id)
        {
            try
            {
                AddressDto addressDto = await _addressService.GetAddressWithIncludesAsync(a => a.Id == id, a => a.Client);

                if (addressDto.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ControllerNameConstants.AddressController, ControllerActionNameConstants.AddressControllerEditView);

                IEnumerable<AddressPrefixDto> addressPrefixes = await _addressPrefixService.GetAllAddressPrefixes();

                AddressEditViewModel viewModel = _mapper.Map<AddressDto, AddressEditViewModel>(addressDto);
                viewModel.AvailablePrefixes = ListOfValuesUtilities.CastIEnumerableOfAddressPrefixDtoToSelectListItemWithCurrentPrefixValueProperty(addressPrefixes, addressDto.Prefix);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        public async Task<ActionResult> Update(AddressUpdateDto addressUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    AddressEditViewModel viewModel = await _addressControllerHelper.GetAddressEditViewModelWhenModelStateIsInvalidForUpdateMethod(addressUpdateDto);

                    return View("EditView", viewModel);
                }
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                AddressDto addressDto = await _addressService.UpdateAddressAsync(addressUpdateDto, userFullName);

                TempData["Success"] = AddressAlertMessages.AddressUpdateSuccessMessage;
                return RedirectToAction("DetailsView", new { id = addressDto.Id });
            }
            catch (Exception) 
            { 
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                AddressDto addressDto = await _addressService.DeleteAddressAsync(id);

                TempData["Success"] = AddressAlertMessages.AddressDeleteSuccessMessage;
                return RedirectToAction("IndexView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}