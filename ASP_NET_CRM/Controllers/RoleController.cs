﻿using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Role;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.Roles;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("Role")]
    public class RoleController : Controller
    {
        private readonly IRoleService _roleService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public RoleController(
            IRoleService roleService,
            IApplicationErrorService applicationErrorService,
            IMapper mapper
            )
        {
            _roleService = roleService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("")]
        public async Task<ActionResult> IndexView(PaginationExtendedLightweight paginationRequest)
        {
            try
            {
                paginationRequest.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationRequest.RecordsPerPage);

                IEnumerable<RoleDto> roleDtos = await _roleService.GetRolesAsync(
                    paginationRequest.CurrentPage * paginationRequest.RecordsPerPage - paginationRequest.RecordsPerPage,
                    paginationRequest.RecordsPerPage
                );

                IEnumerable<RoleDto> roleDtosForPagination = await _roleService.GetRolesForPaginationAsync(paginationRequest.CurrentPage, paginationRequest.RecordsPerPage);

                Pagination pagination = PagingUtilities.Pagination(
                    paginationRequest.CurrentPage,
                    paginationRequest.RecordsPerPage,
                    roleDtos,
                    roleDtosForPagination
                );

                RoleIndexViewModel viewModel = new RoleIndexViewModel
                {
                    Roles = roleDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtendedLightweight>(pagination, paginationRequest)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(string id)
        {
            try
            {
                RoleDto roleDto = await _roleService.GetRoleAsync(id);

                RoleDetailsViewModel viewModel = _mapper.Map<RoleDto, RoleDetailsViewModel>(roleDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}