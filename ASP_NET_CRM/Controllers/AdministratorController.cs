﻿using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    public class AdministratorController : Controller
    {
        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        public ActionResult IndexView()
        {
            return View();
        }
    }
}