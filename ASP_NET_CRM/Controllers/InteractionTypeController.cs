﻿using ASP_NET_CRM.Constants.Alerts.InteractionType;
using ASP_NET_CRM.Constants.Exceptions.Models.InteractionType;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.InteractionType;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.InteractionType;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.InteractionTypes;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("InteractionType")]
    public class InteractionTypeController : Controller
    {
        private readonly IInteractionTypeService _interactionTypeService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IInteractionTypeControllerHelper _interactionTypeControllerHelper;
        private readonly IMapper _mapper;

        public InteractionTypeController(
            IInteractionTypeService interactionTypeService,
            IApplicationErrorService applicationErrorService,
            IInteractionTypeControllerHelper interactionTypeControllerHelper,
            IMapper mapper
            )
        {
            _interactionTypeService = interactionTypeService;
            _applicationErrorService = applicationErrorService;
            _interactionTypeControllerHelper = interactionTypeControllerHelper;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(PaginationExtended paginationRequest)
        {
            try
            {
                paginationRequest.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationRequest.RecordsPerPage);

                IEnumerable<InteractionTypeDto> interactionTypeDtos = await _interactionTypeService.GetInteractionTypesAsync(
                    paginationRequest.CurrentPage * paginationRequest.RecordsPerPage - paginationRequest.RecordsPerPage,
                    paginationRequest.RecordsPerPage,
                    paginationRequest.SearchString,
                    paginationRequest.SearchBy
                );

                IEnumerable<InteractionTypeDto> interactionTypeDtosForPagination = await _interactionTypeService.GetInteractionTypesForPaginationAsync(
                    paginationRequest.CurrentPage,
                    paginationRequest.RecordsPerPage,
                    paginationRequest.SearchString,
                    paginationRequest.SearchBy
                );

                Pagination paginationBase = PagingUtilities.Pagination(paginationRequest.CurrentPage, paginationRequest.RecordsPerPage, interactionTypeDtos, interactionTypeDtosForPagination);

                InteractionTypeIndexViewModel viewModel = new InteractionTypeIndexViewModel
                {
                    InteractionTypes = interactionTypeDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(paginationBase, paginationRequest)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(int id)
        {
            try
            {
                InteractionTypeDto interactionTypeDto = await _interactionTypeService.GetInteractionTypeAsync(id);

                InteractionTypeDetailsViewModel viewModel = _mapper.Map<InteractionTypeDto, InteractionTypeDetailsViewModel>(interactionTypeDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("New")]
        public ActionResult NewView()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(InteractionTypeSaveDto interactionTypeSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return View("NewView");

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                InteractionTypeDto interactionTypeDto = await _interactionTypeService.AddInteractionTypeAsync(interactionTypeSaveDto, userFullName);

                TempData["Success"] = InteractionTypeAlertMessages.InteractionTypeAddSuccessMessage;

                return RedirectToAction("DetailsView", new { id = interactionTypeDto.Id });
            }
            catch (InteractionTypeExistsException exception)
            {
                ModelState.AddModelError(InteractionTypeExceptionMessages.InteractionTypeKey, exception.Message);

                _interactionTypeControllerHelper.LogExceptionForAddMethod(exception, this.ControllerContext);

                return View("NewView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<ActionResult> EditView(int id)
        {
            try
            {
                InteractionTypeDto interactionTypeDto = await _interactionTypeService.GetInteractionTypeAsync(id);

                InteractionTypeEditViewModel viewModel = _mapper.Map<InteractionTypeDto, InteractionTypeEditViewModel>(interactionTypeDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(InteractionTypeUpdateDto interactionTypeUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    InteractionTypeEditViewModel viewModel = await _interactionTypeControllerHelper.GetViewModelWhenModelStateIsInvalidForUpdateMethod(interactionTypeUpdateDto);

                    return View("EditView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                InteractionTypeDto interactionTypeDto = await _interactionTypeService.UpdateInteractionTypeAsync(interactionTypeUpdateDto, userFullName);

                TempData["Success"] = InteractionTypeAlertMessages.InteractionTypeUpdateSuccessMessage;

                return RedirectToAction("DetailsView", new { id = interactionTypeDto.Id });
            }
            catch (InteractionTypeExistsException exception)
            {
                ModelState.AddModelError(InteractionTypeExceptionMessages.InteractionTypeKey, exception.Message);

                _interactionTypeControllerHelper.LogExceptionForAddMethod(exception, this.ControllerContext);

                InteractionTypeEditViewModel viewModel = new InteractionTypeEditViewModel { Id = interactionTypeUpdateDto.Id };

                return View("EditView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                InteractionTypeDto interactionTypeDto = await _interactionTypeService.DeleteInteractionTypeAsync(id);

                TempData["Success"] = InteractionTypeAlertMessages.InteractionTypeDeleteSuccessMessage;

                return RedirectToAction("IndexView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}