﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.Constants.Alerts.Note;
using ASP_NET_CRM.Constants.Exceptions.Controllers;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.Notes;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("Note")]
    public class NoteController : Controller
    {
        private readonly INoteService _noteService;
        private readonly IClientService _clientService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly INoteControllerHelper _noteControllerHelper;
        private readonly IMapper _mapper;

        public NoteController(
            INoteService noteService, 
            IClientService clientService,
            IApplicationErrorService applicationErrorService,
            INoteControllerHelper noteControllerHelper,
            IMapper mapper
            )
        {
            _noteService = noteService;
            _clientService = clientService;
            _applicationErrorService = applicationErrorService;
            _noteControllerHelper = noteControllerHelper;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(PaginationExtended paginationExtended)
        {
            try
            {
                paginationExtended.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationExtended.RecordsPerPage);

                IEnumerable<NoteDto> noteDtos = await _noteService.GetNotesAsync(
                    paginationExtended.CurrentPage * paginationExtended.RecordsPerPage - paginationExtended.RecordsPerPage,
                    paginationExtended.RecordsPerPage,
                    paginationExtended.SearchString,
                    paginationExtended.SearchBy,
                    i => i.Client
                );

                IEnumerable<NoteDto> noteDtosForPagination = await _noteService.GetNotesForPaginationAsync(
                    paginationExtended.CurrentPage,
                    paginationExtended.RecordsPerPage,
                    paginationExtended.SearchString,
                    paginationExtended.SearchBy,
                    i => i.Client
                );

                Pagination paginationBase = PagingUtilities.Pagination(paginationExtended.CurrentPage, paginationExtended.RecordsPerPage, noteDtos, noteDtosForPagination);

                NoteIndexViewModel viewModel = new NoteIndexViewModel
                {
                    Notes = noteDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(paginationBase, paginationExtended)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("Client/{clientId}")]
        public async Task<ActionResult> GetNotesForClientView(int clientId, int currentPage = 1)
        {
            try
            {
                int recordsPerPage = GlobalVariables.defaultRecordsPerPage;

                ClientDto clientDto = await _clientService.GetClientAsync(clientId);

                IEnumerable<NoteDto> noteDtos = await _noteService.GetNotesByClientIdOrderByDescendingAsync(clientId, currentPage * recordsPerPage - recordsPerPage, recordsPerPage);
                IEnumerable<NoteDto> noteDtosForPagination = await _noteService.GetNotesByClientIdOrderByDescendingForPaginationAsync(clientId, currentPage, recordsPerPage);

                Pagination pagination = PagingUtilities.Pagination(currentPage, recordsPerPage, noteDtos, noteDtosForPagination);

                NotesClientDetailsViewModel viewModel = _mapper.Map<ClientDto, NotesClientDetailsViewModel>(clientDto);
                viewModel.Notes = noteDtos;
                viewModel.Pagination = pagination;

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(int id)
        {
            try
            {
                NoteDto noteDto = await _noteService.GetNoteWithIncludesAsync(n => n.Id == id, n => n.Client);

                if (noteDto.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ControllerNameConstants.NoteController, ControllerActionNameConstants.NoteControllerDetailsView);

                NoteDetailsViewModel viewModel = _mapper.Map<NoteDto, NoteDetailsViewModel>(noteDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("New")]
        public ActionResult NewView()
        {
            try
            {
                NewNoteViewModel viewModel = new NewNoteViewModel();
                viewModel.SetSearchClientValues();

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("New/Client/{clientId}")]
        public async Task<ActionResult> NewNoteForClientView(int clientId)
        {
            try
            {
                ClientDto clientDto = await _clientService.GetClientAsync(clientId);

                NewNoteViewModel viewModel = _mapper.Map<ClientDto, NewNoteViewModel>(clientDto);

                return View("NewView", viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(NoteSaveDto noteSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    NewNoteViewModel viewModel = await _noteControllerHelper.GetNewNoteViewModelWhenModelStateIsInvalidForAddMethod(noteSaveDto);

                    return View("NewView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                NoteDto noteDto = await _noteService.AddNoteAsync(noteSaveDto, userFullName);

                TempData["Success"] = NoteAlertMessages.NoteAddSuccessMessage;
                return RedirectToAction("DetailsView", new { id = noteDto.Id });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<ActionResult> EditView(int id)
        {
            try
            {
                NoteDto noteDto = await _noteService.GetNoteWithIncludesAsync(n => n.Id == id, n => n.Client);

                if (noteDto.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ControllerNameConstants.NoteController, ControllerActionNameConstants.NoteControllerEditView);

                NoteEditViewModel viewModel = _mapper.Map<NoteDto, NoteEditViewModel>(noteDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(NoteUpdateDto noteUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    NoteEditViewModel viewModel = await _noteControllerHelper.GetViewModelWhenModelStateIsInvalidForUpdateMethod(noteUpdateDto);

                    return View("EditView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                NoteDto noteDto = await _noteService.UpdateNoteAsync(noteUpdateDto, userFullName);

                TempData["Success"] = NoteAlertMessages.NoteUpdateSuccessMessage;
                return RedirectToAction("DetailsView", new { id = noteDto.Id });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                NoteDto noteDto = await _noteService.DeleteNoteAsync(id);

                TempData["Success"] = NoteAlertMessages.NoteDeleteSuccessMessage;
                return RedirectToAction("IndexView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }

    }
}