﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.Constants.WebAPI.Messages;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Models.Utilities;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace ASP_NET_CRM.Controllers.API
{
    [Authorize]
    [RoutePrefix("Api/Client")]
    public class ClientController : ApiController
    {
        private readonly IClientService _clientService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public ClientController(
            IClientService clientService, 
            IApplicationErrorService applicationErrorService,
            IMapper mapper
            )
        {
            _clientService = clientService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        [Authorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        public async Task<IHttpActionResult> GetClients([FromUri] PaginationExtended pagination)
        {
            try
            {
                pagination.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(pagination.RecordsPerPage);

                IEnumerable<ClientDto> clientDtos = await _clientService.GetClientsAsync(
                    pagination.CurrentPage * pagination.RecordsPerPage - pagination.RecordsPerPage,
                    pagination.RecordsPerPage,
                    pagination.SearchString,
                    pagination.SearchBy
                );

                IEnumerable<ClientDto> clientDtosForPagination = await _clientService.GetClientsForPaginationAsync(
                    pagination.CurrentPage,
                    pagination.RecordsPerPage,
                    pagination.SearchString,
                    pagination.SearchBy
                );

                Pagination paginationBase = PagingUtilities.Pagination(pagination.CurrentPage, pagination.RecordsPerPage, clientDtos, clientDtosForPagination);

                Page<ClientDto> response = new Page<ClientDto>
                {
                    Data = clientDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(paginationBase, pagination)
                };

                return Ok(response);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                ClientDto clientDto = await _clientService.GetClientWithIncludesAsync(c => c.Id == id, c => c.Interactions, c => c.Addresses, c => c.Notes, c => c.Consents);

                return Ok(clientDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        public async Task<IHttpActionResult> Add(ClientSaveDto clientSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ClientDto clientDto = await _clientService.AddClientAsync(clientSaveDto, userFullName);

                return Ok(clientDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPut]
        public async Task<IHttpActionResult> Update(ClientUpdateDto clientUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ClientDto clientDto = await _clientService.UpdateClientAsync(clientUpdateDto, userFullName);

                return Ok(clientDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.AdministratorRole)]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                ClientDto clientDto = await _clientService.DeleteClientAsync(id);

                return Ok(clientDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        private IHttpActionResult OnException(Exception exception)
        {
            if (exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ApplicationCustomException applicationCustomException = (ApplicationCustomException)exception;
                applicationCustomException.SetControllerAndExceptionProperties
                    (
                        this.ControllerContext.ControllerDescriptor.ControllerName,
                        this.ActionContext.ActionDescriptor.ActionName,
                        ControllerType.API.ToString(),
                        exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(applicationCustomException);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                if (applicationCustomException.HttpStatusCode != HttpStatusCode.InternalServerError)
                    return Content(applicationCustomException.HttpStatusCode, applicationCustomException.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, GlobalWebAPIResponseMessages.InternalServerErrorMessage);
            }
            else
            {
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = this.ControllerContext.ControllerDescriptor.ControllerName,
                    ControllerActionName = this.ActionContext.ActionDescriptor.ActionName,
                    ControllerType = ControllerType.API.ToString(),
                    ExceptionType = exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                return Content(HttpStatusCode.InternalServerError, GlobalWebAPIResponseMessages.InternalServerErrorMessage);
            }
        }
    }
}
