﻿using ASP_NET_CRM.DTO.InteractionClass;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.Constants.WebAPI.Messages;
using ASP_NET_CRM.Constants.Roles;
using System.Security.Claims;

namespace ASP_NET_CRM.Controllers.API
{
    [Authorize]
    [RoutePrefix("Api/InteractionClass")]
    public class InteractionClassController : ApiController
    {
        private readonly IInteractionClassService _interactionClassService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public InteractionClassController(
            IInteractionClassService interactionClassService,
            IApplicationErrorService applicationErrorService,
            IMapper mapper
            )
        {
            _interactionClassService = interactionClassService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        [Authorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("InteractionType/{interactionTypeId}")]
        public async Task<IHttpActionResult> GetInteractionClassesByInteractionType(int interactionTypeId)
        {
            try
            {
                IEnumerable<InteractionClassDto> interactionClasses = await _interactionClassService.GetInteractionClassesForInteractionTypeAsync(interactionTypeId);

                return Ok(interactionClasses);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.AdministratorRole)]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                InteractionClassDto interactionClassDto = await _interactionClassService.DeleteInteractionClassAsync(id);

                return Ok(interactionClassDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        private IHttpActionResult OnException(Exception exception)
        {
            if (exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ApplicationCustomException applicationCustomException = (ApplicationCustomException)exception;
                applicationCustomException.SetControllerAndExceptionProperties
                    (
                        this.ControllerContext.ControllerDescriptor.ControllerName,
                        this.ActionContext.ActionDescriptor.ActionName,
                        ControllerType.API.ToString(),
                        exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(applicationCustomException);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                if (applicationCustomException.HttpStatusCode == HttpStatusCode.NotFound)
                    return Content(HttpStatusCode.NotFound, applicationCustomException.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, GlobalWebAPIResponseMessages.InternalServerErrorMessage);
            }
            else
            {
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = this.ControllerContext.ControllerDescriptor.ControllerName,
                    ControllerActionName = this.ActionContext.ActionDescriptor.ActionName,
                    ControllerType = ControllerType.API.ToString(),
                    ExceptionType = exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                return Content(HttpStatusCode.InternalServerError, GlobalWebAPIResponseMessages.InternalServerErrorMessage);
            }
        }
    }
}
