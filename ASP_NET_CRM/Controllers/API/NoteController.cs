﻿using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.Constants.WebAPI.Messages;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace ASP_NET_CRM.Controllers.API
{
    [Authorize]
    [RoutePrefix("Api/Note")]
    public class NoteController : ApiController
    {
        private readonly INoteService _noteService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public NoteController(
            INoteService noteService,
            IApplicationErrorService applicationErrorService,
            IMapper mapper
            )
        {
            _noteService = noteService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        [Authorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        public async Task<IHttpActionResult> GetNotes([FromUri] PaginationExtended pagination)
        {
            try
            {
                pagination.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(pagination.RecordsPerPage);

                IEnumerable<NoteDto> noteDtos = await _noteService.GetNotesAsync(
                    pagination.CurrentPage * pagination.RecordsPerPage - pagination.RecordsPerPage,
                    pagination.RecordsPerPage,
                    pagination.SearchString,
                    pagination.SearchBy
                );

                IEnumerable<NoteDto> noteDtosForPagination = await _noteService.GetNotesForPaginationAsync(
                    pagination.CurrentPage,
                    pagination.RecordsPerPage,
                    pagination.SearchString,
                    pagination.SearchBy
                );

                Pagination paginationBase = PagingUtilities.Pagination(pagination.CurrentPage, pagination.RecordsPerPage, noteDtos, noteDtosForPagination);

                Page<NoteDto> response = new Page<NoteDto>
                {
                    Data = noteDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(paginationBase, pagination)
                };

                return Ok(response);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                NoteDto noteDto = await _noteService.GetNoteWithIncludesAsync(n => n.Id == id);

                return Ok(noteDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        public async Task<IHttpActionResult> Add(NoteSaveDto noteSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                NoteDto noteDto = await _noteService.AddNoteAsync(noteSaveDto, userFullName);
                noteDto.Client = null;

                return Ok(noteDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }
        [Authorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPut]
        public async Task<IHttpActionResult> Update(NoteUpdateDto noteUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                NoteDto noteDto = await _noteService.UpdateNoteAsync(noteUpdateDto, userFullName);
                noteDto.Client = null;

                return Ok(noteDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.AdministratorRole)]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                NoteDto noteDto = await _noteService.DeleteNoteAsync(id);

                return Ok(noteDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        private IHttpActionResult OnException(Exception exception)
        {
            if (exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ApplicationCustomException applicationCustomException = (ApplicationCustomException)exception;
                applicationCustomException.SetControllerAndExceptionProperties
                    (
                        this.ControllerContext.ControllerDescriptor.ControllerName,
                        this.ActionContext.ActionDescriptor.ActionName,
                        ControllerType.API.ToString(),
                        exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(applicationCustomException);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                if (applicationCustomException.HttpStatusCode != HttpStatusCode.InternalServerError)
                    return Content(applicationCustomException.HttpStatusCode, applicationCustomException.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, GlobalWebAPIResponseMessages.InternalServerErrorMessage);
            }
            else
            {
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = this.ControllerContext.ControllerDescriptor.ControllerName,
                    ControllerActionName = this.ActionContext.ActionDescriptor.ActionName,
                    ControllerType = ControllerType.API.ToString(),
                    ExceptionType = exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                return Content(HttpStatusCode.InternalServerError, GlobalWebAPIResponseMessages.InternalServerErrorMessage);
            }
        }
    }
}
