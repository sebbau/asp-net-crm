﻿using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.Constants.WebAPI.Messages;
using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace ASP_NET_CRM.Controllers.API
{
    [Authorize]
    [RoutePrefix("Api/Address")]
    public class AddressController : ApiController
    {
        private readonly IAddressService _addressService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public AddressController(
            IAddressService addressService,
            IApplicationErrorService applicationErrorService,
            IMapper mapper
            )
        {
            _addressService = addressService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        [Authorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        public async Task<IHttpActionResult> GetAddresses([FromUri] AddressPagination addressPagination)
        {
            try
            {
                addressPagination.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(addressPagination.RecordsPerPage);

                IEnumerable<AddressDto> addressDtos = await _addressService.GetAddressesAsync(
                    addressPagination.CurrentPage * addressPagination.RecordsPerPage - addressPagination.RecordsPerPage,
                    addressPagination.RecordsPerPage,
                    addressPagination
                );

                IEnumerable<AddressDto> addressDtosForPagination = await _addressService.GetAddressesForPaginationAsync(addressPagination, a => a.Client);

                Pagination paginationBase = PagingUtilities.Pagination(addressPagination.CurrentPage, addressPagination.RecordsPerPage, addressDtos, addressDtosForPagination);

                Page<AddressDto> response = new Page<AddressDto>
                {
                    Data = addressDtos,
                    Pagination = _mapper.Map<Pagination, AddressPagination>(paginationBase, addressPagination)
                };

                return Ok(response);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Authorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("{id}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                AddressDto addressDto = await _addressService.GetAddressWithPredicate(a => a.Id == id);

                return Ok(addressDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        public async Task<IHttpActionResult> Add(AddressSaveDto addressSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                AddressDto addressDto = await _addressService.AddAddressAsync(addressSaveDto, userFullName);
                addressDto.Client = null;

                return Ok(addressDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPut]
        public async Task<IHttpActionResult> Update(AddressUpdateDto addressUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                AddressDto addressDto = await _addressService.UpdateAddressAsync(addressUpdateDto, userFullName);
                addressDto.Client = null;

                return Ok(addressDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        [Authorize(Roles = RoleConstants.AdministratorRole)]
        [HttpDelete]
        [Route("{id}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                AddressDto addressDto = await _addressService.DeleteAddressAsync(id);

                return Ok(addressDto);
            }
            catch (Exception exception)
            {
                return OnException(exception);
            }
        }

        private IHttpActionResult OnException(Exception exception)
        {
            if (exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ApplicationCustomException applicationCustomException = (ApplicationCustomException)exception;
                applicationCustomException.SetControllerAndExceptionProperties
                    (
                        this.ControllerContext.ControllerDescriptor.ControllerName,
                        this.ActionContext.ActionDescriptor.ActionName,
                        ControllerType.API.ToString(),
                        exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(applicationCustomException);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                if (applicationCustomException.HttpStatusCode != HttpStatusCode.InternalServerError)
                    return Content(applicationCustomException.HttpStatusCode, applicationCustomException.Message);
                else
                    return Content(HttpStatusCode.InternalServerError, GlobalWebAPIResponseMessages.InternalServerErrorMessage);
            }
            else
            {
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                string userFullName = identity.FindFirst(ClaimTypes.UserData).Value;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = this.ControllerContext.ControllerDescriptor.ControllerName,
                    ControllerActionName = this.ActionContext.ActionDescriptor.ActionName,
                    ControllerType = ControllerType.API.ToString(),
                    ExceptionType = exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                return Content(HttpStatusCode.InternalServerError, GlobalWebAPIResponseMessages.InternalServerErrorMessage);
            }
        }
    }
}
