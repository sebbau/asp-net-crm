﻿using ASP_NET_CRM.Constants.Alerts.User;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.User;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.Users;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("User")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IUserControllerHelper _userControllerHelper;
        private readonly IMapper _mapper;

        public UserController(
            IUserService userService,
            IApplicationErrorService applicationErrorService,
            IUserControllerHelper userControllerHelper,
            IMapper mapper
            )
        {
            _userService = userService;
            _applicationErrorService = applicationErrorService;
            _userControllerHelper = userControllerHelper;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(PaginationExtended paginationExtended)
        {
            try
            {
                paginationExtended.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationExtended.RecordsPerPage);

                IEnumerable<UserDto> userDtos = await _userService.GetUsersAsync(
                    paginationExtended.CurrentPage * paginationExtended.RecordsPerPage - paginationExtended.RecordsPerPage,
                    paginationExtended.RecordsPerPage,
                    paginationExtended.SearchString,
                    paginationExtended.SearchBy
                );

                IEnumerable<UserDto> userDtosForPagination = await _userService.GetUsersForPaginationAsync(
                    paginationExtended.CurrentPage,
                    paginationExtended.RecordsPerPage,
                    paginationExtended.SearchString,
                    paginationExtended.SearchBy
                );

                Pagination pagination = PagingUtilities.Pagination(paginationExtended.CurrentPage, paginationExtended.RecordsPerPage, userDtos, userDtosForPagination);

                UserIndexViewModel viewModel = new UserIndexViewModel
                {
                    Users = userDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(pagination, paginationExtended)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(string id)
        {
            try
            {
                UserDto userDto = await _userService.GetUserAsync(id);

                UserDetailsViewModel viewModel = _mapper.Map<UserDto, UserDetailsViewModel>(userDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Edit/{id}")]
        public async Task<ActionResult> EditView(string id)
        {
            try
            {
                UserDto userDto = await _userService.GetUserAsync(id);

                UserEditViewModel viewModel = _mapper.Map<UserDto, UserEditViewModel>(userDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(UserUpdateDto userUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    UserEditViewModel viewModel = await _userControllerHelper.GetViewModelWhenModelStateIsInvalidForUpdateMethod(userUpdateDto);

                    return View("EditView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                UserDto userDto = await _userService.UpdateUserAsync(userUpdateDto, userFullName);

                TempData["Success"] = UserAlertMessages.UserUpdateSuccessMessage;

                return RedirectToAction("DetailsView", new { id = userDto.Id });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                UserDto userDto = await _userService.DeleteUserAsync(id);

                TempData["Success"] = UserAlertMessages.UserDeleteSuccessMessage;

                return RedirectToAction("IndexView");
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}