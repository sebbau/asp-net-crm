﻿using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.Filters;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        public ActionResult Index()
        {
            return View();
        }
    }
}