﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.ApplicationErrors;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("ApplicationError")]
    public class ApplicationErrorController : Controller
    {
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public ApplicationErrorController(IApplicationErrorService applicationErrorService, IMapper mapper)
        {
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        public async Task<ActionResult> IndexView(PaginationExtendedLightweight paginationRequest)
        {
            try
            {
                IEnumerable<ApplicationErrorDto> applicationErrorDtos = await _applicationErrorService.GetApplicationErrorsAsync(
                    paginationRequest.CurrentPage * paginationRequest.RecordsPerPage - paginationRequest.RecordsPerPage,
                    paginationRequest.RecordsPerPage
                );

                IEnumerable<ApplicationErrorDto> applicationErrorDtosForPagination = await _applicationErrorService.GetApplicationErrorsForPaginationAsync(
                    paginationRequest.CurrentPage,
                    paginationRequest.RecordsPerPage
                );

                Pagination pagination = PagingUtilities.Pagination(
                    paginationRequest.CurrentPage,
                    paginationRequest.RecordsPerPage,
                    applicationErrorDtos,
                    applicationErrorDtosForPagination
                );

                ApplicationErrorIndexViewModel viewModel = new ApplicationErrorIndexViewModel
                {
                    ApplicationErrors = applicationErrorDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtendedLightweight>(pagination, paginationRequest)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(int id)
        {
            try
            {
                ApplicationErrorDto applicationErrorDto = await _applicationErrorService.GetApplicationErrorAsync(id);

                ApplicationErrorDetailsViewModel viewModel = _mapper.Map<ApplicationErrorDto, ApplicationErrorDetailsViewModel>(applicationErrorDto);

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("ErrorPage")]
        public ActionResult ErrorPageView()
        {
            try
            {
                ErrorPageViewModel viewModel;

                if (TempData["ErrorPageData"] != null)
                {
                    var exceptionData = TempData["ErrorPageData"];

                    if (exceptionData.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
                    {
                        ApplicationCustomException applicationCustomException = (ApplicationCustomException) exceptionData;
                        viewModel = new ErrorPageViewModel { ErrorMessage = applicationCustomException.Message };
                    }
                    else
                    {
                        viewModel = new ErrorPageViewModel { ErrorMessage = GlobalExceptions.InternalServerErrorMessage };
                    }

                    return View("ErrorPageView", viewModel);
                }
                else
                {
                    viewModel = new ErrorPageViewModel { ErrorMessage = GlobalExceptions.InternalServerErrorMessage };
                    return View("ErrorPageView", viewModel);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("Unathorized")]
        public ActionResult UnathorizedView()
        {
            return View();
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}