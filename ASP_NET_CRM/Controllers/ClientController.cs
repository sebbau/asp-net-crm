﻿using ASP_NET_CRM.Constants.Alerts.Client;
using ASP_NET_CRM.Constants.Exceptions.Controllers;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Roles;
using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Filters;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities.Paging;
using ASP_NET_CRM.ViewModels.Clients;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Controllers
{
    [Authorize]
    [RoutePrefix("Client")]
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IAddressService _addressService;
        private readonly IMapper _mapper;
        private readonly IClientControllerHelper _clientControllerHelper;

        public ClientController
            (
            IClientService clientService, 
            IApplicationErrorService applicationErrorService,
            IAddressService addressService,
            IMapper mapper,
            IClientControllerHelper clientControllerHelper
            )
        {
            _clientService = clientService;
            _applicationErrorService = applicationErrorService;
            _addressService = addressService;
            _mapper = mapper;
            _clientControllerHelper = clientControllerHelper;
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        // GET: Client
        public async Task<ActionResult> IndexView(PaginationExtended paginationExtended)
        {
            try
            {
                paginationExtended.RecordsPerPage = PagingUtilities.ValidateMaxRecordsPerPage(paginationExtended.RecordsPerPage);

                IEnumerable<ClientDto> clientDtos = await _clientService.GetClientsAsync(
                    paginationExtended.CurrentPage * paginationExtended.RecordsPerPage - paginationExtended.RecordsPerPage, 
                    paginationExtended.RecordsPerPage, 
                    paginationExtended.SearchString, 
                    paginationExtended.SearchBy
                );

                IEnumerable<ClientDto> clientDtosForPagination = await _clientService.GetClientsForPaginationAsync(
                    paginationExtended.CurrentPage, 
                    paginationExtended.RecordsPerPage, 
                    paginationExtended.SearchString, 
                    paginationExtended.SearchBy
                );

                Pagination paginationBase = PagingUtilities.Pagination(paginationExtended.CurrentPage, paginationExtended.RecordsPerPage, clientDtos, clientDtosForPagination);

                ClientIndexViewModel viewModel = new ClientIndexViewModel
                {
                    Clients = clientDtos,
                    Pagination = _mapper.Map<Pagination, PaginationExtended>(paginationBase, paginationExtended)
                };

                return View(viewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        public ActionResult NewView()
        {
            return View();
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Add(ClientSaveDto clientSaveDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    NewClientViewModel viewModel = new NewClientViewModel { ClientTypeChoice = clientSaveDto.ClientType };
                    return View("NewView", viewModel);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ClientDto clientDto = await _clientService.AddClientAsync(clientSaveDto, userFullName);

                TempData["Success"] = ClientAlertMessages.clientAddSuccessMessage;
                return RedirectToAction("DetailsView", "Client", new { id = clientDto.Id });
            }
            catch (ClientExistsException clientExistsException)
            {
                if (clientSaveDto.ClientType == ClientTypeEnum.IndividualClient)
                {
                    ModelState.AddModelError(ClientExceptionMessages.PeselKey, clientExistsException.Message);

                    NewClientViewModel viewModel = _clientControllerHelper.GetNewClientViewModelAndLogExceptionOnAddMethod(clientExistsException, this.ControllerContext, clientSaveDto);

                    return View("NewView", viewModel);
                }
                else
                {
                    ModelState.AddModelError(ClientExceptionMessages.NipKey, clientExistsException.Message);

                    NewClientViewModel viewModel = _clientControllerHelper.GetNewClientViewModelAndLogExceptionOnAddMethod(clientExistsException, this.ControllerContext, clientSaveDto);

                    return View("NewView", viewModel);
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        [CustomAuthorize(Roles = RoleConstants.ReadonlyRole)]
        [HttpGet]
        [Route("Details/{id}")]
        public async Task<ActionResult> DetailsView(int id)
        {
            try
            {
                ClientDto clientDto = await _clientService.GetClientAsync(id);

                AddressDto addressDto = await _addressService.GetAddressWithPredicate(a => a.ClientId == id && a.Type == AddressType.Main);

                if (clientDto.ClientType == ClientTypeEnum.IndividualClient)
                {
                    IndividualClientDetailsViewModel viewModel = _mapper.Map<ClientDto, IndividualClientDetailsViewModel>(clientDto);

                    if (addressDto != null)
                        _mapper.Map<AddressDto, IndividualClientDetailsViewModel>(addressDto, viewModel);

                    return View("IndividualClientDetailsView", viewModel);
                }
                else if (clientDto.ClientType == ClientTypeEnum.BusinessClient)
                {
                    BusinessClientDetailsViewModel viewModel = _mapper.Map<ClientDto, BusinessClientDetailsViewModel>(clientDto);

                    if (addressDto != null)
                        _mapper.Map<AddressDto, BusinessClientDetailsViewModel>(addressDto, viewModel);

                    return View("BusinessClientDetailsView", viewModel);
                }
                else
                {
                    throw new ClientTypeNotFoundException(ClientExceptionMessages.ClientTypeNotFoundExceptionMessage, ControllerNameConstants.ClientController, ControllerActionNameConstants.ClientControllerDetailsView);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpGet]
        public async Task<ActionResult> EditView(int id)
        {
            try
            {
                ClientDto clientDto = await _clientService.GetClientAsync(id);

                if (clientDto.ClientType == ClientTypeEnum.IndividualClient)
                {
                    IndividualClientEditViewModel viewModel = _mapper.Map<ClientDto, IndividualClientEditViewModel>(clientDto);
                    return View("IndividualClientEditView", viewModel);
                }
                else if (clientDto.ClientType == ClientTypeEnum.BusinessClient)
                {
                    BusinessClientEditViewModel viewModel = _mapper.Map<ClientDto, BusinessClientEditViewModel>(clientDto);
                    return View("BusinessClientEditView", viewModel);
                }
                else
                {
                    throw new ClientTypeNotFoundException(ClientExceptionMessages.ClientTypeNotFoundExceptionMessage, ControllerNameConstants.ClientController, ControllerActionNameConstants.ClientControllerEditView);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.EmployeeRole)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Update(ClientUpdateDto clientUpdateDto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    if (clientUpdateDto.ClientType == ClientTypeEnum.IndividualClient)
                    {
                        IndividualClientEditViewModel viewModel = new IndividualClientEditViewModel { Name = clientUpdateDto.Name ,ClientType = clientUpdateDto.ClientType  };
                        return View("IndividualClientEditView", viewModel);
                    }                       
                    else if (clientUpdateDto.ClientType == ClientTypeEnum.BusinessClient)
                    {
                        BusinessClientEditViewModel viewModel = new BusinessClientEditViewModel { Name = clientUpdateDto.Name, ClientType = clientUpdateDto.ClientType };
                        return View("BusinessClientEditView", viewModel);
                    }    
                    else
                        throw new ClientTypeNotFoundException(ClientExceptionMessages.ClientTypeNotFoundExceptionMessage, ControllerNameConstants.ClientController, ControllerActionNameConstants.ClientControllerUpdate);
                }

                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;
                
                ClientDto clientDto = await _clientService.UpdateClientAsync(clientUpdateDto, userFullName);

                TempData["Success"] = ClientAlertMessages.clientUpdateSuccessMessage;
                return RedirectToAction("DetailsView", new { id = clientDto.Id });

            }
            catch (ClientExistsException clientExistsException)
            {
                if (clientUpdateDto.ClientType == ClientTypeEnum.IndividualClient)
                {
                    ModelState.AddModelError(ClientExceptionMessages.PeselKey, clientExistsException.Message);

                    IndividualClientEditViewModel viewModel = _clientControllerHelper.GetIndividualClientEditViewModelAndLogExceptionOnUpdateMethod(clientExistsException, this.ControllerContext, clientUpdateDto);

                    return View("IndividualClientEditView", viewModel);
                }
                else
                {
                    ModelState.AddModelError(ClientExceptionMessages.NipKey, clientExistsException.Message);

                    BusinessClientEditViewModel viewModel = _clientControllerHelper.GetBusinessClientEditViewModelAndLogExceptionOnUpdateMethod(clientExistsException, this.ControllerContext, clientUpdateDto);

                    return View("BusinessClientEditView", viewModel);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [CustomAuthorize(Roles = RoleConstants.AdministratorRole)]
        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                ClientDto clientDto = await _clientService.DeleteClientAsync(id);

                TempData["Success"] = ClientAlertMessages.clientDeleteSuccessMessage;
                return RedirectToAction("IndexView", "Client");
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception.GetType().IsSubclassOf(typeof(ApplicationCustomException)))
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException) filterContext.Exception;
                exception.SetControllerAndExceptionProperties
                    (
                        filterContext.RouteData.Values["controller"].ToString(),
                        filterContext.RouteData.Values["action"].ToString(),
                        ControllerType.MVC.ToString(),
                        filterContext.Exception.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = exception;
                filterContext.Result = RedirectToAction("ErrorPageView", "ApplicationError");
            }
            else
            {
                string userFullName = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.User.Identity.GetUserId()).Result.FullName;

                ApplicationErrorSaveDto applicationErrorSaveDto = new ApplicationErrorSaveDto
                {
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    ControllerActionName = filterContext.RouteData.Values["action"].ToString(),
                    ControllerType = ControllerType.MVC.ToString(),
                    ExceptionType = filterContext.Exception.GetType().Name,
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    Message = filterContext.Exception.Message
                };

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                filterContext.ExceptionHandled = true;
                TempData["ErrorPageData"] = filterContext.Exception;
                filterContext.Result = this.RedirectToAction("ErrorPageView", "ApplicationError");
            }
        }
    }
}