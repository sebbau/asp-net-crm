﻿using ASP_NET_CRM.Interfaces.Repositories;
using ASP_NET_CRM.Models;
using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Repositories
{
    public class AddressRepository : Repository<Address>, IAddressRepository
    {
        private readonly ApplicationDbContext _context;

        public AddressRepository(ApplicationDbContext context)
            : base(context)
        {
            _context = context;
        }

        public IQueryable<Address> GetAddresses()
        {
            return _context.Addresses;
        }

        public async Task<IEnumerable<Address>> GetAddressWithPaginationAndSearchAsync(IQueryable<Address> addressesQuery, Expression<Func<Address, int>> orderPredicate, int recordsToSkip, int recordsToTake, params Expression<Func<Address, object>>[] includes)
        {
            IQueryable<Address> query = addressesQuery;

            foreach (Expression<Func<Address, object>> include in includes)
            {
                query = query.Include(include);
            }

            return await query.OrderBy(orderPredicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }
    }
}