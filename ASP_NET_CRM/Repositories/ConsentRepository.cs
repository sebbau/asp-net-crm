﻿using ASP_NET_CRM.Interfaces.Repositories;
using ASP_NET_CRM.Models;
using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Repositories
{
    public class ConsentRepository : Repository<Consent>, IConsentRepository
    {
        public ConsentRepository(ApplicationDbContext context)
            : base(context)
        {}
    }
}