﻿using ASP_NET_CRM.Interfaces.Repositories;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Repositories.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;

        public IClientRepository Clients { get; }
        
        public IConsentRepository Consents { get; }

        public IInteractionRepository Interactions { get; }

        public IInteractionTypeRepository InteractionTypes { get; }

        public IInteractionClassRepository InteractionClasses { get; }

        public IApplicationErrorRepository ApplicationErrors { get; }

        public IAddressPrefixRepository AddressPrefixes { get; }

        public IAddressRepository Addresses { get; }

        public INoteRepository Notes { get; }

        public IRoleRepository Roles { get; }

        public IUserRepository Users { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            Clients = new ClientRepository(_context);
            Consents = new ConsentRepository(_context);
            Interactions = new InteractionRepository(_context);
            InteractionTypes = new InteractionTypeRepository(_context);
            InteractionClasses = new InteractionClassRepository(_context);
            ApplicationErrors = new ApplicationErrorRepository(_context);
            AddressPrefixes = new AddressPrefixRepository(_context);
            Addresses = new AddressRepository(_context);
            Notes = new NoteRepository(_context);
            Roles = new RoleRepository(_context);
            Users = new UserRepository(_context);
        }

        public async Task<int> SaveChanges()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}