﻿using ASP_NET_CRM.Interfaces.Repositories;
using ASP_NET_CRM.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        private readonly IDbSet<IdentityRole> _roles;

        public RoleRepository(ApplicationDbContext context)
        {
            _roles = context.Roles;
        }

        public async Task<IEnumerable<IdentityRole>> GetAllRolesAsync()
        {
            return await _roles.ToListAsync();
        }

        public async Task<IEnumerable<IdentityRole>> GetRecordsWithPaginationAsync(Expression<Func<IdentityRole, string>> orderPredicate, int recordsToSkip, int recordsToTake)
        {
            return await _roles.OrderBy(orderPredicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }

        public async Task<IdentityRole> GetRoleAsync(string roleId)
        {
            return await _roles.FirstOrDefaultAsync(r => r.Id.Equals(roleId));
        }
    }
}