﻿using ASP_NET_CRM.Interfaces.Repositories;
using ASP_NET_CRM.Models;
using ASP_NET_CRM.Models.Entities;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Repositories
{
    public class ClientRepository : Repository<Client>, IClientRepository
    {
        public ClientRepository(ApplicationDbContext context)
            : base(context)
        {}
    }
}