﻿using ASP_NET_CRM.Interfaces.Repositories;
using ASP_NET_CRM.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IDbSet<ApplicationUser> _users;

        public UserRepository(ApplicationDbContext context)
        {
            _users = context.Users;
        }

        public async Task<IEnumerable<ApplicationUser>> GetRecordsWithPaginationAndSearchAsync(Expression<Func<ApplicationUser, string>> orderPredicate, Expression<Func<ApplicationUser, bool>> searchPredicate, int recordsToSkip, int recordsToTake)
        {
            return await _users.Where(searchPredicate).OrderBy(orderPredicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }

        public async Task<IEnumerable<ApplicationUser>> GetRecordsWithPaginationAsync(Expression<Func<ApplicationUser, string>> predicate, int recordsToSkip, int recordsToTake)
        {
            return await _users.OrderBy(predicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }

        public async Task<ApplicationUser> GetUserWithIncludesAsync(Expression<Func<ApplicationUser, bool>> predicate, params Expression<Func<ApplicationUser, object>>[] includes)
        {
            IQueryable<ApplicationUser> query = _users;

            foreach (Expression<Func<ApplicationUser, object>> include in includes)
            {
                query = query.Include(include);
            }

            return await query.FirstOrDefaultAsync(predicate);
        }
    }
}