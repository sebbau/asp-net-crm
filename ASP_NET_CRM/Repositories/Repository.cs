﻿using ASP_NET_CRM.Interfaces.Repositories;
using ASP_NET_CRM.Models;
using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly DbSet<TEntity> _entity;

        public Repository(ApplicationDbContext context)
        {
            _entity = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _entity.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _entity.AddRange(entities);
        }

        public async Task<TEntity> GetEntityAsync(int entityId)
        {
            return await _entity.FindAsync(entityId);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _entity.ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllWhereAsync(Expression<Func<TEntity, bool>> searchPredicate)
        {
            return await _entity.Where(searchPredicate).ToListAsync();
        }

        public async Task<TEntity> GetEntityWithIncludesAsync(Expression<Func<TEntity, bool>> searchPredicate, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _entity.AsQueryable();

            foreach (Expression<Func<TEntity, object>> include in includes)
            {
                query = query.Include(include);
            }

            return await query.FirstOrDefaultAsync(searchPredicate);
        }

        public async Task<IEnumerable<TEntity>> GetRecordsWithPaginationAsync(Expression<Func<TEntity, int>> orderPredicate, int recordsToSkip, int recordsToTake)
        {
            return await _entity.OrderBy(orderPredicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetRecordsWithPaginationAsync(Expression<Func<TEntity, int>> orderPredicate, int recordsToSkip, int recordsToTake, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _entity.AsQueryable();

            foreach (Expression<Func<TEntity, object>> include in includes)
            {
                query = query.Include(include);
            }

            return await query.OrderBy(orderPredicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetRecordsWithPaginationAndOrderByDescendingAsync(Expression<Func<TEntity, int>> orderPredicate, int recordsToSkip, int recordsToTake)
        {
            return await _entity.OrderByDescending(orderPredicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetRecordsWithWhereAndPaginationAndOrderByDescendingAsync(Expression<Func<TEntity, bool>> searchPredicate, Expression<Func<TEntity, int>> orderPredicate, int recordsToSkip, int recordsToTake)
        {
            return await _entity.Where(searchPredicate).OrderByDescending(orderPredicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetRecordsWithPaginationAndSearchAsync(Expression<Func<TEntity, int>> orderPredicate, Expression<Func<TEntity, bool>> searchPredicate, int recordsToSkip, int recordsToTake)
        {
            return await _entity.Where(searchPredicate).OrderBy(orderPredicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetRecordsWithPaginationAndSearchAsync(Expression<Func<TEntity, int>> orderPredicate, Expression<Func<TEntity, bool>> searchPredicate, int recordsToSkip, int recordsToTake, params Expression<Func<TEntity, object>>[] includes)
        {
            IQueryable<TEntity> query = _entity.AsQueryable();

            foreach (Expression<Func<TEntity, object>> include in includes)
            {
                query = query.Include(include);
            }

            return await query.Where(searchPredicate).OrderBy(orderPredicate).Skip(recordsToSkip).Take(recordsToTake).ToListAsync();
        }

        public void Remove(TEntity entity)
        {
            _entity.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _entity.RemoveRange(entities);
        }

        public async Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> searchPredicate)
        {
            return await _entity.SingleOrDefaultAsync(searchPredicate);
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> searchPredicate)
        {
            return await _entity.FirstOrDefaultAsync(searchPredicate);
        }
    }
}