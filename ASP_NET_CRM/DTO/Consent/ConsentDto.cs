﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Consent
{
    public class ConsentDto : DetailsDto
    {
        public string Name { get; set; }

        public string FullName { get; set; }
    }
}