﻿using ASP_NET_CRM.Validations.DTO.Consent;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Consent
{
    [Validator(typeof(ConsentClientUpdateDtoValidator))]
    public class ConsentClientUpdateDto : Dto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string FullName { get; set; }

        public bool Selected { get; set; }
    }
}