﻿using ASP_NET_CRM.Validations.DTO.Consent;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Consent
{
    [Validator(typeof(ConsentSaveDtoValidator))]
    public class ConsentSaveDto : Dto
    {
        public string Name { get; set; }

        public string FullName { get; set; }
    }
}