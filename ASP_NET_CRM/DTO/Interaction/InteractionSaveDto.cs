﻿using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Validations.DTO.Interaction;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Interaction
{
    [Validator(typeof(InteractionSaveDtoValidator))]
    public class InteractionSaveDto : Dto
    {
        public int ClientId { get; set; }

        public int TypeId { get; set; }

        public int ClassId { get; set; }

        public DateTime InteractionDate { get; set; }

        public string Description { get; set; }
    }
}