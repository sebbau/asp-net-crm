﻿using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Validations.DTO.Interaction;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Interaction
{
    [Validator(typeof(InteractionUpdateDtoValidator))]
    public class InteractionUpdateDto : Dto
    {
        public int Id { get; set; }

        public InteractionStatusEnum Status { get; set; }

        public string Description { get; set; }
    }
}