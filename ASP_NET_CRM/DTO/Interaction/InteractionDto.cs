﻿using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Interaction
{
    public class InteractionDto : DetailsDto
    {
        public string Type { get; set; }

        public string Class { get; set; }

        public InteractionStatusEnum Status { get; set; }

        public DateTime InteractionDate { get; set; }

        public string Description { get; set; }

        public virtual ClientDto Client { get; set; }
    }
}