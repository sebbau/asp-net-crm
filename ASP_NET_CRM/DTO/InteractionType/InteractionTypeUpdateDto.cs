﻿using ASP_NET_CRM.Validations.DTO.InteractionType;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.InteractionType
{
    [Validator(typeof(InteractionTypeUpdateDtoValidator))]
    public class InteractionTypeUpdateDto : Dto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}