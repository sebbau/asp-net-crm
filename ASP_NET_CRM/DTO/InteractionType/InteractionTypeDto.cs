﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.InteractionType
{
    public class InteractionTypeDto : DetailsDto
    {
        public string Name { get; set; }
    }
}