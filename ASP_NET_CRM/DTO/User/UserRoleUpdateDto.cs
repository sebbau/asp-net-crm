﻿using ASP_NET_CRM.Validations.DTO.User;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.User
{
    [Validator(typeof(UserRoleUpdateDtoValidator))]
    public class UserRoleUpdateDto : Dto
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public bool Selected { get; set; }
    }
}