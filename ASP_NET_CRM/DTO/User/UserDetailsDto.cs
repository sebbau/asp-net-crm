﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.User
{
    public class UserDetailsDto : Dto
    {
        public string Id { get; set; }

        public DateTime Created { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? LastUpdated { get; set; }

        public string LastUpdatedBy { get; set; }
    }
}