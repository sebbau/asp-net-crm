﻿using ASP_NET_CRM.Validations.DTO.User;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.User
{
    [Validator(typeof(UserUpdateDtoValidator))]
    public class UserUpdateDto : Dto
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string FullName { get; set; }
    }
}