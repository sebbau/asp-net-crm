﻿using ASP_NET_CRM.DTO.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.User
{
    public class UserDto : UserDetailsDto
    {
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string FullName { get; set; }

        public IEnumerable<string> RolesId { get; set; }
    }
}