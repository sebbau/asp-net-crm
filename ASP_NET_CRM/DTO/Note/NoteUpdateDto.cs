﻿using ASP_NET_CRM.Validations.DTO.Note;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Note
{
    [Validator(typeof(NoteUpdateDtoValidator))]
    public class NoteUpdateDto : Dto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}