﻿using ASP_NET_CRM.Validations.DTO.Note;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Note
{
    [Validator(typeof(NoteSaveDtoValidator))]
    public class NoteSaveDto : Dto
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public int ClientId { get; set; }
    }
}