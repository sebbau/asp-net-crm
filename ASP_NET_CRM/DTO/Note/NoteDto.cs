﻿using ASP_NET_CRM.DTO.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Note
{
    public class NoteDto : DetailsDto
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public virtual ClientDto Client { get; set; }
    }
}