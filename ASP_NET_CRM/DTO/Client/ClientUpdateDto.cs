﻿using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Validations.DTO.Client;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Client
{
    [Validator(typeof(ClientUpdateDtoValidator))]
    public class ClientUpdateDto : Dto
    {
        public int Id { get; set; }

        public ClientTypeEnum ClientType { get; set; }

        public string Name { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Pesel { get; set; }

        public string NIP { get; set; }

        public string Regon { get; set; }

        public string KRS { get; set; }

        public string WebSite { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public ClientDocumentType? DocumentType { get; set; }

        public string DocumentNumber { get; set; }

        public DateTime? DocumentIssueDate { get; set; }

        public string DocumentIssuedBy { get; set; }
    }
}