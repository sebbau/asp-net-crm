﻿using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.DTO.Consent;
using ASP_NET_CRM.DTO.Interaction;
using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Client
{
    public class ClientDto : DetailsDto
    {
        public ClientTypeEnum ClientType { get; set; }

        public string Name { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Pesel { get; set; }

        public string NIP { get; set; }

        public string Regon { get; set; }

        public string KRS { get; set; }

        public string WebSite { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string DocumentNumber { get; set; }

        public DateTime? DocumentIssueDate { get; set; }

        public string DocumentIssuedBy { get; set; }

        public ClientDocumentType DocumentType { get; set; }

        public virtual ICollection<ConsentDto> Consents { get; set; }

        public virtual ICollection<InteractionDto> Interactions { get; set; }

        public virtual ICollection<AddressDto> Addresses { get; set; }

        public virtual ICollection<NoteDto> Notes { get; set; }
    }
}