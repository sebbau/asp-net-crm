﻿using ASP_NET_CRM.Validations.DTO.InteractionClass;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.InteractionClass
{
    [Validator(typeof(InteractionClassUpdateDtoValidator))]
    public class InteractionClassUpdateDto : Dto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}