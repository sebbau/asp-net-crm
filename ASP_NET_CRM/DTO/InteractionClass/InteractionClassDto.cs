﻿using ASP_NET_CRM.DTO.InteractionType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.InteractionClass
{
    public class InteractionClassDto : DetailsDto
    {
        public string Name { get; set; }

        public InteractionTypeDto InteractionType { get; set; }

        public int InteractionTypeId { get; set; }
    }
}