﻿using ASP_NET_CRM.Validations.DTO.InteractionClass;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.InteractionClass
{
    [Validator(typeof(InteractionClassSaveDtoValidator))]
    public class InteractionClassSaveDto
    {
        public string Name { get; set; }

        public int InteractionTypeId { get; set; }
    }
}