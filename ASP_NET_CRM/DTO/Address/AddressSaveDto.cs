﻿using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Validations.DTO.Address;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Address
{
    [Validator(typeof(AddressSaveDtoValidator))]
    public class AddressSaveDto : Dto
    {
        public AddressType Type { get; set; }

        public int? PrefixId { get; set; }

        public string PrefixValue { get; set; }

        public string StreetName { get; set; }

        public string HouseNumber { get; set; }

        public string ApartmentNumber { get; set; }

        public string FullAddressName { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public AddressStateEnum State { get; set; }

        public string Description { get; set; }

        public int ClientId { get; set; }
    }
}