﻿using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Address
{
    public class AddressLightweightDto : Dto
    {
        public int Id { get; set; }

        public AddressType Type { get; set; }

        public string FullAddressName { get; set; }
    }
}