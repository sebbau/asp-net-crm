﻿using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Address
{
    public class AddressDto : DetailsDto
    {
        public AddressType Type { get; set; }

        public string Prefix { get; set; }

        public string StreetName { get; set; }

        public string HouseNumber { get; set; }

        public string ApartmentNumber { get; set; }

        public string FullAddressName { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public AddressStateEnum State { get; set; }

        public string Description { get; set; }

        public virtual ClientDto Client { get; set; }
    }
}