﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.Role
{
    public class RoleDto : DetailsDto
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}