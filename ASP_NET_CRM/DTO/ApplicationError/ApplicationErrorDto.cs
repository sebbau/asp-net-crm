﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ASP_NET_CRM.DTO.ApplicationError
{
    public class ApplicationErrorDto : DetailsDto
    {
        public string ControllerName { get; set; }

        public string ControllerActionName { get; set; }

        public string ControllerType { get; set; }

        public string ExceptionType { get; set; }

        public string ClassName { get; set; }

        public string ActionName { get; set; }

        public HttpStatusCode HttpStatusCode { get; set; }

        public string Message { get; set; }
    }
}