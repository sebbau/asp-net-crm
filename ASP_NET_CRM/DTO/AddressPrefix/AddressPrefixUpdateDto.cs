﻿using ASP_NET_CRM.Validations.DTO.AddressPrefix;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.AddressPrefix
{
    [Validator(typeof(AddressPrefixUpdateDtoValidator))]
    public class AddressPrefixUpdateDto : Dto
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}