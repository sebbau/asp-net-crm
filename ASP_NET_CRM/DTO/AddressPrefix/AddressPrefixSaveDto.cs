﻿using ASP_NET_CRM.Validations.DTO.AddressPrefix;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.AddressPrefix
{
    [Validator(typeof(AddressPrefixSaveDtoValidator))]
    public class AddressPrefixSaveDto : Dto
    {
        public string Name { get; set; }
    }
}