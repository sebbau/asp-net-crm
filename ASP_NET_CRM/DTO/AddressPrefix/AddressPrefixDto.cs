﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.DTO.AddressPrefix
{
    public class AddressPrefixDto : DetailsDto
    {
        public string Name { get; set; }
    }
}