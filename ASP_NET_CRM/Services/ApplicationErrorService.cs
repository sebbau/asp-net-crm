﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.ApplicationError;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.Exceptions.ApplicationError;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Repositories.UnitOfWork;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class ApplicationErrorService : IApplicationErrorService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public ApplicationErrorService(IUnitOfWork context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<IEnumerable<ApplicationErrorDto>> GetApplicationErrorsAsync(int recordsToSkip, int recordsToTake)
        {
            try
            {
                IEnumerable<ApplicationError> applicationErrors = await _context.ApplicationErrors.GetRecordsWithPaginationAndOrderByDescendingAsync(ae => ae.Id, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<ApplicationError>, IEnumerable<ApplicationErrorDto>>(applicationErrors);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ApplicationErrorDto>> GetApplicationErrorsForPaginationAsync(int currentPage, int recordsPerPage)
        {
            try
            {
                int constSkipToIntRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetApplicationErrorsAsync(currentPage * recordsPerPage, recordsPerPage * constSkipToIntRecords);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ApplicationErrorDto> GetApplicationErrorAsync(int applicationErrorId)
        {
            try
            {
                ApplicationError applicationError = await _context.ApplicationErrors.GetEntityAsync(applicationErrorId);

                if (applicationError == null)
                    throw new ApplicationErrorNotFoundException(
                        ApplicationErrorExceptionMessages.ApplicationErrorNotFoundByIdExceptionMessage, 
                        ServiceNameConstants.ApplicationErrorService, 
                        ServiceActionNameConstants.ApplicationErrorServiceGetApplicationErrorAsync
                    );

                return _mapper.Map<ApplicationError, ApplicationErrorDto>(applicationError);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AddApplicationError(ApplicationErrorSaveDto applicationErrorSaveDto, string userFullName)
        {
            try
            {
                SubStringAllApplicationErrorSaveDtoProperties(ref applicationErrorSaveDto);

                ApplicationError applicationError = _mapper.Map<ApplicationErrorSaveDto, ApplicationError>(applicationErrorSaveDto);

                EntityUtilities.SetCreatedAndCreatedByInEntity(ref applicationError, userFullName);

                _context.ApplicationErrors.Add(applicationError);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SubStringAllApplicationErrorSaveDtoProperties(ref ApplicationErrorSaveDto applicationErrorSaveDto)
        {
            if (!String.IsNullOrEmpty(applicationErrorSaveDto.ActionName))
            {
                if (applicationErrorSaveDto.ActionName.Length > 200)
                    applicationErrorSaveDto.ActionName = applicationErrorSaveDto.ActionName.Substring(0, 199);
            }

            if (!String.IsNullOrEmpty(applicationErrorSaveDto.ClassName))
            {
                if (applicationErrorSaveDto.ClassName.Length > 200)
                    applicationErrorSaveDto.ClassName = applicationErrorSaveDto.ClassName.Substring(0, 199);
            }

            if (applicationErrorSaveDto.ControllerActionName.Length > 200)
                applicationErrorSaveDto.ControllerActionName = applicationErrorSaveDto.ControllerActionName.Substring(0, 199);

            if (applicationErrorSaveDto.ControllerName.Length > 200)
                applicationErrorSaveDto.ControllerName = applicationErrorSaveDto.ControllerName.Substring(0, 199);

            if (applicationErrorSaveDto.ExceptionType.Length > 200)
                applicationErrorSaveDto.ExceptionType = applicationErrorSaveDto.ExceptionType.Substring(0, 199);

            if (applicationErrorSaveDto.Message.Length > 2500)
                applicationErrorSaveDto.Message = applicationErrorSaveDto.Message.Substring(0, 2499);
        }
    }
}