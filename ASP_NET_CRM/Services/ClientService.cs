﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.Constants.Validations.Messages.Client;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Repositories.UnitOfWork;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ASP_NET_CRM.Services
{
    public class ClientService : IClientService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public ClientService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _context = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ClientDto>> GetClientsAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy)
        {
            try
            {
                IEnumerable<Client> clients;

                if (!String.IsNullOrEmpty(searchString) && !String.IsNullOrEmpty(searchBy))
                {
                    switch (searchBy)
                    {
                        case ClientConstants.id:
                            int searchInt = SearchUtilities.ValidateSearchInt(searchString);
                            clients = await _context.Clients.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.Id == searchInt, recordsToSkip, recordsToTake);
                            break;
                        case ClientConstants.name:
                            clients = await _context.Clients.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.Name.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        case ClientConstants.nip:
                            clients = await _context.Clients.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.NIP.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        case ClientConstants.pesel:
                            clients = await _context.Clients.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.Pesel.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        default:
                            clients = Enumerable.Empty<Client>();
                            break;
                    }
                }
                else
                    clients = await _context.Clients.GetRecordsWithPaginationAsync(c => c.Id, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<Client>, IEnumerable<ClientDto>>(clients);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ClientDto> GetClientAsync(int id)
        {
            try
            {
                Client client = await _context.Clients.GetEntityAsync(id);

                if (client == null)
                {
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.ClientService, ServiceActionNameConstants.ClientServiceGetClientAsync);
                }

                return _mapper.Map<Client, ClientDto>(client);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<ClientDto> GetClientWithIncludesAsync(Expression<Func<Client, bool>> predicate, params Expression<Func<Client, object>>[] includes)
        {
            try
            {
                Client client = await _context.Clients.GetEntityWithIncludesAsync(predicate, includes);

                if (client == null)
                {
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.ClientService, ServiceActionNameConstants.ClientServiceGetClientWithIncludesAsync);
                }

                return _mapper.Map<Client, ClientDto>(client);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ClientDto>> GetClientsForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);
                return await GetClientsAsync(currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords, searchString, searchBy);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ClientDto> AddClientAsync(ClientSaveDto clientSaveDto, string userFullName)
        {
            try
            {
                Client client;

                if (clientSaveDto.ClientType == ClientTypeEnum.IndividualClient)
                    client = await _context.Clients.FirstOrDefaultAsync(c => c.Pesel == clientSaveDto.Pesel);
                else
                    client = await _context.Clients.FirstOrDefaultAsync(c => c.NIP == clientSaveDto.NIP);

                if (client != null)
                {
                    if (client.ClientType == ClientTypeEnum.IndividualClient)
                    {
                        throw new ClientExistsException(ClientExceptionMessages.PeselUniqueExceptionMessage, ServiceNameConstants.ClientService, ServiceActionNameConstants.ClientServiceAddClientAsync);
                    }
                    else if (client.ClientType == ClientTypeEnum.BusinessClient)
                    {
                        throw new ClientExistsException(ClientExceptionMessages.NipUniqueExceptionMessage, ServiceNameConstants.ClientService, ServiceActionNameConstants.ClientServiceAddClientAsync);
                    }
                    else
                    {
                        throw new ClientTypeNotFoundException(ClientExceptionMessages.ClientTypeNotFoundExceptionMessage, ServiceNameConstants.ClientService, ServiceActionNameConstants.ClientServiceAddClientAsync);
                    }
                }

                client = _mapper.Map<ClientSaveDto, Client>(clientSaveDto);
                EntityUtilities.SetCreatedAndCreatedByInEntity(ref client, userFullName);

                _context.Clients.Add(client);
                await _context.SaveChanges();

                return _mapper.Map<Client, ClientDto>(client);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ClientDto> UpdateClientAsync(ClientUpdateDto clientUpdateDto, string userFullName)
        {
            try
            {
                Client client = await _context.Clients.GetEntityAsync(clientUpdateDto.Id);

                if (client == null)
                {
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.ClientService, ServiceActionNameConstants.ClientServiceUpdateClientAsync);
                }

                Client isClientUnique;

                if (client.ClientType == ClientTypeEnum.IndividualClient)
                    isClientUnique = await _context.Clients.FirstOrDefaultAsync(c => c.Id != clientUpdateDto.Id && c.Pesel == clientUpdateDto.Pesel);
                else
                    isClientUnique = await _context.Clients.FirstOrDefaultAsync(c => c.Id != clientUpdateDto.Id && c.NIP == clientUpdateDto.NIP);

                if (isClientUnique != null)
                {
                    if (isClientUnique.ClientType == ClientTypeEnum.IndividualClient)
                    {
                        throw new ClientExistsException(ClientExceptionMessages.PeselUniqueExceptionMessage, ServiceNameConstants.ClientService, ServiceActionNameConstants.ClientServiceUpdateClientAsync);
                    }
                    else
                    {
                        throw new ClientExistsException(ClientExceptionMessages.NipUniqueExceptionMessage, ServiceNameConstants.ClientService, ServiceActionNameConstants.ClientServiceUpdateClientAsync);
                    }
                }

                _mapper.Map<ClientUpdateDto, Client>(clientUpdateDto, client);
                EntityUtilities.SetLastUpdatedAndLastUpdatedByInEntity(ref client, userFullName);

                await _context.SaveChanges();

                return _mapper.Map<Client, ClientDto>(client);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ClientDto> DeleteClientAsync(int clientId)
        {
            try
            {
                Client client = await _context.Clients.GetEntityAsync(clientId);

                if (client == null)
                {
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.ClientService, ServiceActionNameConstants.ClientServiceDeleteClientAsync);
                }

                _context.Clients.Remove(client);
                await _context.SaveChanges();

                return _mapper.Map<Client, ClientDto>(client);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}