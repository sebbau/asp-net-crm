﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.Role;
using ASP_NET_CRM.DTO.Role;
using ASP_NET_CRM.Exceptions.Role;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class RoleService : IRoleService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public RoleService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _context = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<RoleDto>> GetAllRolesAsync()
        {
            try
            {
                IEnumerable<IdentityRole> roles = await _context.Roles.GetAllRolesAsync();

                return _mapper.Map<IEnumerable<IdentityRole>, IEnumerable<RoleDto>>(roles);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<RoleDto>> GetRolesAsync(int recordsToSkip, int recordsToTake)
        {
            try
            {
                IEnumerable<IdentityRole> roles = await _context.Roles.GetRecordsWithPaginationAsync(r => r.Id, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<IdentityRole>, IEnumerable<RoleDto>>(roles);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<RoleDto>> GetRolesForPaginationAsync(int currentPage, int recordsPerPage)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetRolesAsync(currentPage * recordsPerPage - recordsPerPage, recordsPerPage);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<RoleDto> GetRoleAsync(string roleId)
        {
            try
            {
                IdentityRole role = await _context.Roles.GetRoleAsync(roleId);

                if (role == null)
                    throw new RoleNotFoundException(RoleExceptionMessages.RoleNotFoundByIdExceptionMessage, ServiceNameConstants.RoleService, ServiceActionNameConstants.RoleServiceGetRoleAsync);

                return _mapper.Map<IdentityRole, RoleDto>(role);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}