﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.InteractionClass;
using ASP_NET_CRM.Constants.Exceptions.Models.InteractionType;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.Constants.Validations.Messages.InteractionClass;
using ASP_NET_CRM.DTO.InteractionClass;
using ASP_NET_CRM.Exceptions.InteractionClass;
using ASP_NET_CRM.Exceptions.InteractionType;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class InteractionClassService : IInteractionClassService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public InteractionClassService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _context = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<InteractionClassDto>> GetAllInteractionClassesAsync()
        {
            try
            {
                IEnumerable<InteractionClass> interactionClasses = await _context.InteractionClasses.GetAllAsync();

                return _mapper.Map<IEnumerable<InteractionClass>, IEnumerable<InteractionClassDto>>(interactionClasses);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<InteractionClassDto>> GetInteractionClassesAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy, params Expression<Func<InteractionClass, object>>[] includes)
        {
            try
            {
                IEnumerable<InteractionClass> interactionClasses;

                if (!String.IsNullOrEmpty(searchString) && !String.IsNullOrEmpty(searchBy))
                {
                    switch (searchBy)
                    {
                        case InteractionClassConstants.Id:
                            int searchInt = SearchUtilities.ValidateSearchInt(searchString);
                            interactionClasses = await _context.InteractionClasses.GetRecordsWithPaginationAndSearchAsync(ic => ic.Id, ic => ic.Id == searchInt, recordsToSkip, recordsToTake, includes);
                            break;
                        case InteractionClassConstants.Type:
                            interactionClasses = await _context.InteractionClasses.GetRecordsWithPaginationAndSearchAsync(ic => ic.Id, ic => ic.InteractionType.Name.Contains(searchString), recordsToSkip, recordsToTake, includes);
                            break;
                        case InteractionClassConstants.Name:
                            interactionClasses = await _context.InteractionClasses.GetRecordsWithPaginationAndSearchAsync(ic => ic.Id, ic => ic.Name.Contains(searchString), recordsToSkip, recordsToTake, includes);
                            break;
                        default:
                            interactionClasses = Enumerable.Empty<InteractionClass>();
                            break;
                    }
                }
                else
                    interactionClasses = await _context.InteractionClasses.GetRecordsWithPaginationAsync(i => i.Id, recordsToSkip, recordsToTake, includes);

                return _mapper.Map<IEnumerable<InteractionClass>, IEnumerable<InteractionClassDto>>(interactionClasses);
            }
            catch (Exception)
            {
                throw;
            }
        }

    public async Task<IEnumerable<InteractionClassDto>> GetInteractionClassesForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy, params Expression<Func<InteractionClass, object>>[] includes)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetInteractionClassesAsync(currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords, searchString, searchBy, includes);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<InteractionClassDto>> GetInteractionClassesForInteractionTypeAsync(int interactionTypeId)
        {
            try
            {
                IEnumerable<InteractionClass> interactionClasses = await _context.InteractionClasses.GetAllWhereAsync(ic => ic.InteractionTypeId == interactionTypeId);

                return _mapper.Map<IEnumerable<InteractionClass>, IEnumerable<InteractionClassDto>>(interactionClasses);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionClassDto> GetInteractionClassWithIncludesAsync(Expression<Func<InteractionClass, bool>> predicate, params Expression<Func<InteractionClass, object>>[] includes)
        {
            try
            {
                InteractionClass interactionClass = await _context.InteractionClasses.GetEntityWithIncludesAsync(predicate, includes);

                if (interactionClass == null)
                    throw new InteractionClassNotFoundException(InteractionClassExceptionMessages.InteractionClassNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionClassService, ServiceActionNameConstants.InteractionClassServiceGetInteractionClassWithIncludesAsync);

                return _mapper.Map<InteractionClass, InteractionClassDto>(interactionClass);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionClassDto> AddInteractionClassAsync(InteractionClassSaveDto interactionClassSaveDto, string userFullName)
        {
            try
            {
                InteractionType interactionType = await _context.InteractionTypes.GetEntityAsync(interactionClassSaveDto.InteractionTypeId);

                if (interactionType == null)
                    throw new InteractionTypeNotFoundException(InteractionTypeExceptionMessages.InteractionTypeNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionClassService, ServiceActionNameConstants.InteractionClassAddInteractionClassAsync);

                InteractionClass isInteractionClassExists = await _context.InteractionClasses.SingleOrDefaultAsync(ic => ic.Name.Equals(interactionClassSaveDto.Name) && ic.InteractionTypeId == interactionClassSaveDto.InteractionTypeId);

                if (isInteractionClassExists != null)
                    throw new InteractionClassExistsException(InteractionClassExceptionMessages.InteractionClassNameUniqueExceptionMessage, ServiceNameConstants.InteractionClassService, ServiceActionNameConstants.InteractionClassAddInteractionClassAsync);               

                InteractionClass interactionClass = _mapper.Map<InteractionClassSaveDto, InteractionClass>(interactionClassSaveDto);

                EntityUtilities.SetCreatedAndCreatedByInEntity(ref interactionClass, userFullName);

                _context.InteractionClasses.Add(interactionClass);
                await _context.SaveChanges();

                return _mapper.Map<InteractionClass, InteractionClassDto>(interactionClass);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionClassDto> UpdateInteractionClassAsync(InteractionClassUpdateDto interactionClassUpdateDto, string userFullName)
        {
            InteractionClass interactionClass = await _context.InteractionClasses.GetEntityAsync(interactionClassUpdateDto.Id);

            if (interactionClass == null)
                throw new InteractionClassNotFoundException(InteractionClassExceptionMessages.InteractionClassNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionClassService, ServiceActionNameConstants.InteractionClassUpdateInteractionClassAsync);

            InteractionClass isInteractionClassExists = await _context.InteractionClasses.SingleOrDefaultAsync(ic => ic.Name.Equals(interactionClassUpdateDto.Name) && ic.InteractionTypeId == interactionClass.InteractionTypeId && ic.Id != interactionClassUpdateDto.Id);

            if (isInteractionClassExists != null)
                throw new InteractionClassExistsException(InteractionClassExceptionMessages.InteractionClassNameUniqueExceptionMessage, ServiceNameConstants.InteractionClassService, ServiceActionNameConstants.InteractionClassUpdateInteractionClassAsync);

            _mapper.Map<InteractionClassUpdateDto, InteractionClass>(interactionClassUpdateDto, interactionClass);

            EntityUtilities.SetLastUpdatedAndLastUpdatedByInEntity(ref interactionClass, userFullName);

            await _context.SaveChanges();

            return _mapper.Map<InteractionClass, InteractionClassDto>(interactionClass);
        }

        public async Task<InteractionClassDto> DeleteInteractionClassAsync(int interactionClassId)
        {
            InteractionClass interactionClass = await _context.InteractionClasses.GetEntityAsync(interactionClassId);

            if (interactionClass == null)
                throw new InteractionClassNotFoundException(InteractionClassExceptionMessages.InteractionClassNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionClassService, ServiceActionNameConstants.InteractionClassUpdateInteractionClassAsync);

            _context.InteractionClasses.Remove(interactionClass);
            await _context.SaveChanges();

            return _mapper.Map<InteractionClass, InteractionClassDto>(interactionClass);
        }
    }
}