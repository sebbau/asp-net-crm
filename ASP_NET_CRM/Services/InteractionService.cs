﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Exceptions.Models.Interaction;
using ASP_NET_CRM.Constants.Exceptions.Models.InteractionClass;
using ASP_NET_CRM.Constants.Exceptions.Models.InteractionType;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.Constants.Validations.Messages.Client;
using ASP_NET_CRM.Constants.Validations.Messages.Interaction;
using ASP_NET_CRM.Constants.Validations.Messages.InteractionClass;
using ASP_NET_CRM.Constants.Validations.Messages.InteractionType;
using ASP_NET_CRM.DTO.Interaction;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Exceptions.Interaction;
using ASP_NET_CRM.Exceptions.InteractionClass;
using ASP_NET_CRM.Exceptions.InteractionType;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class InteractionService : IInteractionService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public InteractionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _context = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<InteractionDto>> GetInteractionsAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy, params Expression<Func<Interaction, object>>[] includes)
        {
            try
            {
                IEnumerable<Interaction> interactions;

                if (!String.IsNullOrEmpty(searchString) && !String.IsNullOrEmpty(searchBy))
                {
                    switch (searchBy)
                    {
                        case InteractionConstants.Id:
                            int searchInt = SearchUtilities.ValidateSearchInt(searchString);
                            interactions = await _context.Interactions.GetRecordsWithPaginationAndSearchAsync(i => i.Id, i => i.Id == searchInt, recordsToSkip, recordsToTake, includes);
                            break;
                        case InteractionConstants.ClientName:
                            interactions = await _context.Interactions.GetRecordsWithPaginationAndSearchAsync(i => i.Id, i => i.Client.Name.Contains(searchString), recordsToSkip, recordsToTake, includes);
                            break;
                        case InteractionConstants.Type:
                            interactions = await _context.Interactions.GetRecordsWithPaginationAndSearchAsync(i => i.Id, i => i.Type.Contains(searchString), recordsToSkip, recordsToTake, includes);
                            break;
                        case InteractionConstants.Class:
                            interactions = await _context.Interactions.GetRecordsWithPaginationAndSearchAsync(i => i.Id, i => i.Class.Contains(searchString), recordsToSkip, recordsToTake, includes);
                            break;
                        default:
                            interactions = Enumerable.Empty<Interaction>();
                            break;
                    }
                }
                else
                    interactions = await _context.Interactions.GetRecordsWithPaginationAsync(i => i.Id, recordsToSkip, recordsToTake, includes);

                return _mapper.Map<IEnumerable<Interaction>, IEnumerable<InteractionDto>>(interactions);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<InteractionDto>> GetInteractionsByClientIdAndOrderByDescendingAsync(int clientId, int recordsToSkip, int recordsToTake)
        {
            try
            {
                IEnumerable<Interaction> interactions = await _context.Interactions.GetRecordsWithWhereAndPaginationAndOrderByDescendingAsync(i => i.ClientId == clientId, i => i.Id, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<Interaction>, IEnumerable<InteractionDto>>(interactions);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<InteractionDto>> GetInteractionsForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy, params Expression<Func<Interaction, object>>[] includes)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetInteractionsAsync(currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords, searchString, searchBy, includes);
            }
            catch (Exception)
            {
                throw;
            }          
        }

        public async Task<IEnumerable<InteractionDto>> GetInteractionsByClientIdAndOrderByDescendingForPaginationAsync(int clientId, int currentPage, int recordsPerPage)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetInteractionsByClientIdAndOrderByDescendingAsync(clientId, currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionDto> GetInteractionWithIncludesAsync(Expression<Func<Interaction, bool>> predicate, params Expression<Func<Interaction, object>>[] includes)
        {
            try
            {
                Interaction interaction = await _context.Interactions.GetEntityWithIncludesAsync(predicate, includes);

                if (interaction == null)
                    throw new InteractionNotFoundException(InteractionExceptionMessages.InteractionNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionService, ServiceActionNameConstants.InteractionServiceGetInteractionWithIncludesAsync);

                return _mapper.Map<Interaction, InteractionDto>(interaction);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionDto> AddInteractionAsync(InteractionSaveDto interactionSaveDto, string userFullName)
        {
            try
            {
                Client client = await _context.Clients.GetEntityAsync(interactionSaveDto.ClientId);

                if (client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionService, ServiceActionNameConstants.InteractionServiceAddInteractionAsync);

                InteractionType interactionType = await _context.InteractionTypes.GetEntityAsync(interactionSaveDto.TypeId);

                if (interactionType == null)
                    throw new InteractionTypeNotFoundException(InteractionTypeExceptionMessages.InteractionTypeNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionService, ServiceActionNameConstants.InteractionServiceAddInteractionAsync);

                InteractionClass interactionClass = await _context.InteractionClasses.GetEntityAsync(interactionSaveDto.ClassId);

                if (interactionClass == null)
                    throw new InteractionClassNotFoundException(InteractionClassExceptionMessages.InteractionClassNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionService, ServiceActionNameConstants.InteractionServiceAddInteractionAsync);

                Interaction interaction = _mapper.Map<InteractionSaveDto, Interaction>(interactionSaveDto);

                interaction.Type = interactionType.Name;
                interaction.Class = interactionClass.Name;
                interaction.Status = InteractionStatusEnum.New;

                EntityUtilities.SetCreatedAndCreatedByInEntity(ref interaction, userFullName);

                _context.Interactions.Add(interaction);
                await _context.SaveChanges();

                return _mapper.Map<Interaction, InteractionDto>(interaction);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionDto> UpdateInteractionAsync(InteractionUpdateDto interactionUpdateDto, string userFullName)
        {
            try
            {
                Interaction interaction = await _context.Interactions.GetEntityWithIncludesAsync(i => i.Id == interactionUpdateDto.Id, i => i.Client);

                if (interaction == null)
                    throw new InteractionNotFoundException(InteractionExceptionMessages.InteractionNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionService, ServiceActionNameConstants.InteractionServiceUpdateInteractionAsync);

                if (interaction.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionService, ServiceActionNameConstants.InteractionServiceUpdateInteractionAsync);

                if (interaction.Status == InteractionStatusEnum.Canceled || interaction.Status == InteractionStatusEnum.Closed)
                    throw new InteractionStatusEditException(InteractionExceptionMessages.InteractionStatusEditExceptionMessage, ServiceNameConstants.InteractionService, ServiceActionNameConstants.InteractionServiceUpdateInteractionAsync);

                _mapper.Map<InteractionUpdateDto, Interaction>(interactionUpdateDto, interaction);

                EntityUtilities.SetLastUpdatedAndLastUpdatedByInEntity(ref interaction, userFullName);

                await _context.SaveChanges();

                return _mapper.Map<Interaction, InteractionDto>(interaction);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionDto> DeleteInteractionAsync(int interactionId)
        {
            try
            {
                Interaction interaction = await _context.Interactions.GetEntityAsync(interactionId);

                if (interaction == null)
                    throw new InteractionNotFoundException(InteractionExceptionMessages.InteractionNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionService, ServiceActionNameConstants.InteractionServiceDeleteInteractionAsync);

                _context.Interactions.Remove(interaction);
                await _context.SaveChanges();

                return _mapper.Map<Interaction, InteractionDto>(interaction);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}