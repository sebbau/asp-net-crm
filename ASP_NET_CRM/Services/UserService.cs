﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.Role;
using ASP_NET_CRM.Constants.Exceptions.Models.User;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.Role;
using ASP_NET_CRM.DTO.User;
using ASP_NET_CRM.Exceptions.Role;
using ASP_NET_CRM.Exceptions.User;
using ASP_NET_CRM.Interfaces.Repositories;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationUserManager _userManager;
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public UserService(
            ApplicationUserManager userManager, 
            IUnitOfWork unitOfWork,
            IMapper mapper
            )
        {
            _userManager = userManager;
            _context = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserDto>> GetUsersAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy)
        {
            try
            {
                IEnumerable<ApplicationUser> applicationUsers;

                if (!String.IsNullOrEmpty(searchString) && !String.IsNullOrEmpty(searchBy))
                {
                    switch (searchBy)
                    {
                        case UserConstants.Email:
                            applicationUsers = await _context.Users.GetRecordsWithPaginationAndSearchAsync(u => u.Email, u => u.Email.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        case UserConstants.FirstName:
                            applicationUsers = await _context.Users.GetRecordsWithPaginationAndSearchAsync(u => u.Email, u => u.FirstName.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        case UserConstants.Surname:
                            applicationUsers = await _context.Users.GetRecordsWithPaginationAndSearchAsync(u => u.Email, u => u.Surname.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        case UserConstants.FullName:
                            applicationUsers = await _context.Users.GetRecordsWithPaginationAndSearchAsync(u => u.Email, u => u.FullName.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        default:
                            applicationUsers = Enumerable.Empty<ApplicationUser>();
                            break;
                    }
                }
                else
                    applicationUsers = await _context.Users.GetRecordsWithPaginationAsync(i => i.Email, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<UserDto>>(applicationUsers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<UserDto>> GetUsersForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetUsersAsync(currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords, searchString, searchBy);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<UserDto> GetUserAsync(string userId)
        {
            try
            {
                ApplicationUser user = await _userManager.FindByIdAsync(userId);

                if (user == null)
                    throw new UserNotFoundException(UserExceptionMessages.UserNotFoundByIdExceptionMessage, ServiceNameConstants.UserService, ServiceActionNameConstants.UserServiceGetUserAsync);

                return _mapper.Map<ApplicationUser, UserDto>(user);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<UserDto> GetUserWithIncludesAsync(Expression<Func<ApplicationUser, bool>> predicate, params Expression<Func<ApplicationUser, object>>[] includes)
        {
            try
            {
                ApplicationUser user = await _context.Users.GetUserWithIncludesAsync(predicate, includes);

                if (user == null)
                    throw new UserNotFoundException(UserExceptionMessages.UserNotFoundByIdExceptionMessage, ServiceNameConstants.UserService, ServiceActionNameConstants.UserServiceGetUserWithIncludesAsync);

                return _mapper.Map<ApplicationUser, UserDto>(user);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<UserDto> UpdateUserAsync(UserUpdateDto userUpdateDto, string userFullName)
        {
            try
            {
                ApplicationUser user = await _userManager.FindByIdAsync(userUpdateDto.Id);

                if (user == null)
                    throw new UserNotFoundException(UserExceptionMessages.UserNotFoundByIdExceptionMessage, ServiceNameConstants.UserService, ServiceActionNameConstants.UserServiceUpdateUserAsync);

                _mapper.Map<UserUpdateDto, ApplicationUser>(userUpdateDto, user);

                user.LastUpdated = DateTime.Now;
                user.LastUpdatedBy = userFullName;

                await _userManager.UpdateAsync(user);

                return _mapper.Map<ApplicationUser, UserDto>(user);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<UserDto> DeleteUserAsync(string userId)
        {
            try
            {
                ApplicationUser user = await _userManager.FindByIdAsync(userId);

                if (user == null)
                    throw new UserNotFoundException(UserExceptionMessages.UserNotFoundByIdExceptionMessage, ServiceNameConstants.UserService, ServiceActionNameConstants.UserServiceDeleteUserAsync);

                await _userManager.DeleteAsync(user);

                return _mapper.Map<ApplicationUser, UserDto>(user);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<RoleDto>> UpdateUserRolesAsync(string userId, IEnumerable<UserRoleUpdateDto> selectedRoles)
        {
            try
            {
                IEnumerable<IdentityRole> rolesFromDb = await _context.Roles.GetAllRolesAsync();

                ApplicationUser user = await _context.Users.GetUserWithIncludesAsync(u => u.Id.Equals(userId), u => u.Roles);

                foreach (UserRoleUpdateDto role in selectedRoles)
                {
                    IdentityRole identityRole = rolesFromDb.FirstOrDefault(r => r.Id.Equals(role.Id));

                    if (identityRole == null)
                        throw new RoleNotFoundException(RoleExceptionMessages.RoleNotFoundByIdExceptionMessage, ServiceNameConstants.UserService, ServiceActionNameConstants.UserServiceUpdateUserRolesAsync);
                }

                List<string> listRolesToAdd = new List<string>();
                List<string> listRolesToRemove = new List<string>();

                foreach (UserRoleUpdateDto role in selectedRoles)
                {
                    if (user.Roles.Any(ur => ur.RoleId.Equals(role.Id)))
                        continue;

                    listRolesToAdd.Add(role.Name);
                }
                
                foreach (IdentityUserRole role in user.Roles)
                {
                    if (selectedRoles.Any(r => r.Id.Equals(role.RoleId)))
                        continue;

                    listRolesToRemove.Add(rolesFromDb.First(r => r.Id.Equals(role.RoleId)).Name);
                }

                String[] rolesToAdd = listRolesToAdd.ToArray();
                String[] rolesToRemove = listRolesToRemove.ToArray();

                await _userManager.AddToRolesAsync(user.Id, rolesToAdd);
                await _userManager.RemoveFromRolesAsync(user.Id, rolesToRemove);

                return _mapper.Map<IEnumerable<UserRoleUpdateDto>, IEnumerable<RoleDto>>(selectedRoles);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}