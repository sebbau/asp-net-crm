﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.Address;
using ASP_NET_CRM.Constants.Exceptions.Models.AddressPrefix;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions.Address;
using ASP_NET_CRM.Exceptions.AddressPrefix;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class AddressService : IAddressService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public AddressService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _context = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<AddressDto>> GetAddressesAsync(int recordsToSkip, int recordsToTake, AddressPagination addressPagination, params Expression<Func<Address, object>>[] includes)
        {
            try
            {
                IEnumerable<Address> addresses;

                if (addressPagination.IsSearchBy)
                {
                    IQueryable<Address> addressesQuery = _context.Addresses.GetAddresses();

                    if (!String.IsNullOrEmpty(addressPagination.SearchId))
                    {
                        int searchInt = SearchUtilities.ValidateSearchInt(addressPagination.SearchId);
                        addressesQuery = addressesQuery.Where(a => a.Id == searchInt);
                    }

                    if (!String.IsNullOrEmpty(addressPagination.SearchStreetName))
                        addressesQuery = addressesQuery.Where(a => a.StreetName.Contains(addressPagination.SearchStreetName));

                    if (!String.IsNullOrEmpty(addressPagination.SearchHouseNumber))
                        addressesQuery = addressesQuery.Where(a => a.HouseNumber.Contains(addressPagination.SearchHouseNumber));

                    if (!String.IsNullOrEmpty(addressPagination.SearchApartmentNumber))
                        addressesQuery = addressesQuery.Where(a => a.ApartmentNumber.Contains(addressPagination.SearchApartmentNumber));

                    if (!String.IsNullOrEmpty(addressPagination.SearchCity))
                        addressesQuery = addressesQuery.Where(a => a.City.Contains(addressPagination.SearchCity));

                    if (!String.IsNullOrEmpty(addressPagination.SearchPostalCode))
                        addressesQuery = addressesQuery.Where(a => a.PostalCode.Contains(addressPagination.SearchPostalCode));

                    addresses = await _context.Addresses.GetAddressWithPaginationAndSearchAsync(addressesQuery, a => a.Id, recordsToSkip, recordsToTake, includes);
                }
                else
                    addresses = await _context.Addresses.GetRecordsWithPaginationAsync(a => a.Id, recordsToSkip, recordsToTake, includes);

                return _mapper.Map<IEnumerable<Address>, IEnumerable<AddressDto>>(addresses);
            }
            catch (Exception)
            {
                throw;
            }            
        }

        public async Task<IEnumerable<AddressDto>> GetAddressesForPaginationAsync(AddressPagination addressPagination, params Expression<Func<Address, object>>[] includes)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(addressPagination.CurrentPage);

                return await GetAddressesAsync(addressPagination.CurrentPage * addressPagination.RecordsPerPage, addressPagination.RecordsPerPage * constIntToSkipRecords, addressPagination, includes);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<AddressDto>> GetAddressesByClientIdOrderByDescendingAsync(int clientId, int recordsToSkip, int recordsToTake)
        {
            try
            {
                IEnumerable<Address> addresses = await _context.Addresses.GetRecordsWithWhereAndPaginationAndOrderByDescendingAsync(a => a.ClientId == clientId, a => a.Id, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<Address>, IEnumerable<AddressDto>>(addresses);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<AddressDto>> GetAddressesByClientIdOrderByDescendingForPaginationAsync(int clientId, int currentPage, int recordsPerPage)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetAddressesByClientIdOrderByDescendingAsync(clientId, currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressDto> GetAddressWithIncludesAsync(Expression<Func<Address, bool>> predicate, params Expression<Func<Address, object>>[] includes)
        {
            try
            {
                Address address = await _context.Addresses.GetEntityWithIncludesAsync(predicate, includes);

                if (address == null)
                    throw new AddressNotFoundException(AddressExceptionMessages.AddressNotFoundByIdExceptionMessage, ServiceNameConstants.AddressService, ServiceActionNameConstants.AddressServiceGetAddressWithIncludesAsync);

                return _mapper.Map<Address, AddressDto>(address);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressDto> GetAddressWithPredicate(Expression<Func<Address, bool>> predicate)
        {
            try
            {
                Address address = await _context.Addresses.FirstOrDefaultAsync(predicate);

                return _mapper.Map<Address, AddressDto>(address);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressDto> AddAddressAsync(AddressSaveDto addressSaveDto, string userFullName)
        {
            try
            {
                Client client = await _context.Clients.GetEntityWithIncludesAsync(c => c.Id == addressSaveDto.ClientId, c => c.Addresses);

                if (client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.AddressService, ServiceActionNameConstants.AddressServiceAddAddressAsync);

                if (addressSaveDto.Type == AddressType.Main && client.Addresses.Any(a => a.Type == AddressType.Main))
                    throw new AddressMainForClientExistsException(AddressExceptionMessages.AddressMainForClientExistsExceptionMessage, ServiceNameConstants.AddressService, ServiceActionNameConstants.AddressServiceAddAddressAsync);

                Address address = _mapper.Map<AddressSaveDto, Address>(addressSaveDto);

                if (addressSaveDto.PrefixId != 0 && addressSaveDto.PrefixId != null)
                {
                    AddressPrefix addressPrefix = await _context.AddressPrefixes.GetEntityAsync((int) addressSaveDto.PrefixId);

                    if (addressPrefix == null)
                        throw new AddressPrefixNotFoundException(AddressPrefixExceptionMessages.AddressPrefixNotFoundByIdExceptionMessages, ServiceNameConstants.AddressService, ServiceActionNameConstants.AddressServiceAddAddressAsync);

                    address.Prefix = addressPrefix.Name;
                }

                EntityUtilities.SetCreatedAndCreatedByInEntity(ref address, userFullName);

                _context.Addresses.Add(address);
                await _context.SaveChanges();

                return _mapper.Map<Address, AddressDto>(address);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressDto> UpdateAddressAsync(AddressUpdateDto addressUpdateDto, string userFullName)
        {
            try
            {
                Address address = await _context.Addresses.GetEntityWithIncludesAsync(a => a.Id == addressUpdateDto.Id, a => a.Client);

                if (address == null)
                    throw new AddressNotFoundException(AddressExceptionMessages.AddressNotFoundByIdExceptionMessage, ServiceNameConstants.AddressService, ServiceActionNameConstants.AddressServiceUpdateAddressAsync);

                if (address.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.AddressService, ServiceActionNameConstants.AddressServiceUpdateAddressAsync);

                _mapper.Map<AddressUpdateDto, Address>(addressUpdateDto, address);

                if (addressUpdateDto.PrefixId != 0 && addressUpdateDto.PrefixId != null)
                {
                    AddressPrefix addressPrefix = await _context.AddressPrefixes.GetEntityAsync((int)addressUpdateDto.PrefixId);

                    if (addressPrefix == null)
                        throw new AddressPrefixNotFoundException(AddressPrefixExceptionMessages.AddressPrefixNotFoundByIdExceptionMessages, ServiceNameConstants.AddressService, ServiceActionNameConstants.AddressServiceUpdateAddressAsync);

                    address.Prefix = addressPrefix.Name;
                }
                else
                    address.Prefix = String.Empty;

                EntityUtilities.SetLastUpdatedAndLastUpdatedByInEntity(ref address, userFullName);

                await _context.SaveChanges();

                return _mapper.Map<Address, AddressDto>(address);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressDto> DeleteAddressAsync(int addressId)
        {
            try
            {
                Address address = await _context.Addresses.GetEntityAsync(addressId);

                if (address == null)
                    throw new AddressNotFoundException(AddressExceptionMessages.AddressNotFoundByIdExceptionMessage, ServiceNameConstants.AddressService, ServiceActionNameConstants.AddressServiceDeleteAddressAsync);

                _context.Addresses.Remove(address);
                await _context.SaveChanges();

                return _mapper.Map<Address, AddressDto>(address);
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}