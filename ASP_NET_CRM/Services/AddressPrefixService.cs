﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.AddressPrefix;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.AddressPrefix;
using ASP_NET_CRM.Exceptions.AddressPrefix;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class AddressPrefixService : IAddressPrefixService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public AddressPrefixService(IUnitOfWork context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<AddressPrefixDto>> GetAllAddressPrefixes()
        {
            try
            {
                IEnumerable<AddressPrefix> addressPrefixes = await _context.AddressPrefixes.GetAllAsync();

                return _mapper.Map<IEnumerable<AddressPrefix>, IEnumerable<AddressPrefixDto>>(addressPrefixes);
            }
            catch
            {
                throw;
            }
        }

        public async Task<IEnumerable<AddressPrefixDto>> GetAddressPrefixesAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy)
        {
            try
            {
                IEnumerable<AddressPrefix> addressPrefixes;

                if (!String.IsNullOrEmpty(searchString) && !String.IsNullOrEmpty(searchBy))
                {
                    switch (searchBy)
                    {
                        case AddressPrefixConstants.Id:
                            int searchInt = SearchUtilities.ValidateSearchInt(searchString);
                            addressPrefixes = await _context.AddressPrefixes.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.Id == searchInt, recordsToSkip, recordsToTake);
                            break;
                        case AddressPrefixConstants.Name:
                            addressPrefixes = await _context.AddressPrefixes.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.Name.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        default:
                            addressPrefixes = Enumerable.Empty<AddressPrefix>();
                            break;
                    }
                }
                else
                    addressPrefixes = await _context.AddressPrefixes.GetRecordsWithPaginationAsync(i => i.Id, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<AddressPrefix>, IEnumerable<AddressPrefixDto>>(addressPrefixes);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<AddressPrefixDto>> GetAddressPrefixesForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetAddressPrefixesAsync(currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords, searchString, searchBy);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressPrefixDto> GetAddressPrefixAsync(int addressPrefixId)
        {
            try
            {
                AddressPrefix addressPrefix = await _context.AddressPrefixes.GetEntityAsync(addressPrefixId);

                if (addressPrefix == null)
                    throw new AddressPrefixNotFoundException(AddressPrefixExceptionMessages.AddressPrefixNotFoundByIdExceptionMessages, ServiceNameConstants.AddressPrefixService, ServiceActionNameConstants.AddressPrefixServiceGetAddressPrefixAsync);

                return _mapper.Map<AddressPrefix, AddressPrefixDto>(addressPrefix);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressPrefixDto> AddAddressPrefixAsync(AddressPrefixSaveDto addressPrefixSaveDto, string userFullName)
        {
            try
            {
                AddressPrefix isAddressPrefixExists = await _context.AddressPrefixes.SingleOrDefaultAsync(c => c.Name.Equals(addressPrefixSaveDto.Name));

                if (isAddressPrefixExists != null)
                    throw new AddressPrefixExistsException(AddressPrefixExceptionMessages.AddressPrefixNameUniqueExceptionMessage, ServiceNameConstants.AddressPrefixService, ServiceActionNameConstants.AddressPrefixServiceAddAddressPrefixAsync);

                AddressPrefix addressPrefix = _mapper.Map<AddressPrefixSaveDto, AddressPrefix>(addressPrefixSaveDto);

                EntityUtilities.SetCreatedAndCreatedByInEntity(ref addressPrefix, userFullName);

                _context.AddressPrefixes.Add(addressPrefix);
                await _context.SaveChanges();

                return _mapper.Map<AddressPrefix, AddressPrefixDto>(addressPrefix);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressPrefixDto> UpdateAddressPrefixAsync(AddressPrefixUpdateDto addressPrefixUpdateDto, string userFullName)
        {
            try
            {
                AddressPrefix addressPrefix = await _context.AddressPrefixes.GetEntityAsync(addressPrefixUpdateDto.Id);

                if (addressPrefix == null)
                    throw new AddressPrefixNotFoundException(AddressPrefixExceptionMessages.AddressPrefixNotFoundByIdExceptionMessages, ServiceNameConstants.AddressPrefixService, ServiceActionNameConstants.AddressPrefixServiceUpdateAddressPrefixAsync);

                AddressPrefix isAddressPrefixExists = await _context.AddressPrefixes.SingleOrDefaultAsync(c => c.Name.Equals(addressPrefixUpdateDto.Name) && c.Id != addressPrefixUpdateDto.Id);

                if (isAddressPrefixExists != null)
                    throw new AddressPrefixExistsException(AddressPrefixExceptionMessages.AddressPrefixNameUniqueExceptionMessage, ServiceNameConstants.AddressPrefixService, ServiceActionNameConstants.AddressPrefixServiceUpdateAddressPrefixAsync);

                _mapper.Map<AddressPrefixUpdateDto, AddressPrefix>(addressPrefixUpdateDto, addressPrefix);

                EntityUtilities.SetLastUpdatedAndLastUpdatedByInEntity(ref addressPrefix, userFullName);

                await _context.SaveChanges();

                return _mapper.Map<AddressPrefix, AddressPrefixDto>(addressPrefix);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressPrefixDto> DeleteAddressPrefixAsync(int addressPrefixId)
        {
            try
            {
                AddressPrefix addressPrefix = await _context.AddressPrefixes.GetEntityAsync(addressPrefixId);

                if (addressPrefix == null)
                    throw new AddressPrefixNotFoundException(AddressPrefixExceptionMessages.AddressPrefixNotFoundByIdExceptionMessages, ServiceNameConstants.AddressPrefixService, ServiceActionNameConstants.AddressPrefixServiceDeleteAddressPrefixAsync);

                _context.AddressPrefixes.Remove(addressPrefix);
                await _context.SaveChanges();

                return _mapper.Map<AddressPrefix, AddressPrefixDto>(addressPrefix);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}