﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.InteractionType;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.InteractionType;
using ASP_NET_CRM.Exceptions.InteractionType;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class InteractionTypeService : IInteractionTypeService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public InteractionTypeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _context = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<InteractionTypeDto>> GetAllInteractionTypesAsync()
        {
            IEnumerable<InteractionType> interactionTypes = await _context.InteractionTypes.GetAllAsync();

            return _mapper.Map<IEnumerable<InteractionType>, IEnumerable<InteractionTypeDto>>(interactionTypes);
        }

        public async Task<IEnumerable<InteractionTypeDto>> GetInteractionTypesAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy)
        {
            try
            {
                IEnumerable<InteractionType> interactionTypes;

                if (!String.IsNullOrEmpty(searchString) && !String.IsNullOrEmpty(searchBy))
                {
                    switch (searchBy)
                    {
                        case InteractionTypeConstants.Id:
                            int searchInt = SearchUtilities.ValidateSearchInt(searchString);
                            interactionTypes = await _context.InteractionTypes.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.Id == searchInt, recordsToSkip, recordsToTake);
                            break;
                        case InteractionTypeConstants.Name:
                            interactionTypes = await _context.InteractionTypes.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.Name.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        default:
                            interactionTypes = Enumerable.Empty<InteractionType>();
                            break;
                    }
                }
                else
                    interactionTypes = await _context.InteractionTypes.GetRecordsWithPaginationAsync(i => i.Id, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<InteractionType>, IEnumerable<InteractionTypeDto>>(interactionTypes);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<InteractionTypeDto>> GetInteractionTypesForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetInteractionTypesAsync(currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords, searchString, searchBy);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionTypeDto> GetInteractionTypeAsync(int interactionTypeId)
        {
            try
            {
                InteractionType interactionType = await _context.InteractionTypes.GetEntityAsync(interactionTypeId);

                if (interactionType == null)
                    throw new InteractionTypeNotFoundException(InteractionTypeExceptionMessages.InteractionTypeNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionTypeService, ServiceActionNameConstants.InteractionTypeServiceGetInteractionTypeAsync);

                return _mapper.Map<InteractionType, InteractionTypeDto>(interactionType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionTypeDto> AddInteractionTypeAsync(InteractionTypeSaveDto interactionTypeSaveDto, string userFullName)
        {
            try
            {
                InteractionType isInteractionTypeExists = await _context.InteractionTypes.SingleOrDefaultAsync(c => c.Name.Equals(interactionTypeSaveDto.Name));

                if (isInteractionTypeExists != null)
                    throw new InteractionTypeExistsException(InteractionTypeExceptionMessages.InteractionTypeNameUniqueExceptionMessage, ServiceNameConstants.InteractionTypeService, ServiceActionNameConstants.InteractionTypeServiceAddInteractionTypeAsync);

                InteractionType interactionType = _mapper.Map<InteractionTypeSaveDto, InteractionType>(interactionTypeSaveDto);

                EntityUtilities.SetCreatedAndCreatedByInEntity(ref interactionType, userFullName);

                _context.InteractionTypes.Add(interactionType);
                await _context.SaveChanges();

                return _mapper.Map<InteractionType, InteractionTypeDto>(interactionType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionTypeDto> UpdateInteractionTypeAsync(InteractionTypeUpdateDto interactionTypeUpdateDto, string userFullName)
        {
            try
            {
                InteractionType interactionType = await _context.InteractionTypes.GetEntityAsync(interactionTypeUpdateDto.Id);

                if (interactionType == null)
                    throw new InteractionTypeNotFoundException(InteractionTypeExceptionMessages.InteractionTypeNotFoundByIdExceptionMessage, ServiceNameConstants.InteractionTypeService, ServiceActionNameConstants.InteractionTypeServiceUpdateInteractionTypeAsync);

                InteractionType isInteractionTypeExists = await _context.InteractionTypes.SingleOrDefaultAsync(c => c.Name.Equals(interactionTypeUpdateDto.Name) && c.Id != interactionTypeUpdateDto.Id);

                if (isInteractionTypeExists != null)
                    throw new InteractionTypeExistsException(InteractionTypeExceptionMessages.InteractionTypeNameUniqueExceptionMessage, ServiceNameConstants.InteractionTypeService, ServiceActionNameConstants.InteractionTypeServiceUpdateInteractionTypeAsync);

                _mapper.Map<InteractionTypeUpdateDto, InteractionType>(interactionTypeUpdateDto, interactionType);

                EntityUtilities.SetLastUpdatedAndLastUpdatedByInEntity(ref interactionType, userFullName);

                await _context.SaveChanges();

                return _mapper.Map<InteractionType, InteractionTypeDto>(interactionType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionTypeDto> DeleteInteractionTypeAsync(int interactionTypeId)
        {
            try
            {
                InteractionType interactionType = await _context.InteractionTypes.GetEntityAsync(interactionTypeId);

                if (interactionType == null)
                    throw new InteractionTypeNotFoundException(InteractionTypeExceptionMessages.InteractionTypeNotFoundByIdExceptionMessage, ServiceNameConstants.AddressPrefixService, ServiceActionNameConstants.InteractionTypeServiceDeleteInteractionTypeAsync);

                _context.InteractionTypes.Remove(interactionType);
                await _context.SaveChanges();

                return _mapper.Map<InteractionType, InteractionTypeDto>(interactionType);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}