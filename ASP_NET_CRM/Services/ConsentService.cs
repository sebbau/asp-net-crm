﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Exceptions.Models.Consent;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.Consent;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Exceptions.Consent;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class ConsentService : IConsentService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public ConsentService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _context = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ConsentDto>> GetAllConsents()
        {
            try
            {
                IEnumerable<Consent> consents = await _context.Consents.GetAllAsync();

                return _mapper.Map<IEnumerable<Consent>, IEnumerable<ConsentDto>>(consents);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ConsentDto>> GetConsentsAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy)
        {
            try
            {
                IEnumerable<Consent> consents;

                if (!String.IsNullOrEmpty(searchString) && !String.IsNullOrEmpty(searchBy))
                {
                    switch (searchBy)
                    {
                        case ConsentConstants.Id:
                            int searchInt = SearchUtilities.ValidateSearchInt(searchString);
                            consents = await _context.Consents.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.Id == searchInt, recordsToSkip, recordsToTake);
                            break;
                        case ConsentConstants.Name:
                            consents = await _context.Consents.GetRecordsWithPaginationAndSearchAsync(c => c.Id, c => c.Name.Contains(searchString), recordsToSkip, recordsToTake);
                            break;
                        default:
                            consents = Enumerable.Empty<Consent>();
                            break;
                    }
                }
                else
                    consents = await _context.Consents.GetRecordsWithPaginationAsync(i => i.Id, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<Consent>, IEnumerable<ConsentDto>>(consents);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ConsentDto>> GetConsentsForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetConsentsAsync(currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords, searchString, searchBy);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ConsentDto> GetConsentAsync(int consentId)
        {
            try
            {
                Consent consent = await _context.Consents.GetEntityAsync(consentId);

                if (consent == null)
                    throw new ConsentNotFoundException(ConsentExceptionMessages.ConsentNotFoundByIdExceptionMessage, ServiceNameConstants.ConsentService, ServiceActionNameConstants.ConsentServiceGetConsentAsync);

                return _mapper.Map<Consent, ConsentDto>(consent);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ConsentDto> AddConsentAsync(ConsentSaveDto consentSaveDto, string userFullName)
        {
            try
            {
                Consent isConsentExists = await _context.Consents.SingleOrDefaultAsync(c => c.Name.Equals(consentSaveDto.Name));

                if (isConsentExists != null)
                    throw new ConsentExistsException(ConsentExceptionMessages.ConsentNameUniqueExceptionMessage, ServiceNameConstants.ConsentService, ServiceActionNameConstants.ConsentServiceAddConsentAsync);

                Consent consent = _mapper.Map<ConsentSaveDto, Consent>(consentSaveDto);

                EntityUtilities.SetCreatedAndCreatedByInEntity(ref consent, userFullName);

                _context.Consents.Add(consent);
                await _context.SaveChanges();

                return _mapper.Map<Consent, ConsentDto>(consent);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ConsentDto> UpdateConsentAsync(ConsentUpdateDto consentUpdateDto, string userFullName)
        {
            try
            {
                Consent isExistsConsent = await _context.Consents.SingleOrDefaultAsync(c => c.Name.Equals(consentUpdateDto.Name) && c.Id != consentUpdateDto.Id);

                if (isExistsConsent != null)
                    throw new ConsentExistsException(ConsentExceptionMessages.ConsentNameUniqueExceptionMessage, ServiceNameConstants.ConsentService, ServiceActionNameConstants.ConsentServiceUpdateConsentAsync);

                Consent consent = await _context.Consents.GetEntityAsync(consentUpdateDto.Id);

                if (consent == null)
                    throw new ConsentNotFoundException(ConsentExceptionMessages.ConsentNotFoundByIdExceptionMessage, ServiceNameConstants.ConsentService, ServiceActionNameConstants.ConsentServiceUpdateConsentAsync);               

                _mapper.Map<ConsentUpdateDto, Consent>(consentUpdateDto, consent);

                EntityUtilities.SetLastUpdatedAndLastUpdatedByInEntity(ref consent, userFullName);

                await _context.SaveChanges();

                return _mapper.Map<Consent, ConsentDto>(consent);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ConsentDto> DeleteConsentAsync(int consentId)
        {
            try
            {
                Consent consent = await _context.Consents.GetEntityAsync(consentId);

                if (consent == null)
                    throw new ConsentNotFoundException(ConsentExceptionMessages.ConsentNotFoundByIdExceptionMessage, ServiceNameConstants.ConsentService, ServiceActionNameConstants.ConsentServiceDeleteConsentAsync);

                _context.Consents.Remove(consent);
                await _context.SaveChanges();

                return _mapper.Map<Consent, ConsentDto>(consent);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<ConsentDto>> UpdateConsentsForClient(int clientId, IEnumerable<ConsentClientUpdateDto> clientSelectedConsents)
        {
            try
            {
                IEnumerable<Consent> consents = await _context.Consents.GetAllAsync();

                Client client = await _context.Clients.GetEntityWithIncludesAsync(c => c.Id == clientId, c => c.Consents);

                if (client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.ConsentService, ServiceActionNameConstants.ConsentServiceUpdateConsentsForClient);

                List<Consent> consentsToRemove = new List<Consent>();

                foreach (ConsentClientUpdateDto selectedConsent in clientSelectedConsents)
                {
                    if (client.Consents.Any(con => con.Id == selectedConsent.Id))
                        continue;

                    Consent consentToAdd = consents.FirstOrDefault(con => con.Id == selectedConsent.Id);

                    client.Consents.Add(consentToAdd);
                }

                foreach (Consent clientConsent in client.Consents)
                {
                    if (clientSelectedConsents.Any(con => con.Id == clientConsent.Id))
                        continue;

                    Consent consentToRemove = consents.FirstOrDefault(con => con.Id == clientConsent.Id);

                    consentsToRemove.Add(consentToRemove);
                }

                foreach (Consent consentToRemove in consentsToRemove)
                    client.Consents.Remove(consentToRemove);

                await _context.SaveChanges();

                return _mapper.Map<IEnumerable<ConsentClientUpdateDto>, IEnumerable<ConsentDto>>(clientSelectedConsents);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}