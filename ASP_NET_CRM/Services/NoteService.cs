﻿using ASP_NET_CRM.Constants.Exceptions;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Exceptions.Models.Note;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Exceptions.Note;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Utilities;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Services
{
    public class NoteService : INoteService
    {
        private readonly IUnitOfWork _context;
        private readonly IMapper _mapper;

        public NoteService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _context = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<NoteDto>> GetNotesAsync(int recordsToSkip, int recordsToTake, string searchString, string searchBy, params Expression<Func<Note, object>>[] includes)
        {
            try
            {
                IEnumerable<Note> notes;

                if (!String.IsNullOrEmpty(searchString) && !String.IsNullOrEmpty(searchBy))
                {
                    switch (searchBy)
                    {
                        case NoteConstants.Id:
                            int searchInt = SearchUtilities.ValidateSearchInt(searchString);
                            notes = await _context.Notes.GetRecordsWithPaginationAndSearchAsync(n => n.Id, n => n.Id == searchInt, recordsToSkip, recordsToTake, includes);
                            break;
                        case NoteConstants.ClientName:
                            notes = await _context.Notes.GetRecordsWithPaginationAndSearchAsync(n => n.Id, n => n.Client.Name.Contains(searchString), recordsToSkip, recordsToTake, includes);
                            break;
                        case NoteConstants.Title:
                            notes = await _context.Notes.GetRecordsWithPaginationAndSearchAsync(n => n.Id, n => n.Title.Contains(searchString), recordsToSkip, recordsToTake, includes);
                            break;
                        default:
                            notes = Enumerable.Empty<Note>();
                            break;
                    }
                }
                else
                    notes = await _context.Notes.GetRecordsWithPaginationAsync(i => i.Id, recordsToSkip, recordsToTake, includes);

                return _mapper.Map<IEnumerable<Note>, IEnumerable<NoteDto>>(notes);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<NoteDto>> GetNotesForPaginationAsync(int currentPage, int recordsPerPage, string searchString, string searchBy, params Expression<Func<Note, object>>[] includes)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetNotesAsync(currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords, searchString, searchBy, includes);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<NoteDto>> GetNotesByClientIdOrderByDescendingAsync(int clientId, int recordsToSkip, int recordsToTake)
        {
            try
            {
                IEnumerable<Note> notes = await _context.Notes.GetRecordsWithWhereAndPaginationAndOrderByDescendingAsync(n => n.ClientId == clientId, n => n.Id, recordsToSkip, recordsToTake);

                return _mapper.Map<IEnumerable<Note>, IEnumerable<NoteDto>>(notes);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<NoteDto>> GetNotesByClientIdOrderByDescendingForPaginationAsync(int clientId, int currentPage, int recordsPerPage)
        {
            try
            {
                int constIntToSkipRecords = PagingUtilities.SetIntToSkipRecords(currentPage);

                return await GetNotesByClientIdOrderByDescendingAsync(clientId, currentPage * recordsPerPage, recordsPerPage * constIntToSkipRecords);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<NoteDto> GetNoteWithIncludesAsync(Expression<Func<Note, bool>> predicate, params Expression<Func<Note, object>>[] includes)
        {
            try
            {
                Note note = await _context.Notes.GetEntityWithIncludesAsync(predicate, includes);

                if (note == null)
                    throw new NoteNotFoundException(NoteExceptionMessages.NoteNotFoundByIdExceptionMessage, ServiceNameConstants.NoteService, ServiceActionNameConstants.NoteServiceGetNoteWithIncludesAsync);

                return _mapper.Map<Note, NoteDto>(note);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<NoteDto> AddNoteAsync(NoteSaveDto noteSaveDto, string userFullName)
        {
            try
            {
                Client client = await _context.Clients.GetEntityAsync(noteSaveDto.ClientId);

                if (client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.NoteService, ServiceActionNameConstants.NoteServiceAddNoteAsync);

                Note note = _mapper.Map<NoteSaveDto, Note>(noteSaveDto);

                EntityUtilities.SetCreatedAndCreatedByInEntity(ref note, userFullName);

                _context.Notes.Add(note);
                await _context.SaveChanges();

                return _mapper.Map<Note, NoteDto>(note);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<NoteDto> UpdateNoteAsync(NoteUpdateDto noteUpdateDto, string userFullName)
        {
            try
            {
                Note note = await _context.Notes.GetEntityWithIncludesAsync(n => n.Id == noteUpdateDto.Id, n => n.Client);

                if (note == null)
                    throw new NoteNotFoundException(NoteExceptionMessages.NoteNotFoundByIdExceptionMessage, ServiceNameConstants.NoteService, ServiceActionNameConstants.NoteServiceUpdateNoteAsync);

                if (note.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, ServiceNameConstants.NoteService, ServiceActionNameConstants.NoteServiceUpdateNoteAsync);

                _mapper.Map<NoteUpdateDto, Note>(noteUpdateDto, note);

                EntityUtilities.SetLastUpdatedAndLastUpdatedByInEntity(ref note, userFullName);

                await _context.SaveChanges();

                return _mapper.Map<Note, NoteDto>(note);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<NoteDto> DeleteNoteAsync(int noteId)
        {
            try
            {
                Note note = await _context.Notes.GetEntityAsync(noteId);

                if (note == null)
                    throw new NoteNotFoundException(NoteExceptionMessages.NoteNotFoundByIdExceptionMessage, ServiceNameConstants.NoteService, ServiceActionNameConstants.NoteServiceDeleteNoteAsync);

                _context.Notes.Remove(note);
                await _context.SaveChanges();

                return _mapper.Map<Note, NoteDto>(note);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}