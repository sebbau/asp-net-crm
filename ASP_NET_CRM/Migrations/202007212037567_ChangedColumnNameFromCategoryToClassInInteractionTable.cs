﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedColumnNameFromCategoryToClassInInteractionTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Interactions", "Category", "Class");
        }
        
        public override void Down()
        {
            RenameColumn("dbo.Interactions", "Class", "Category");
        }
    }
}
