﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRegonKRSWebSitePhoneNumberEmailDocumentTypeDocumentNumberDocumentIssueDateAndDocumentIssuedByInClientTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "Regon", c => c.String(maxLength: 14));
            AddColumn("dbo.Clients", "KRS", c => c.String(maxLength: 10));
            AddColumn("dbo.Clients", "WebSite", c => c.String(maxLength: 250));
            AddColumn("dbo.Clients", "PhoneNumber", c => c.String(nullable: false, maxLength: 11));
            AddColumn("dbo.Clients", "Email", c => c.String(maxLength: 200));
            AddColumn("dbo.Clients", "DocumentType", c => c.Int());
            AddColumn("dbo.Clients", "DocumentNumber", c => c.String(maxLength: 15));
            AddColumn("dbo.Clients", "DocumentIssueDate", c => c.DateTime());
            AddColumn("dbo.Clients", "DocumentIssuedBy", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "DocumentIssuedBy");
            DropColumn("dbo.Clients", "DocumentIssueDate");
            DropColumn("dbo.Clients", "DocumentNumber");
            DropColumn("dbo.Clients", "DocumentType");
            DropColumn("dbo.Clients", "Email");
            DropColumn("dbo.Clients", "PhoneNumber");
            DropColumn("dbo.Clients", "WebSite");
            DropColumn("dbo.Clients", "KRS");
            DropColumn("dbo.Clients", "Regon");
        }
    }
}
