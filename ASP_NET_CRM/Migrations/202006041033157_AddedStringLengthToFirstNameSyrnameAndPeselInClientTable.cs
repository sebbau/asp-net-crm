﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStringLengthToFirstNameSyrnameAndPeselInClientTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clients", "FirstName", c => c.String(maxLength: 50));
            AlterColumn("dbo.Clients", "Surname", c => c.String(maxLength: 150));
            AlterColumn("dbo.Clients", "Pesel", c => c.String(maxLength: 11));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clients", "Pesel", c => c.String());
            AlterColumn("dbo.Clients", "Surname", c => c.String());
            AlterColumn("dbo.Clients", "FirstName", c => c.String());
        }
    }
}
