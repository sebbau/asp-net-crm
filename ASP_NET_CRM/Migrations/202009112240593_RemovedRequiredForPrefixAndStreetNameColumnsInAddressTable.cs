﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedRequiredForPrefixAndStreetNameColumnsInAddressTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Addresses", "Prefix", c => c.String(maxLength: 10));
            AlterColumn("dbo.Addresses", "StreetName", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Addresses", "StreetName", c => c.String(nullable: false, maxLength: 200));
            AlterColumn("dbo.Addresses", "Prefix", c => c.String(nullable: false, maxLength: 10));
        }
    }
}
