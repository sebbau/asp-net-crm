﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMaxLengthAndRequiredForColumnsNameAndFullNameInConsentTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Consents", "Name", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Consents", "FullName", c => c.String(nullable: false, maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Consents", "FullName", c => c.String());
            AlterColumn("dbo.Consents", "Name", c => c.String());
        }
    }
}
