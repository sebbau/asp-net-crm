﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedConsentAndClient_ConsentTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Consents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        FullName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Client_Consent",
                c => new
                    {
                        ClientId = c.Int(nullable: false),
                        ConsentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ClientId, t.ConsentId })
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .ForeignKey("dbo.Consents", t => t.ConsentId, cascadeDelete: true)
                .Index(t => t.ClientId)
                .Index(t => t.ConsentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Client_Consent", "ConsentId", "dbo.Consents");
            DropForeignKey("dbo.Client_Consent", "ClientId", "dbo.Clients");
            DropIndex("dbo.Client_Consent", new[] { "ConsentId" });
            DropIndex("dbo.Client_Consent", new[] { "ClientId" });
            DropTable("dbo.Client_Consent");
            DropTable("dbo.Consents");
        }
    }
}
