﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRelationBetweenClientAndInteractionTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Interactions", "ClientId", c => c.Int(nullable: false));
            CreateIndex("dbo.Interactions", "ClientId");
            AddForeignKey("dbo.Interactions", "ClientId", "dbo.Clients", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Interactions", "ClientId", "dbo.Clients");
            DropIndex("dbo.Interactions", new[] { "ClientId" });
            DropColumn("dbo.Interactions", "ClientId");
        }
    }
}
