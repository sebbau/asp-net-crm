﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedControllerTypeColumnInApplicationErrorsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ApplicationErrors", "ControllerType", c => c.String(maxLength: 3));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ApplicationErrors", "ControllerType");
        }
    }
}
