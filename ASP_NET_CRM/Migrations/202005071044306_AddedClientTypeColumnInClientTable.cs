﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedClientTypeColumnInClientTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "ClientType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "ClientType");
        }
    }
}
