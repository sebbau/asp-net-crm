﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRelationToClientInAddressTableWithClientIdColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "ClientId", c => c.Int(nullable: false));
            CreateIndex("dbo.Addresses", "ClientId");
            AddForeignKey("dbo.Addresses", "ClientId", "dbo.Clients", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Addresses", "ClientId", "dbo.Clients");
            DropIndex("dbo.Addresses", new[] { "ClientId" });
            DropColumn("dbo.Addresses", "ClientId");
        }
    }
}
