﻿namespace ASP_NET_CRM.Migrations
{
    using ASP_NET_CRM.Constants.Roles;
    using ASP_NET_CRM.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ASP_NET_CRM.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ASP_NET_CRM.Models.ApplicationDbContext context)
        {
            try
            {
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                string email = "aspnetcrm@gmail.com";
                string firstName = "ASP NET CRM";
                string surname = "Administrator";
                string fullName = "Seed Data";
                string password = "SeedData1!";

                if (userManager.FindByEmail(email) == null)
                {
                    ApplicationUser user = new ApplicationUser
                    {
                        Email = email,
                        EmailConfirmed = true,
                        Created = DateTime.Now,
                        CreatedBy = "Initial Data",
                        FirstName = firstName,
                        Surname = surname,
                        FullName = fullName,
                        UserName = email
                    };

                    var addUser = userManager.Create(user, password);

                    if (addUser.Succeeded)
                    {
                        userManager.AddToRole(user.Id, RoleConstants.ReadonlyRole);
                        userManager.AddToRole(user.Id, RoleConstants.EmployeeRole);
                        userManager.AddToRole(user.Id, RoleConstants.AdministratorRole);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
