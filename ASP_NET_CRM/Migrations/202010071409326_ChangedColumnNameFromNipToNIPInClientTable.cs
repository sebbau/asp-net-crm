﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedColumnNameFromNipToNIPInClientTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Clients", "Nip", "NIP");
        }
        
        public override void Down()
        {
            RenameColumn("dbo.Clients", "NIP", "Nip");
        }
    }
}
