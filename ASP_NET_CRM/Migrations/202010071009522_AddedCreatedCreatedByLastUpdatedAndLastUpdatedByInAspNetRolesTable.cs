﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCreatedCreatedByLastUpdatedAndLastUpdatedByInAspNetRolesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetRoles", "Created", c => c.DateTime());
            AddColumn("dbo.AspNetRoles", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.AspNetRoles", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.AspNetRoles", "LastUpdatedBy", c => c.String(maxLength: 201));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetRoles", "LastUpdatedBy");
            DropColumn("dbo.AspNetRoles", "LastUpdated");
            DropColumn("dbo.AspNetRoles", "CreatedBy");
            DropColumn("dbo.AspNetRoles", "Created");
        }
    }
}
