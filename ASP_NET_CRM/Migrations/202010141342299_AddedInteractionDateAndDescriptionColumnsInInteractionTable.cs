﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedInteractionDateAndDescriptionColumnsInInteractionTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Interactions", "InteractionDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Interactions", "Description", c => c.String(maxLength: 2000));
            AlterColumn("dbo.Interactions", "Type", c => c.String(nullable: false));
            AlterColumn("dbo.Interactions", "Class", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Interactions", "Class", c => c.String());
            AlterColumn("dbo.Interactions", "Type", c => c.String());
            DropColumn("dbo.Interactions", "Description");
            DropColumn("dbo.Interactions", "InteractionDate");
        }
    }
}
