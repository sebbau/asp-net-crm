﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedStatusColumnInInteractionTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Interactions", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Interactions", "Status");
        }
    }
}
