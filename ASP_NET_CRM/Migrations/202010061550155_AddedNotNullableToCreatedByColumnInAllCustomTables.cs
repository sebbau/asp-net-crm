﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedNotNullableToCreatedByColumnInAllCustomTables : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Addresses", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
            AlterColumn("dbo.Clients", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
            AlterColumn("dbo.Consents", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
            AlterColumn("dbo.Interactions", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
            AlterColumn("dbo.Notes", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
            AlterColumn("dbo.AddressPrefixes", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
            AlterColumn("dbo.ApplicationErrors", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
            AlterColumn("dbo.InteractionClasses", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
            AlterColumn("dbo.InteractionTypes", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InteractionTypes", "CreatedBy", c => c.String(maxLength: 201));
            AlterColumn("dbo.InteractionClasses", "CreatedBy", c => c.String(maxLength: 201));
            AlterColumn("dbo.ApplicationErrors", "CreatedBy", c => c.String(maxLength: 201));
            AlterColumn("dbo.AddressPrefixes", "CreatedBy", c => c.String(maxLength: 201));
            AlterColumn("dbo.Notes", "CreatedBy", c => c.String(maxLength: 201));
            AlterColumn("dbo.Interactions", "CreatedBy", c => c.String(maxLength: 201));
            AlterColumn("dbo.Consents", "CreatedBy", c => c.String(maxLength: 201));
            AlterColumn("dbo.Clients", "CreatedBy", c => c.String(maxLength: 201));
            AlterColumn("dbo.Addresses", "CreatedBy", c => c.String(maxLength: 201));
        }
    }
}
