﻿// <auto-generated />
namespace ASP_NET_CRM.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class CreateInteractionTypesTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateInteractionTypesTable));
        
        string IMigrationMetadata.Id
        {
            get { return "202007112224152_CreateInteractionTypesTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
