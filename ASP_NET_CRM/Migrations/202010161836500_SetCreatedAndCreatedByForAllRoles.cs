﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetCreatedAndCreatedByForAllRoles : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.AspNetRoles SET Created = CURRENT_TIMESTAMP, CreatedBy = 'Seed Data'");
        }
        
        public override void Down()
        {
            Sql("UPDATE dbo.AspNetRoles SET Created = NULL, CreatedBy = NULL");
        }
    }
}
