﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RolesDiscriminatorSeedData : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE dbo.AspNetRoles SET Discriminator = 'Role';");
        }
        
        public override void Down()
        {
            Sql("UPDATE dbo.AspNetRoles SET Discriminator = '';");
        }
    }
}
