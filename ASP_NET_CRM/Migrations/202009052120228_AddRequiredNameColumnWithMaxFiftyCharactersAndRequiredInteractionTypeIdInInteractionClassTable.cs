﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRequiredNameColumnWithMaxFiftyCharactersAndRequiredInteractionTypeIdInInteractionClassTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InteractionClasses", "Name", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InteractionClasses", "Name", c => c.String());
        }
    }
}
