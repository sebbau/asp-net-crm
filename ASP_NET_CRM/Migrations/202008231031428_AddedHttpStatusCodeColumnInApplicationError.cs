﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedHttpStatusCodeColumnInApplicationError : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ApplicationErrors", "HttpStatusCode", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ApplicationErrors", "HttpStatusCode");
        }
    }
}
