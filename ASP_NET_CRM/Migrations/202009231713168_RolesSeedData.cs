﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RolesSeedData : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO dbo.AspNetRoles VALUES (1, 'Administrator'), (2, 'Employee'), (3, 'Readonly');");
        }
        
        public override void Down()
        {
            Sql("DELETE dbo.AspNetRoles WHERE Name IN ('Administrator', 'Employee', 'Readonly');");
        }
    }
}
