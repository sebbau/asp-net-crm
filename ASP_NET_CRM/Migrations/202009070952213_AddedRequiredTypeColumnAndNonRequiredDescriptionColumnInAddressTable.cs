﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRequiredTypeColumnAndNonRequiredDescriptionColumnInAddressTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Addresses", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Addresses", "Description");
            DropColumn("dbo.Addresses", "Type");
        }
    }
}
