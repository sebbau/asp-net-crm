﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedApplicationErrorTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApplicationErrors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ControllerName = c.String(maxLength: 200),
                        ControllerActionName = c.String(maxLength: 200),
                        ExceptionType = c.String(maxLength: 200),
                        ClassName = c.String(maxLength: 200),
                        ActionName = c.String(maxLength: 200),
                        Message = c.String(maxLength: 2500),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ApplicationErrors");
        }
    }
}
