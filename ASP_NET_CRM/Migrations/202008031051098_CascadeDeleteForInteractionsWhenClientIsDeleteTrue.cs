﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CascadeDeleteForInteractionsWhenClientIsDeleteTrue : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Interactions", "ClientId", "dbo.Clients");
            AddForeignKey("dbo.Interactions", "ClientId", "dbo.Clients", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Interactions", "ClientId", "dbo.Clients");
            AddForeignKey("dbo.Interactions", "ClientId", "dbo.Clients", "Id");
        }
    }
}
