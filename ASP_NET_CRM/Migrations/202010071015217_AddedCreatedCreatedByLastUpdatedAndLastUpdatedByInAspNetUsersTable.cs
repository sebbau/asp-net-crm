﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCreatedCreatedByLastUpdatedAndLastUpdatedByInAspNetUsersTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "CreatedBy", c => c.String(nullable: false, maxLength: 201));
            AddColumn("dbo.AspNetUsers", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.AspNetUsers", "LastUpdatedBy", c => c.String(maxLength: 201));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "LastUpdatedBy");
            DropColumn("dbo.AspNetUsers", "LastUpdated");
            DropColumn("dbo.AspNetUsers", "CreatedBy");
            DropColumn("dbo.AspNetUsers", "Created");
        }
    }
}
