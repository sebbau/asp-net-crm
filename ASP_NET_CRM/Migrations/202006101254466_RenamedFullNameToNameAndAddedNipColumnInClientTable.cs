﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamedFullNameToNameAndAddedNipColumnInClientTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "Nip", c => c.String(maxLength: 10));
            RenameColumn("dbo.Clients", "FullName", "Name");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "Nip");
            RenameColumn("dbo.Clients", "Name", "FullName");
        }
    }
}
