﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamedColumnFromStreetPrefixToPrefixInAddressTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Addresses", "StreetPrefix", "Prefix");
        }
        
        public override void Down()
        {
            RenameColumn("dbo.Addresses", "Prefix", "StreetPrefix");
        }
    }
}
