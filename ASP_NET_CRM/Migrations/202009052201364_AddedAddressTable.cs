﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAddressTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StreetPrefix = c.String(nullable: false, maxLength: 10),
                        StreetName = c.String(nullable: false, maxLength: 200),
                        HouseNumber = c.String(nullable: false, maxLength: 6),
                        ApartmentNumber = c.String(maxLength: 10),
                        FullAddressName = c.String(nullable: false, maxLength: 230),
                        PostalCode = c.String(nullable: false, maxLength: 6),
                        City = c.String(nullable: false, maxLength: 50),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Addresses");
        }
    }
}
