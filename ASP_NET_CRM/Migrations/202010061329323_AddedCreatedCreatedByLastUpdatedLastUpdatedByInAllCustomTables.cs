﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCreatedCreatedByLastUpdatedLastUpdatedByInAllCustomTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Addresses", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.Addresses", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.Addresses", "LastUpdatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.Clients", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Clients", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.Clients", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.Clients", "LastUpdatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.Consents", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Consents", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.Consents", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.Consents", "LastUpdatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.Interactions", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Interactions", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.Interactions", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.Interactions", "LastUpdatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.Notes", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Notes", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.Notes", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.Notes", "LastUpdatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.AddressPrefixes", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.AddressPrefixes", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.AddressPrefixes", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.AddressPrefixes", "LastUpdatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.ApplicationErrors", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.ApplicationErrors", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.ApplicationErrors", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.ApplicationErrors", "LastUpdatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.InteractionClasses", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.InteractionClasses", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.InteractionClasses", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.InteractionClasses", "LastUpdatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.InteractionTypes", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.InteractionTypes", "CreatedBy", c => c.String(maxLength: 201));
            AddColumn("dbo.InteractionTypes", "LastUpdated", c => c.DateTime());
            AddColumn("dbo.InteractionTypes", "LastUpdatedBy", c => c.String(maxLength: 201));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InteractionTypes", "LastUpdatedBy");
            DropColumn("dbo.InteractionTypes", "LastUpdated");
            DropColumn("dbo.InteractionTypes", "CreatedBy");
            DropColumn("dbo.InteractionTypes", "Created");
            DropColumn("dbo.InteractionClasses", "LastUpdatedBy");
            DropColumn("dbo.InteractionClasses", "LastUpdated");
            DropColumn("dbo.InteractionClasses", "CreatedBy");
            DropColumn("dbo.InteractionClasses", "Created");
            DropColumn("dbo.ApplicationErrors", "LastUpdatedBy");
            DropColumn("dbo.ApplicationErrors", "LastUpdated");
            DropColumn("dbo.ApplicationErrors", "CreatedBy");
            DropColumn("dbo.ApplicationErrors", "Created");
            DropColumn("dbo.AddressPrefixes", "LastUpdatedBy");
            DropColumn("dbo.AddressPrefixes", "LastUpdated");
            DropColumn("dbo.AddressPrefixes", "CreatedBy");
            DropColumn("dbo.AddressPrefixes", "Created");
            DropColumn("dbo.Notes", "LastUpdatedBy");
            DropColumn("dbo.Notes", "LastUpdated");
            DropColumn("dbo.Notes", "CreatedBy");
            DropColumn("dbo.Notes", "Created");
            DropColumn("dbo.Interactions", "LastUpdatedBy");
            DropColumn("dbo.Interactions", "LastUpdated");
            DropColumn("dbo.Interactions", "CreatedBy");
            DropColumn("dbo.Interactions", "Created");
            DropColumn("dbo.Consents", "LastUpdatedBy");
            DropColumn("dbo.Consents", "LastUpdated");
            DropColumn("dbo.Consents", "CreatedBy");
            DropColumn("dbo.Consents", "Created");
            DropColumn("dbo.Clients", "LastUpdatedBy");
            DropColumn("dbo.Clients", "LastUpdated");
            DropColumn("dbo.Clients", "CreatedBy");
            DropColumn("dbo.Clients", "Created");
            DropColumn("dbo.Addresses", "LastUpdatedBy");
            DropColumn("dbo.Addresses", "LastUpdated");
            DropColumn("dbo.Addresses", "CreatedBy");
            DropColumn("dbo.Addresses", "Created");
        }
    }
}
