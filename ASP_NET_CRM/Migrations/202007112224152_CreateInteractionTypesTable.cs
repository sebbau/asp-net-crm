﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateInteractionTypesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InteractionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.InteractionTypes");
        }
    }
}
