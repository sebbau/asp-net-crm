﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRequiredNameColumnWithMaxFiftyCharactersInInteractionTypeTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InteractionTypes", "Name", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InteractionTypes", "Name", c => c.String());
        }
    }
}
