﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateInteractionClassesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InteractionClasses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        InteractionTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InteractionTypes", t => t.InteractionTypeId, cascadeDelete: true)
                .Index(t => t.InteractionTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InteractionClasses", "InteractionTypeId", "dbo.InteractionTypes");
            DropIndex("dbo.InteractionClasses", new[] { "InteractionTypeId" });
            DropTable("dbo.InteractionClasses");
        }
    }
}
