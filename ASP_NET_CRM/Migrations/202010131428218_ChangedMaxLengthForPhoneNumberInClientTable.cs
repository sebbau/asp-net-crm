﻿namespace ASP_NET_CRM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedMaxLengthForPhoneNumberInClientTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clients", "PhoneNumber", c => c.String(nullable: false, maxLength: 12));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clients", "PhoneNumber", c => c.String(nullable: false, maxLength: 11));
        }
    }
}
