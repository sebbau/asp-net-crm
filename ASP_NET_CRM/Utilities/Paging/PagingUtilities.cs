﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.DTO;
using ASP_NET_CRM.Models.Utilities.Paging;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ASP_NET_CRM.Utilities.Paging
{
    public static class PagingUtilities
    {
        private static readonly int pagesToDisplay = GlobalVariables.pagesRangeToDisplay;

        public static Pagination Pagination(int currentPage, int recordsPerPage, IEnumerable<Dto> recordsForCurrentPage, IEnumerable<Dto> recordsForNextPages)
        {
            try
            {
                int constIntToSkipRecords = pagesToDisplay - currentPage >= 2 ? pagesToDisplay - currentPage : 2;

                Pagination pagination = new Pagination()
                {
                    CurrentPage = currentPage
                };

                if (currentPage > 0 && currentPage <= 2)
                {
                    if (recordsForCurrentPage.Count() == recordsPerPage)
                    {
                        if (recordsForNextPages.Count() == recordsPerPage * constIntToSkipRecords)
                        {
                            pagination.StartPage = 1;
                            pagination.EndPage = 5;
                            return pagination;
                        }
                        else
                        {
                            pagination.StartPage = 1;
                            pagination.EndPage = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(recordsForNextPages.Count()) / Convert.ToDecimal(recordsPerPage))) + currentPage;
                            return pagination;
                        }
                    }
                    else
                    {
                        pagination.StartPage = 1;
                        pagination.EndPage = currentPage;
                        return pagination;
                    }
                }

                else if (currentPage > 2)
                {
                    if (recordsForCurrentPage.Count() == recordsPerPage && recordsForNextPages.Count() != 0)
                    {
                        if (recordsForNextPages.Count() > recordsPerPage)
                        {
                            pagination.StartPage = currentPage - 2;
                            pagination.EndPage = currentPage + 2;
                            return pagination;
                        }
                        else
                        {
                            if (currentPage - 3 <= 0)
                                pagination.StartPage = 1;
                            else
                                pagination.StartPage = currentPage - 3;

                            pagination.EndPage = currentPage + 1;
                            return pagination;
                        }
                    }
                    else
                    {
                        if (currentPage - 4 <= 0)
                            pagination.StartPage = 1;
                        else
                            pagination.StartPage = currentPage - 4;

                        pagination.EndPage = currentPage;
                        return pagination;
                    }
                }
                else
                {
                    throw new Exception("PagingUtilities exception");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int ValidateMaxRecordsPerPage(int currentRecordsPerPage)
        {
            if (currentRecordsPerPage <= GlobalVariables.maxRecordsPerPage)
                return currentRecordsPerPage;

            else
                return GlobalVariables.defaultRecordsPerPage;
        }

        public static int SetIntToSkipRecords(int currentPage)
        {
            return pagesToDisplay - currentPage >= 2 ? pagesToDisplay - currentPage : 2;
        }
    }
}