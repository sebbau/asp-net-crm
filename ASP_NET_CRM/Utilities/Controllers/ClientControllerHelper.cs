﻿using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Services;
using ASP_NET_CRM.ViewModels.Clients;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Utilities.Controllers
{
    public class ClientControllerHelper : IClientControllerHelper
    {
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public ClientControllerHelper
            (
                IApplicationErrorService applicationErrorService,
                IMapper mapper
            )
        {
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        public NewClientViewModel GetNewClientViewModelAndLogExceptionOnAddMethod(ClientExistsException clientExistsException, ControllerContext controllerContext, ClientSaveDto clientSaveDto)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)clientExistsException;

                exception.SetControllerAndExceptionProperties
                (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    clientExistsException.GetType().Name
                );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                NewClientViewModel viewModel = new NewClientViewModel { ClientTypeChoice = clientSaveDto.ClientType };

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IndividualClientEditViewModel GetIndividualClientEditViewModelAndLogExceptionOnUpdateMethod(ClientExistsException clientExistsException, ControllerContext controllerContext, ClientUpdateDto clientUpdateDto)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)clientExistsException;

                exception.SetControllerAndExceptionProperties
                (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    clientExistsException.GetType().Name
                );
                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                IndividualClientEditViewModel viewModel = new IndividualClientEditViewModel { Id = clientUpdateDto.Id, Name = clientUpdateDto.Name, ClientType = clientUpdateDto.ClientType };

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public BusinessClientEditViewModel GetBusinessClientEditViewModelAndLogExceptionOnUpdateMethod(ClientExistsException clientExistsException, ControllerContext controllerContext, ClientUpdateDto clientUpdateDto)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)clientExistsException;

                exception.SetControllerAndExceptionProperties
                (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    clientExistsException.GetType().Name
                );
                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                BusinessClientEditViewModel viewModel = new BusinessClientEditViewModel { Id = clientUpdateDto.Id, Name = clientUpdateDto.Name, ClientType = clientUpdateDto.ClientType };

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}