﻿using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Exceptions.Models.Interaction;
using ASP_NET_CRM.Constants.Exceptions.Utilities;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.DTO.Interaction;
using ASP_NET_CRM.DTO.InteractionClass;
using ASP_NET_CRM.DTO.InteractionType;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Exceptions.Interaction;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.Controllers;
using ASP_NET_CRM.ViewModels.Interactions;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Utilities.MVC.Controllers
{
    public class InteractionControllerHelper : IInteractionControllerHelper
    {
        private readonly IInteractionService _interactionService;
        private readonly IClientService _clientService;
        private readonly IInteractionTypeService _interactionTypeService;
        private readonly IInteractionClassService _interactionClassService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public InteractionControllerHelper(
            IInteractionService interactionService,
            IClientService clientService,
            IInteractionTypeService interactionTypeService,
            IInteractionClassService interactionClassService,
            IApplicationErrorService applicationErrorService,
            IMapper mapper
        )
        {
            _interactionService = interactionService;
            _clientService = clientService;
            _interactionTypeService = interactionTypeService;
            _interactionClassService = interactionClassService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        public async Task<NewInteractionViewModel> GetNewInteractionViewModelWhenModelStateIsInvalidForAddMethod(InteractionSaveDto interactionSaveDto)
        {
            try
            {
                NewInteractionViewModel viewModel = new NewInteractionViewModel();

                if (interactionSaveDto.ClientId != 0)
                {
                    ClientDto clientDto = await _clientService.GetClientAsync(interactionSaveDto.ClientId);

                    viewModel = _mapper.Map<ClientDto, NewInteractionViewModel>(clientDto);
                }
                else
                    viewModel.SetSearchClientValues();

                IEnumerable<InteractionTypeDto> interactionTypes = await _interactionTypeService.GetAllInteractionTypesAsync();               
                viewModel.AvailableTypes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(interactionTypes);

                if (interactionSaveDto.TypeId > 0)
                {
                    IEnumerable<InteractionClassDto> interactionClasses = await _interactionClassService.GetInteractionClassesForInteractionTypeAsync(interactionSaveDto.TypeId);
                    viewModel.AvailableClasses = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(interactionClasses);
                }

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<NewInteractionViewModel> GetNewInteractionViewModelAndLogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext, InteractionSaveDto interactionSaveDto)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                applicationCustomException.SetControllerAndExceptionProperties
                    (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    applicationCustomException.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(applicationCustomException);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                NewInteractionViewModel viewModel = new NewInteractionViewModel();

                if (applicationCustomException.GetType() != typeof(ClientNotFoundException))
                {
                    ClientDto clientDto = await _clientService.GetClientAsync(interactionSaveDto.ClientId);

                    viewModel = _mapper.Map<ClientDto, NewInteractionViewModel>(clientDto);
                }
                else
                    viewModel.SetSearchClientValues();

                IEnumerable<InteractionTypeDto> interactionTypes = await _interactionTypeService.GetAllInteractionTypesAsync();
                viewModel.AvailableTypes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(interactionTypes);

                if (interactionSaveDto.TypeId > 0)
                {
                    IEnumerable<InteractionClassDto> interactionClasses = await _interactionClassService.GetInteractionClassesForInteractionTypeAsync(interactionSaveDto.TypeId);
                    viewModel.AvailableClasses = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(interactionClasses);
                }

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(InteractionUpdateDto interactionUpdateDto)
        {
            try
            {
                InteractionDto interaction = await _interactionService.GetInteractionWithIncludesAsync(i => i.Id == interactionUpdateDto.Id, i => i.Client);

                if (interaction.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, UtilitiesNameConstants.InteractionControllerHelper, UtilitiesActionNameConstants.InteractionControllerHelperGetViewModelWhenModelStateIsInvalidForUpdateMethod);

                if (interaction.Status == InteractionStatusEnum.Canceled || interaction.Status == InteractionStatusEnum.Closed)
                    throw new InteractionStatusEditException(InteractionExceptionMessages.InteractionStatusEditExceptionMessage, UtilitiesNameConstants.InteractionControllerHelper, UtilitiesActionNameConstants.InteractionControllerHelperGetViewModelWhenModelStateIsInvalidForUpdateMethod);

                InteractionStatusEnum[] availableInteractionStatuses = EnumUtilities.GetAvailableInteractionStatusesForCurrentStatus(interaction.Status);

                InteractionEditViewModel viewModel = _mapper.Map<InteractionDto, InteractionEditViewModel>(interaction);
                viewModel.AvailableStatuses = ListOfValuesUtilities.CastArrayOfEnumsToSelectListItem<InteractionStatusEnum>(availableInteractionStatuses, interaction.Status);

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}