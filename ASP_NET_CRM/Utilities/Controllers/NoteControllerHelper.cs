﻿using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Exceptions.Utilities;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.ViewModels.Notes;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Utilities.Controllers
{
    public class NoteControllerHelper : INoteControllerHelper
    {
        private readonly INoteService _noteService;
        private readonly IClientService _clientService;
        private readonly IMapper _mapper;

        public NoteControllerHelper(
            INoteService noteService,
            IClientService clientService,
            IMapper mapper
            )
        {
            _noteService = noteService;
            _clientService = clientService;
            _mapper = mapper;
        }

        public async Task<NewNoteViewModel> GetNewNoteViewModelWhenModelStateIsInvalidForAddMethod(NoteSaveDto noteSaveDto)
        {
            try
            {
                NewNoteViewModel viewModel = new NewNoteViewModel();

                if (noteSaveDto.ClientId != 0)
                {
                    ClientDto clientDto = await _clientService.GetClientAsync(noteSaveDto.ClientId);

                    viewModel = _mapper.Map<ClientDto, NewNoteViewModel>(clientDto);
                }
                else
                    viewModel.SetSearchClientValues();

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<NoteEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(NoteUpdateDto noteUpdateDto)
        {
            try
            {
                NoteDto noteDto = await _noteService.GetNoteWithIncludesAsync(n => n.Id == noteUpdateDto.Id, n => n.Client);

                if (noteDto.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, UtilitiesNameConstants.NoteControllerHelper, UtilitiesActionNameConstants.NoteControllerHelperGetViewModelWhenModelStateIsInvalidForUpdateMethod);

                return _mapper.Map<NoteDto, NoteEditViewModel>(noteDto);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}