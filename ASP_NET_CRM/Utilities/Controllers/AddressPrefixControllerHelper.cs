﻿using ASP_NET_CRM.DTO.AddressPrefix;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.ViewModels.AddressPrefixes;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Utilities.Controllers
{
    public class AddressPrefixControllerHelper : IAddressPrefixControllerHelper
    {
        private readonly IAddressPrefixService _addressPrefixService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public AddressPrefixControllerHelper(
            IAddressPrefixService addressPrefixService,
            IApplicationErrorService applicationErrorService, 
            IMapper mapper)
        {
            _addressPrefixService = addressPrefixService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        public void LogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)applicationCustomException;

                exception.SetControllerAndExceptionProperties
                (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    exception.GetType().Name
                );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<AddressPrefixEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(AddressPrefixUpdateDto addressPrefixUpdateDto)
        {
            try
            {
                AddressPrefixDto addressPrefixDto = await _addressPrefixService.GetAddressPrefixAsync(addressPrefixUpdateDto.Id);

                AddressPrefixEditViewModel viewModel = _mapper.Map<AddressPrefixDto, AddressPrefixEditViewModel>(addressPrefixDto);

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}