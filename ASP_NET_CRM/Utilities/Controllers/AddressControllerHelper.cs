﻿using ASP_NET_CRM.Constants.Exceptions.Controllers;
using ASP_NET_CRM.Constants.Exceptions.Models.Client;
using ASP_NET_CRM.Constants.Exceptions.Utilities;
using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.DTO.AddressPrefix;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Client;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.ViewModels.Addresses;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Utilities.Controllers
{
    public class AddressControllerHelper : IAddressControllerHelper
    {
        private readonly IAddressService _addressService;
        private readonly IClientService _clientService;
        private readonly IAddressPrefixService _addressPrefixService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public AddressControllerHelper(
            IAddressService addressService,
            IClientService clientService,
            IAddressPrefixService addressPrefixService,
            IApplicationErrorService applicationErrorService,
            IMapper mapper
            )
        {
            _addressService = addressService;
            _clientService = clientService;
            _addressPrefixService = addressPrefixService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        public async Task<NewAddressViewModel> GetNewAddressViewModelWhenModelStateIsInvalidForAddMethod(AddressSaveDto addressSaveDto)
        {
            try
            {
                NewAddressViewModel viewModel = new NewAddressViewModel();

                if (addressSaveDto.ClientId != 0)
                {
                    ClientDto clientDto = await _clientService.GetClientAsync(addressSaveDto.ClientId);

                    viewModel = _mapper.Map<ClientDto, NewAddressViewModel>(clientDto);
                }
                else
                    viewModel.SetSearchClientValues();

                IEnumerable<AddressPrefixDto> addressPrefixes = await _addressPrefixService.GetAllAddressPrefixes();

                viewModel.AvailablePrefixes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(addressPrefixes);

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<NewAddressViewModel> GetNewAddressViewModelAndLogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext, AddressSaveDto interactionSaveDto)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                applicationCustomException.SetControllerAndExceptionProperties
                    (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    applicationCustomException.GetType().Name
                    );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(applicationCustomException);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                NewAddressViewModel viewModel = new NewAddressViewModel();

                if (applicationCustomException.GetType() != typeof(ClientNotFoundException))
                {
                    ClientDto clientDto = await _clientService.GetClientAsync(interactionSaveDto.ClientId);

                    viewModel = _mapper.Map<ClientDto, NewAddressViewModel>(clientDto);
                }
                else
                    viewModel.SetSearchClientValues();

                IEnumerable<AddressPrefixDto> addressPrefixes = await _addressPrefixService.GetAllAddressPrefixes();

                viewModel.AvailablePrefixes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(addressPrefixes);

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }           
        }

        public async Task<AddressEditViewModel> GetAddressEditViewModelWhenModelStateIsInvalidForUpdateMethod(AddressUpdateDto addressUpdateDto)
        {
            try
            {
                AddressDto addressDto = await _addressService.GetAddressWithIncludesAsync(a => a.Id == addressUpdateDto.Id, a => a.Client);

                if (addressDto.Client == null)
                    throw new ClientNotFoundException(ClientExceptionMessages.ClientNotFoundByIdExceptionMessage, UtilitiesNameConstants.AddressControllerHelper, UtilitiesActionNameConstants.AddressControllerHelperGetAddressEditViewModelWhenModelStateIsInvalidForUpdateMethod);

                IEnumerable<AddressPrefixDto> addressPrefixes = await _addressPrefixService.GetAllAddressPrefixes();

                AddressEditViewModel viewModel = new AddressEditViewModel();
                SetAddressEditViewModelValuesWhenModelStateIsInvalidForUpdateMethod(addressDto, addressUpdateDto, addressPrefixes, ref viewModel);
                
                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetAddressEditViewModelValuesWhenModelStateIsInvalidForUpdateMethod(AddressDto addressDto, AddressUpdateDto addressUpdateDto, IEnumerable<AddressPrefixDto> addressPrefixes,  ref AddressEditViewModel viewModel)
        {
            try
            {
                viewModel.Id = addressDto.Id;
                viewModel.ClientName = addressDto.Client.Name;
                viewModel.Type = addressDto.Type;
                viewModel.FullAddressName = addressDto.FullAddressName;
                viewModel.AvailablePrefixes = ListOfValuesUtilities.CastIEnumerableOfAddressPrefixDtoToSelectListItemWithCurrentPrefixValueProperty(addressPrefixes, addressUpdateDto.PrefixValue);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}