﻿using ASP_NET_CRM.Constants.Exceptions.Models.User;
using ASP_NET_CRM.Constants.Exceptions.Utilities;
using ASP_NET_CRM.DTO.User;
using ASP_NET_CRM.Exceptions.User;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models;
using ASP_NET_CRM.ViewModels.Users;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ASP_NET_CRM.Utilities.Controllers
{
    public class UserControllerHelper : IUserControllerHelper
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserControllerHelper(
            IUserService userService,
            IMapper mapper
            )
        {
            _userService = userService;
            _mapper = mapper;
        }
        public async Task<UserEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(UserUpdateDto userUpdateDto)
        {
            try
            {
                UserDto userDto = await _userService.GetUserAsync(userUpdateDto.Id);

                if (userDto == null)
                    throw new UserNotFoundException(UserExceptionMessages.UserNotFoundByIdExceptionMessage, UtilitiesNameConstants.UserControllerHelper, UtilitiesActionNameConstants.UserControllerHelperGetViewModelWhenModelStateIsInvalidForUpdateMethod);

                UserEditViewModel viewModel = _mapper.Map<UserDto, UserEditViewModel>(userDto);

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}