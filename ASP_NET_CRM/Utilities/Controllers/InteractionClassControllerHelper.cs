﻿using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.InteractionClass;
using ASP_NET_CRM.DTO.InteractionType;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.InteractionClass;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.ViewModels.InteractionClasses;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Utilities.Controllers
{
    public class InteractionClassControllerHelper : IInteractionClassControllerHelper
    {
        private readonly IInteractionClassService _interactionClassService;
        private readonly IInteractionTypeService _interactionTypeService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;

        public InteractionClassControllerHelper(
            IInteractionClassService interactionClassService,
            IInteractionTypeService interactionTypeService,
            IApplicationErrorService applicationErrorService,
            IMapper mapper
            )
        {
            _interactionClassService = interactionClassService;
            _interactionTypeService = interactionTypeService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        public async Task<NewInteractionClassViewModel> GetViewModelWhenModelStateIsInvalidForAddMethod()
        {
            try 
            {
                IEnumerable<InteractionTypeDto> interactionTypeDtos = await _interactionTypeService.GetAllInteractionTypesAsync();

                NewInteractionClassViewModel viewModel = new NewInteractionClassViewModel
                {
                    AvailableTypes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(interactionTypeDtos)
                };

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<NewInteractionClassViewModel> GetViewModelAndLogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)applicationCustomException;

                exception.SetControllerAndExceptionProperties
                (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    exception.GetType().Name
                );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                IEnumerable<InteractionTypeDto> interactionTypeDtos = await _interactionTypeService.GetAllInteractionTypesAsync();

                NewInteractionClassViewModel viewModel = new NewInteractionClassViewModel
                {
                    AvailableTypes = ListOfValuesUtilities.CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(interactionTypeDtos)
                };

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionClassEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(InteractionClassUpdateDto interactionClassUpdateDto)
        {
            try
            {
                InteractionClassDto interactionClassDto = await _interactionClassService.GetInteractionClassWithIncludesAsync(ic => ic.Id == interactionClassUpdateDto.Id, ic => ic.InteractionType);

                InteractionClassEditViewModel viewModel = _mapper.Map<InteractionClassDto, InteractionClassEditViewModel>(interactionClassDto);

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<InteractionClassEditViewModel> GetViewModelAndLogExceptionForUpdateMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext, InteractionClassUpdateDto interactionClassUpdateDto)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)applicationCustomException;

                exception.SetControllerAndExceptionProperties
                (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    exception.GetType().Name
                );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                InteractionClassDto interactionClassDto = await _interactionClassService.GetInteractionClassWithIncludesAsync(ic => ic.Id == interactionClassUpdateDto.Id, ic => ic.InteractionType);

                InteractionClassEditViewModel viewModel = _mapper.Map<InteractionClassDto, InteractionClassEditViewModel>(interactionClassDto);

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}