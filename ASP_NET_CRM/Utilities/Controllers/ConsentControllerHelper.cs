﻿using ASP_NET_CRM.Constants.Exceptions.Models.Consent;
using ASP_NET_CRM.Constants.Exceptions.Utilities;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Consent;
using ASP_NET_CRM.Enums;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Exceptions.Consent;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.ViewModels.Consents;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Utilities.Controllers
{
    public class ConsentControllerHelper : IConsentControllerHelper
    {
        private readonly IConsentService _consentService;
        private readonly IApplicationErrorService _applicationErrorService;
        private readonly IMapper _mapper;


        public ConsentControllerHelper(
            IConsentService consentService,
            IApplicationErrorService applicationErrorService, 
            IMapper mapper
            )
        {
            _consentService = consentService;
            _applicationErrorService = applicationErrorService;
            _mapper = mapper;
        }

        public void LogExceptionForAddMethod(ApplicationCustomException applicationCustomException, ControllerContext controllerContext)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)applicationCustomException;

                exception.SetControllerAndExceptionProperties
                (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    exception.GetType().Name
                );

                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public async Task<ConsentEditViewModel> GetViewModelWhenModelStateIsInvalidForUpdateMethod(ConsentUpdateDto consentUpdateDto)
        {
            try
            {
                ConsentDto consentDto = await _consentService.GetConsentAsync(consentUpdateDto.Id);

                if (consentDto == null)
                    throw new ConsentNotFoundException(ConsentExceptionMessages.ConsentNotFoundByIdExceptionMessage, UtilitiesNameConstants.ConsentControllerHelper, UtilitiesActionNameConstants.ConsentControllerHelperGetViewModelWhenModelStateIsInvalidForUpdateMethod);

                return _mapper.Map<ConsentDto, ConsentEditViewModel>(consentDto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ConsentEditViewModel GetViewModelAndLogExceptionForUpdateMethod(ConsentExistsException consentExistsException, ControllerContext controllerContext, ConsentUpdateDto consentUpdateDto)
        {
            try
            {
                string userFullName = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result.FullName;

                ApplicationCustomException exception = (ApplicationCustomException)consentExistsException;

                exception.SetControllerAndExceptionProperties
                (
                    controllerContext.RouteData.Values["controller"].ToString(),
                    controllerContext.RouteData.Values["action"].ToString(),
                    ControllerType.MVC.ToString(),
                    consentExistsException.GetType().Name
                );
                ApplicationErrorSaveDto applicationErrorSaveDto = _mapper.Map<ApplicationCustomException, ApplicationErrorSaveDto>(exception);

                _applicationErrorService.AddApplicationError(applicationErrorSaveDto, userFullName);

                ConsentEditViewModel viewModel = new ConsentEditViewModel { Id = consentUpdateDto.Id };

                return viewModel;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}