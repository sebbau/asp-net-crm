﻿using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ASP_NET_CRM.Utilities
{
    public static class EnumUtilities
    {
        public static string ShowEnumDisplayAttribute<TEnum>(TEnum enumType) where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            if (Enum.IsDefined(enumType.GetType(), enumType))
            {
                return enumType.GetType()
                    .GetMember(enumType.ToString())
                    .First()
                    .GetCustomAttribute<DisplayAttribute>()
                    .Name;
            }
            else
                return null;
        }

        public static InteractionStatusEnum[] GetAvailableInteractionStatusesForCurrentStatus(InteractionStatusEnum currentStatus)
        {
            try
            {
                InteractionStatusEnum[] availableStatuses;

                switch (currentStatus)
                {
                    case InteractionStatusEnum.New:
                        availableStatuses = new InteractionStatusEnum[3];
                        availableStatuses[0] = InteractionStatusEnum.New;
                        availableStatuses[1] = InteractionStatusEnum.InProgress;
                        availableStatuses[2] = InteractionStatusEnum.Canceled;
                        return availableStatuses;
                    case InteractionStatusEnum.InProgress:
                        availableStatuses = new InteractionStatusEnum[3];
                        availableStatuses[0] = InteractionStatusEnum.InProgress;
                        availableStatuses[1] = InteractionStatusEnum.Canceled;
                        availableStatuses[2] = InteractionStatusEnum.Closed;
                        return availableStatuses;
                    case InteractionStatusEnum.Canceled:
                        return new InteractionStatusEnum[0];
                    case InteractionStatusEnum.Closed:
                        return new InteractionStatusEnum[0];
                }

                throw new ArgumentOutOfRangeException("Argument given in method is out of range.");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}