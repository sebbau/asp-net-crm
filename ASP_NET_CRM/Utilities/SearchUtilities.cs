﻿using System;

namespace ASP_NET_CRM.Utilities
{
    public static class SearchUtilities
    {
        public static int ValidateSearchInt(string searchString)
        {
            int intValue;
            bool isInt = Int32.TryParse(searchString, out intValue);

            if (isInt)
                return intValue;
            else
                return 0;

        }
    }
}