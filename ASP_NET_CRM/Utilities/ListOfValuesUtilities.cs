﻿using ASP_NET_CRM.DTO;
using ASP_NET_CRM.DTO.AddressPrefix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.Utilities
{
    public static class ListOfValuesUtilities
    {
        public static List<SelectListItem> CastIEnumerableDtoToSelectListWithTextAsNameAndValueAsId(IEnumerable<Dto> dtosToCast)
        {
            try
            {
                List<SelectListItem> listOfSelectListItem = new List<SelectListItem>();

                foreach (Dto dto in dtosToCast)
                {
                    SelectListItem selectListItem = new SelectListItem
                    {
                        Text = dto.GetType().GetProperty("Name").GetValue(dto).ToString(),
                        Value = dto.GetType().GetProperty("Id").GetValue(dto).ToString()
                    };
                    listOfSelectListItem.Add(selectListItem);
                }

                return listOfSelectListItem;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> CastArrayOfEnumsToSelectListItem<TEnum>(TEnum[] enumArray, TEnum currentEnumValue) where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            try
            {
                List<SelectListItem> listOfSelectListItem = new List<SelectListItem>();

                foreach (TEnum enumVal in enumArray)
                {
                    if (enumVal.Equals(currentEnumValue))
                    {
                        SelectListItem selectListItem = new SelectListItem
                        {
                            Text = EnumUtilities.ShowEnumDisplayAttribute(enumVal),
                            Value = enumVal.ToString(),
                            Selected = true
                        };

                        listOfSelectListItem.Add(selectListItem);
                    }
                    else
                    {
                        SelectListItem selectListItem = new SelectListItem
                        {
                            Text = EnumUtilities.ShowEnumDisplayAttribute(enumVal),
                            Value = enumVal.ToString(),
                        };

                        listOfSelectListItem.Add(selectListItem);
                    }

                    
                }

                return listOfSelectListItem;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<SelectListItem> CastIEnumerableOfAddressPrefixDtoToSelectListItemWithCurrentPrefixValueProperty(IEnumerable<AddressPrefixDto> addressPrefixDtos, string currentPrefixValue)
        {
            try
            {
                List<SelectListItem> listOfSelectListItems = new List<SelectListItem>();

                foreach (AddressPrefixDto addressPrefixDto in addressPrefixDtos)
                {
                    if (addressPrefixDto.Name.Equals(currentPrefixValue))
                    {
                        SelectListItem selectListItem = new SelectListItem
                        {
                            Text = addressPrefixDto.Name,
                            Value = addressPrefixDto.Id.ToString(),
                            Selected = true
                        };

                        listOfSelectListItems.Add(selectListItem);
                    }
                    else
                    {
                        SelectListItem selectListItem = new SelectListItem
                        {
                            Text = addressPrefixDto.Name,
                            Value = addressPrefixDto.Id.ToString()
                        };

                        listOfSelectListItems.Add(selectListItem);
                    }
                }

                return listOfSelectListItems;
            }
            catch (Exception)
            {
                throw;
            }           
        }
    }
}