﻿using ASP_NET_CRM.Models.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Utilities
{
    public static class EntityUtilities
    {
        public static void SetCreatedAndCreatedByInEntity<T>(ref T entity, string userFullName) where T : Entity
        {
            try
            {
                entity.Created = DateTime.Now;
                entity.CreatedBy = userFullName;
            }
            catch (Exception)
            {
                throw;
            }           
        }
        
        public static void SetLastUpdatedAndLastUpdatedByInEntity<T>(ref T entity, string userFullName) where T : Entity
        {
            try
            {
                entity.LastUpdated = DateTime.Now;
                entity.LastUpdatedBy = userFullName;
            }
            catch (Exception)
            {
                throw;
            }           
        }
    }
}