﻿using ASP_NET_CRM.EntityConfigurations;
using ASP_NET_CRM.Models.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public DbSet<Client> Clients { get; set; }
        
        public DbSet<Consent> Consents { get; set; }

        public DbSet<Interaction> Interactions { get; set; }

        public DbSet<InteractionType> InteractionTypes { get; set; }

        public DbSet<InteractionClass> InteractionClasses { get; set; }

        public DbSet<ApplicationError> ApplicationErrors { get; set; }

        public DbSet<AddressPrefix> AddressPrefixes { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Note> Notes { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new ClientConfiguration());
            modelBuilder.Configurations.Add(new ConsentConfiguration());
            modelBuilder.Configurations.Add(new InteractionConfiguration());
            modelBuilder.Configurations.Add(new ApplicationErrorConfiguration());
            modelBuilder.Configurations.Add(new AddressPrefixConfiguration());
            modelBuilder.Configurations.Add(new InteractionTypeConfiguration());
            modelBuilder.Configurations.Add(new InteractionClassConfiguration());
            modelBuilder.Configurations.Add(new AddressConfiguration());
            modelBuilder.Configurations.Add(new NoteConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}