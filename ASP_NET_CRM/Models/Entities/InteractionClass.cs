﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Entities
{
    public class InteractionClass : Entity
    {
        public string Name { get; set; }

        public InteractionType InteractionType { get; set; }

        public int InteractionTypeId { get; set; }
    }
}