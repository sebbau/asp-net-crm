﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Entities
{
    public class Consent : Entity
    {
        public string Name { get; set; }

        public string FullName { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
    }
}