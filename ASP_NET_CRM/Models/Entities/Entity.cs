﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Entities
{
    public abstract class Entity
    {
        public int Id { get; set; }

        public DateTime Created { get; set; }

        [Required]
        [MaxLength(201)]
        public string CreatedBy { get; set; }

        public DateTime? LastUpdated { get; set; }

        [MaxLength(201)]
        public string LastUpdatedBy { get; set; }
    }
}