﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Entities
{
    public class Note : Entity
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public virtual Client Client { get; set; }

        public int ClientId { get; set; }
    }
}