﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Entities
{
    public class InteractionType : Entity
    {
        public string Name { get; set; }

        public virtual ICollection<InteractionClass> InteractionClasses { get; set; }
    }
}