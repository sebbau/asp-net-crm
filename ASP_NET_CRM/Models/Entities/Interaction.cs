﻿using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Entities
{
    public class Interaction : Entity
    {
        public string Type { get; set; }

        public string Class { get; set; }

        public DateTime InteractionDate { get; set; }

        public string Description { get; set; }

        public InteractionStatusEnum Status { get; set; }

        public virtual Client Client { get; set; }

        public int ClientId { get; set; }

    }
}