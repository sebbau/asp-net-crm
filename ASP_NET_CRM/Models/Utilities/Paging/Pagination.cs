﻿using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Utilities.Paging
{
    public class Pagination
    {
        public int StartPage { get; set; }

        public int EndPage { get; set; }

        public int CurrentPage { get; set; } = 1;
    }
}