﻿using ASP_NET_CRM.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Utilities.Paging
{
    public class PaginationExtendedLightweight : Pagination
    {
        public int RecordsPerPage { get; set; } = GlobalVariables.defaultRecordsPerPage;
    }
}