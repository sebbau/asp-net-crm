﻿using ASP_NET_CRM.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Utilities.Paging
{
    public class Page<T> where T : Dto
    {
        public IEnumerable<Dto> Data { get; set; }

        public Pagination Pagination { get; set; }
    }
}