﻿using ASP_NET_CRM.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Utilities.Paging
{
    public class AddressPagination : Pagination
    {
        public int RecordsPerPage { get; set; } = GlobalVariables.defaultRecordsPerPage;

        public string SearchId { get; set; }

        public string SearchStreetName { get; set; }

        public string SearchHouseNumber { get; set; }

        public string SearchApartmentNumber { get; set; }

        public string SearchCity { get; set; }

        public string SearchPostalCode { get; set; }

        public bool IsSearchBy { get; set; }
    }
}