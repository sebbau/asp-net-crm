﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Models.Utilities
{
    public class ErrorMessage
    {
        public ErrorMessage(string message)
        {
            Message = message;
        }

        public ErrorMessage(string message, IEnumerable<string> errors)
        {
            Message = message;
            Errors = errors;
        }

        public string Message { get; }

        public IEnumerable<string> Errors { get; }
    }
}