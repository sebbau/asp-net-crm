﻿using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Notes
{
    public class NotesClientDetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<NoteDto> Notes { get; set; }

        public Pagination Pagination { get; set; }
    }
}