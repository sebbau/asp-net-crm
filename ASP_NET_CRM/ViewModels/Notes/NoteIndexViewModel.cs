﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Notes
{
    public class NoteIndexViewModel
    {
        public IEnumerable<NoteDto> Notes { get; set; }

        public PaginationExtended Pagination { get; set; }

        public IEnumerable<int> RecordsPerPageList { get; set; }

        public IEnumerable<string> SearchPropertyList { get; set; }

        public NoteIndexViewModel()
        {
            RecordsPerPageList = new int[]
            {
                PaginationConstants.tenRecords,
                PaginationConstants.fifteenRecords,
                PaginationConstants.twentyRecords
            };

            SearchPropertyList = new string[]
            {
                NoteConstants.Id,
                NoteConstants.ClientName,
                NoteConstants.Title
            };
        }
    }
}