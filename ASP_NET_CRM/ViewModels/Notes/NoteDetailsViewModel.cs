﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Notes
{
    public class NoteDetailsViewModel : ViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int ClientId { get; set; }

        [Display(Name = "Client Name")]
        public string ClientName { get; set; }
    }
}