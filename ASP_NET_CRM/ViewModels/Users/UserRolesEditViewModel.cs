﻿using ASP_NET_CRM.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Users
{
    public class UserRolesEditViewModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public List<UserRoleUpdateDto> Roles { get; set; }
    }
}