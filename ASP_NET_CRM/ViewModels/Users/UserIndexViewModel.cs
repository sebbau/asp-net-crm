﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.User;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Users
{
    public class UserIndexViewModel
    {
        public IEnumerable<UserDto> Users { get; set; }

        public PaginationExtended Pagination { get; set; }

        public IEnumerable<int> RecordsPerPageList { get; set; }

        public IEnumerable<string> SearchPropertyList { get; set; }

        public UserIndexViewModel()
        {
            RecordsPerPageList = new int[]
            {
                PaginationConstants.tenRecords,
                PaginationConstants.fifteenRecords,
                PaginationConstants.twentyRecords
            };

            SearchPropertyList = new string[]
            {
                UserConstants.Email,
                UserConstants.FirstName,
                UserConstants.Surname,
                UserConstants.FullName
            };
        }
    }
}