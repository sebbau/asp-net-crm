﻿using ASP_NET_CRM.DTO.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Users
{
    public class UserRolesDetailsViewModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public IEnumerable<RoleDto> Roles { get; set; }

        public IEnumerable<string> UserRoles { get; set; }
    }
}