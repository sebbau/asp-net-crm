﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Users
{
    public class UserEditViewModel
    {
        public string Id { get; set; }

        [Display(Name = "User Name / Email")]
        public string UserName { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        public string Surname { get; set; }

        [Display(Name = "Full Name")]
        public string FullName { get; set; }
    }
}