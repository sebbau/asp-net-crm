﻿using ASP_NET_CRM.DTO.Consent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Consents
{
    public class ConsentsClientDetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<ConsentDto> Consents { get; set; }

        public IEnumerable<ConsentDto> ClientConsents { get; set; }
    }
}