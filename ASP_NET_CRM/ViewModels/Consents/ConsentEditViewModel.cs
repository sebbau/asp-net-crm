﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Consents
{
    public class ConsentEditViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "Full Name")]
        public string FullName { get; set; }
    }
}