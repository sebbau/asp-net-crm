﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.InteractionTypes
{
    public class InteractionTypeEditViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}