﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.InteractionClasses
{
    public class InteractionClassDetailsViewModel : ViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "Type")]
        public string InteractionTypeName { get; set; }
    }
}