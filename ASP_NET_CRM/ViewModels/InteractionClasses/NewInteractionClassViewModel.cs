﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.ViewModels.InteractionClasses
{
    public class NewInteractionClassViewModel
    {
        public string Name { get; set; }

        [Display(Name = "Type")]
        public int InteractionTypeId { get; set; }

        public List<SelectListItem> AvailableTypes { get; set; }
    }
}