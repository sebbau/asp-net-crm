﻿using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Clients
{
    public class IndividualClientEditViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Client Type")]
        public ClientTypeEnum ClientType { get; set; }

        public string Name { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Pesel { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [Display(Name = "Document Issue Date")]
        public DateTime? DocumentIssueDate { get; set; }

        [Display(Name = "Document Issued By")]
        public string DocumentIssuedBy { get; set; }

        [Display(Name = "Document Type")]
        public ClientDocumentType DocumentType { get; set; }
    }
}