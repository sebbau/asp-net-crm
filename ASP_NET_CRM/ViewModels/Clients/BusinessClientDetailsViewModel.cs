﻿using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Clients
{
    public class BusinessClientDetailsViewModel : ViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Client Type")]
        public ClientTypeEnum ClientType { get; set; }

        public string Name { get; set; }

        public string NIP { get; set; }

        [Display(Name = "Main Address Full Name")]
        public string MainAddressFullName { get; set; }

        [Display(Name = "Main Address Postal Code")]
        public string MainAddressPostalCode { get; set; }

        [Display(Name = "Main Address City")]
        public string MainAddressCity { get; set; }

        [Display(Name = "Main Address State")]
        public AddressStateEnum MainAddressState { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Regon { get; set; }

        public string KRS { get; set; }

        [Display(Name = "Web Site")]
        public string WebSite { get; set; }
    }
}