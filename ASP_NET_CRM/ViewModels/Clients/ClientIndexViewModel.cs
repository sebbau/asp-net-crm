﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASP_NET_CRM.Constants;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Models.Utilities;
using ASP_NET_CRM.Models.Utilities.Paging;

namespace ASP_NET_CRM.ViewModels.Clients
{
    public class ClientIndexViewModel
    {
        public IEnumerable<ClientDto> Clients { get; set; }
        public PaginationExtended Pagination { get; set; }
        public IEnumerable<int> RecordsPerPageList { get; set; }
        public IEnumerable<string> SearchPropertyList { get; set; }

        public ClientIndexViewModel()
        {
            RecordsPerPageList = new int[] 
            { 
                PaginationConstants.tenRecords, 
                PaginationConstants.fifteenRecords, 
                PaginationConstants.twentyRecords 
            };

            SearchPropertyList = new string[]
            {
                ClientConstants.id,
                ClientConstants.name,
                ClientConstants.pesel,
                ClientConstants.nip
            };
        }
    }
}