﻿using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Clients
{
    public class IndividualClientDetailsViewModel : ViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Client Type")]
        public ClientTypeEnum ClientType { get; set; }

        public string Name { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        public string Surname { get; set; }
        
        public string Pesel { get; set; }

        [Display(Name = "Main Address Full Name")]
        public string MainAddressFullName { get; set; }

        [Display(Name = "Main Address Postal Code")]
        public string MainAddressPostalCode { get; set; }

        [Display(Name = "Main Address City")]
        public string MainAddressCity { get; set; }

        [Display(Name = "Main Address State")]
        public AddressStateEnum MainAddressState { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        [Display(Name = "Document Number")]
        public string DocumentNumber { get; set; }

        [Display(Name = "Document Issue Date")]
        public DateTime? DocumentIssueDate { get; set; }

        [Display(Name = "Document Issued By")]
        public string DocumentIssuedBy { get; set; }

        [Display(Name = "Document Type")]
        public ClientDocumentType DocumentType { get; set; }
    }
}