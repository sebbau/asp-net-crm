﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.AddressPrefixes
{
    public class NewAddressPrefixViewModel
    {
        public string Name { get; set; }
    }
}