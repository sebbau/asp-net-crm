﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.ApplicationErrors
{
    public class ApplicationErrorIndexViewModel
    {
        public IEnumerable<ApplicationErrorDto> ApplicationErrors { get; set; }

        public PaginationExtendedLightweight Pagination { get; set; }

        public IEnumerable<int> RecordsPerPageList { get; set; }

        public ApplicationErrorIndexViewModel()
        {
            RecordsPerPageList = new int[]
            {
                PaginationConstants.tenRecords,
                PaginationConstants.fifteenRecords,
                PaginationConstants.twentyRecords
            };
        }
    }
}