﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;

namespace ASP_NET_CRM.ViewModels.ApplicationErrors
{
    public class ApplicationErrorDetailsViewModel : ViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Controller Name")]
        public string ControllerName { get; set; }

        [Display(Name = "Controller Action Name")]
        public string ControllerActionName { get; set; }

        [Display(Name = "Controller Type")]
        public string ControllerType { get; set; }

        [Display(Name = "Exception Type")]
        public string ExceptionType { get; set; }

        [Display(Name = "Class Name")]
        public string ClassName { get; set; }

        [Display(Name = "Action Name")]
        public string ActionName { get; set; }

        [Display(Name = "Http Status Code")]
        public HttpStatusCode HttpStatusCode { get; set; }

        public string Message { get; set; }
    }
}