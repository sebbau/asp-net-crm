﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Roles
{
    public class RoleDetailsViewModel : ViewModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}