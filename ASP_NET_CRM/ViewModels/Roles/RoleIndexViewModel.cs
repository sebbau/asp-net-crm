﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.DTO.Role;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Roles
{
    public class RoleIndexViewModel
    {
        public IEnumerable<RoleDto> Roles { get; set; }

        public PaginationExtendedLightweight Pagination { get; set; }

        public IEnumerable<int> RecordsPerPageList { get; set; }

        public RoleIndexViewModel()
        {
            RecordsPerPageList = new int[]
            {
                PaginationConstants.tenRecords,
                PaginationConstants.fifteenRecords,
                PaginationConstants.twentyRecords
            };
        }
    }
}