﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.Interaction;
using ASP_NET_CRM.Models.Utilities;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Interactions
{
    public class InteractionIndexViewModel
    {
        public IEnumerable<InteractionDto> Interactions { get; set; }

        public PaginationExtended Pagination { get; set; }

        public IEnumerable<int> RecordsPerPageList { get; set; }

        public IEnumerable<string> SearchPropertyList { get; set; }

        public InteractionIndexViewModel()
        {
            RecordsPerPageList = new int[]
            {
                PaginationConstants.tenRecords,
                PaginationConstants.fifteenRecords,
                PaginationConstants.twentyRecords
            };

            SearchPropertyList = new string[]
            {
                InteractionConstants.Id,
                InteractionConstants.ClientName,
                InteractionConstants.Type,
                InteractionConstants.Class
            };
        }
    }
}