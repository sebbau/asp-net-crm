﻿using ASP_NET_CRM.Constants.SearchConstants;
using ASP_NET_CRM.DTO.InteractionClass;
using ASP_NET_CRM.DTO.InteractionType;
using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.ViewModels.Interactions
{
    public class NewInteractionViewModel
    {
        public int ClientId { get; set; }

        [Display(Name = "Client Name")]
        public string ClientName { get; set; }

        [Display(Name = "Type")]
        public int TypeId { get; set; }

        public List<SelectListItem> AvailableTypes { get; set; }

        [Display(Name = "Class")]
        public int ClassId { get; set; }

        [Display(Name = "Interaction Date")]
        public DateTime? InteractionDate { get; set; }

        public string Description { get; set; }

        public List<SelectListItem> AvailableClasses { get; set; } = new List<SelectListItem>();

        public InteractionStatusEnum Status { get; } = InteractionStatusEnum.New;

        public IEnumerable<string> SearchClientPropertyList { get; set; }

        public bool IsShowSearchButton { get; set; } = false;

        public void SetSearchClientValues()
        {
            SearchClientPropertyList = new string[]
            {
                ClientConstants.id,
                ClientConstants.name,
                ClientConstants.pesel,
                ClientConstants.nip
            };

            IsShowSearchButton = true;
        }
    }
}