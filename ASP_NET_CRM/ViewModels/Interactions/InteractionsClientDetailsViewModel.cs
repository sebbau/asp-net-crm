﻿using ASP_NET_CRM.DTO.Interaction;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Interactions
{
    public class InteractionsClientDetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<InteractionDto> Interactions { get; set; }

        public Pagination Pagination { get; set; }
    }
}