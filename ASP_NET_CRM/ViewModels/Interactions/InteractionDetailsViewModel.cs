﻿using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Interactions
{
    public class InteractionDetailsViewModel : ViewModel
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public string Class { get; set; }

        public InteractionStatusEnum Status { get; set; }

        [Display(Name = "Interaction Date")]
        public DateTime InteractionDate { get; set; }

        public string Description { get; set; }

        public int ClientId { get; set; }

        [Display(Name = "Client Name")]
        public string ClientName { get; set; }
    }
}