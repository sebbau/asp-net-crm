﻿using ASP_NET_CRM.Constants;
using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Addresses
{
    public class AddressIndexViewModel
    {
        public IEnumerable<AddressDto> Addresses { get; set; }

        public AddressPagination AddressSearch { get; set; }

        public IEnumerable<int> RecordsPerPageList { get; set; }

        public AddressIndexViewModel()
        {
            RecordsPerPageList = new int[]
            {
                PaginationConstants.tenRecords,
                PaginationConstants.fifteenRecords,
                PaginationConstants.twentyRecords
            };
        }
    }
}