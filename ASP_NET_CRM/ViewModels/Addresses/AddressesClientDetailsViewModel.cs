﻿using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.Models.Utilities.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.ViewModels.Addresses
{
    public class AddressesClientDetailsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<AddressLightweightDto> Addresses { get; set; }

        public Pagination Pagination { get; set; }
    }
}