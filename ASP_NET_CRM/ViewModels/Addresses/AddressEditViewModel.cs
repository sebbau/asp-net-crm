﻿using ASP_NET_CRM.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP_NET_CRM.ViewModels.Addresses
{
    public class AddressEditViewModel
    {
        public int Id { get; set; }

        public AddressType Type { get; set; }

        [Display(Name = "Prefix")]
        public string PrefixId { get; set; }

        public string PrefixValue { get; set; }

        public List<SelectListItem> AvailablePrefixes { get; set; }

        [Display(Name = "Street Name")]
        public string StreetName { get; set; }

        [Display(Name = "House Number")]
        public string HouseNumber { get; set; }

        [Display(Name = "Apartment Number")]
        public string ApartmentNumber { get; set; }

        [Display(Name = "Full Address Name")]
        public string FullAddressName { get; set; }

        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }

        public string City { get; set; }

        public AddressStateEnum State { get; set; }

        public string Description { get; set; }

        [Display(Name = "Client Name")]
        public string ClientName { get; set; }
    }
}