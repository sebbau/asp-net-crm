﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.EntityConfigurations
{
    public class AddressPrefixConfiguration : EntityTypeConfiguration<AddressPrefix>
    {
        public AddressPrefixConfiguration()
        {
            Property(ap => ap.Name)
                .IsRequired()
                .HasMaxLength(10);
        }
    }
}