﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.EntityConfigurations
{
    public class ConsentConfiguration : EntityTypeConfiguration<Consent>
    {
        public ConsentConfiguration()
        {
            Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(50);

            Property(c => c.FullName)
                .IsRequired()
                .HasMaxLength(250);
        }
    }
}