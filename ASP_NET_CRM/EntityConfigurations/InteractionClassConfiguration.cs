﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.EntityConfigurations
{
    public class InteractionClassConfiguration : EntityTypeConfiguration<InteractionClass>
    {
        public InteractionClassConfiguration()
        {
            Property(ic => ic.Name)
                .IsRequired()
                .HasMaxLength(50);

            HasRequired(ic => ic.InteractionType)
                .WithMany(it => it.InteractionClasses)
                .HasForeignKey(ic => ic.InteractionTypeId)
                .WillCascadeOnDelete(true);
        }
    }
}