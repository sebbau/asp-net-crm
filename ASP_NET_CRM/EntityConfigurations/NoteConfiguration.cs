﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.EntityConfigurations
{
    public class NoteConfiguration : EntityTypeConfiguration<Note>
    {
        public NoteConfiguration()
        {
            Property(n => n.Title)
                .IsRequired()
                .HasMaxLength(200);

            Property(n => n.Description)
                .HasMaxLength(2000);

            HasRequired(n => n.Client)
                .WithMany(c => c.Notes)
                .HasForeignKey(n => n.ClientId)
                .WillCascadeOnDelete(true);
        }
    }
}