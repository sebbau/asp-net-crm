﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.EntityConfigurations
{
    public class AddressConfiguration : EntityTypeConfiguration<Address>
    {
        public AddressConfiguration()
        {
            Property(a => a.Type)
                .IsRequired();

            Property(a => a.Prefix)
                .HasMaxLength(10);

            Property(a => a.StreetName)
                .HasMaxLength(200);

            Property(a => a.HouseNumber)
                .IsRequired()
                .HasMaxLength(6);

            Property(a => a.ApartmentNumber)
                .HasMaxLength(10);

            Property(a => a.FullAddressName)
                .IsRequired()
                .HasMaxLength(230);

            Property(a => a.PostalCode)
                .IsRequired()
                .HasMaxLength(6);

            Property(a => a.City)
                .IsRequired()
                .HasMaxLength(50);

            Property(a => a.State)
                .IsRequired();

            HasRequired(a => a.Client)
                .WithMany(c => c.Addresses)
                .HasForeignKey(a => a.ClientId)
                .WillCascadeOnDelete(true);
        }
    }
}