﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.EntityConfigurations
{
    public class InteractionConfiguration : EntityTypeConfiguration<Interaction>
    {
        public InteractionConfiguration()
        {
            Property(i => i.Type)
                .IsRequired();

            Property(i => i.Class)
                .IsRequired();

            Property(i => i.InteractionDate)
                .IsRequired();

            Property(i => i.Description)
                .HasMaxLength(2000);

            HasRequired(i => i.Client)
                .WithMany(c => c.Interactions)
                .HasForeignKey(i => i.ClientId)
                .WillCascadeOnDelete(true);
        }
    }
}