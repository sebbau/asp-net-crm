﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.EntityConfigurations
{
    public class InteractionTypeConfiguration : EntityTypeConfiguration<InteractionType>
    {
        public InteractionTypeConfiguration()
        {
            Property(it => it.Name)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}