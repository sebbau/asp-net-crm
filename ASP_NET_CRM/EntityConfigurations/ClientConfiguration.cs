﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.EntityConfigurations
{
    public class ClientConfiguration : EntityTypeConfiguration<Client>
    {
        public ClientConfiguration()
        {
            Property(c => c.ClientType)
                .IsRequired();

            Property(c => c.Name)
                .HasMaxLength(250);

            Property(c => c.FirstName)
                .HasMaxLength(50);

            Property(c => c.Surname)
                .HasMaxLength(150);

            Property(c => c.Pesel)
                .HasMaxLength(11);

            Property(c => c.NIP)
                .HasMaxLength(10);

            Property(c => c.Regon)
                .HasMaxLength(14);

            Property(c => c.KRS)
                .HasMaxLength(10);

            Property(c => c.WebSite)
                .HasMaxLength(250);

            Property(c => c.PhoneNumber)
                .IsRequired()
                .HasMaxLength(12);

            Property(c => c.Email)
                .HasMaxLength(200);

            Property(c => c.DocumentType)
                .IsOptional();

            Property(c => c.DocumentNumber)
                .HasMaxLength(15);

            Property(c => c.DocumentIssueDate)
                .IsOptional();

            Property(c => c.DocumentIssuedBy)
                .HasMaxLength(200);

            HasMany(cl => cl.Consents)
                .WithMany(cn => cn.Clients)
                .Map
                (
                    clcn =>
                    {
                        clcn.ToTable("Client_Consent");
                        clcn.MapLeftKey("ClientId");
                        clcn.MapRightKey("ConsentId");
                    }
                );
        }
    }
}