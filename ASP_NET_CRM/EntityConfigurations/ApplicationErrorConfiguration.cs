﻿using ASP_NET_CRM.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.EntityConfigurations
{
    public class ApplicationErrorConfiguration : EntityTypeConfiguration<ApplicationError>
    {
        public ApplicationErrorConfiguration()
        {
            Property(ae => ae.ActionName)
                .HasMaxLength(200);

            Property(ae => ae.ClassName)
                .HasMaxLength(200);

            Property(ae => ae.ControllerActionName)
                .HasMaxLength(200);

            Property(ae => ae.ControllerName)
                .HasMaxLength(200);

            Property(ae => ae.ControllerType)
                .HasMaxLength(3);

            Property(ae => ae.ExceptionType)
                .HasMaxLength(200);

            Property(ae => ae.HttpStatusCode)
                .IsRequired();

            Property(ae => ae.Message)
                .HasMaxLength(2500);
        }
    }
}