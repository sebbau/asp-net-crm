﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ASP_NET_CRM.Exceptions.Address
{
    public class AddressNotFoundException : ApplicationCustomException
    {
        public AddressNotFoundException(string message, string className, string actionName)
            : base(message, className, actionName)
        {
            HttpStatusCode = HttpStatusCode.NotFound;
        }
    }
}