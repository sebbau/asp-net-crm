﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ASP_NET_CRM.Exceptions
{
    public abstract class ApplicationCustomException : Exception
    {
        public string ControllerName { get; set; }

        public string ControllerActionName { get; set; }

        public string ControllerType { get; set; }

        public string ExceptionType { get; set; }

        public string ClassName { get; }

        public string ActionName { get; }

        public HttpStatusCode HttpStatusCode { get; set; }

        public new string Message { get; }

        public ApplicationCustomException(string message, string className, string actionName)
        {
            Message = message;
            ClassName = className;
            ActionName = actionName;           
        }

        public void SetControllerAndExceptionProperties(string controllerName, string controllerActionName, string controllerType, string exceptionType)
        {
            ControllerName = controllerName;
            ControllerActionName = controllerActionName;
            ControllerType = controllerType;
            ExceptionType = exceptionType;
        }
    }
}