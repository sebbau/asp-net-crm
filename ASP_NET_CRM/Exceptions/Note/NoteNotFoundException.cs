﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ASP_NET_CRM.Exceptions.Note
{
    public class NoteNotFoundException : ApplicationCustomException
    {
        public NoteNotFoundException(string message, string className, string actionName) 
            : base(message, className, actionName)
        {
            HttpStatusCode = HttpStatusCode.NotFound;
        }
    }
}