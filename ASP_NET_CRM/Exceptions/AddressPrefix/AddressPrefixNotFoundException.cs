﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ASP_NET_CRM.Exceptions.AddressPrefix
{
    public class AddressPrefixNotFoundException : ApplicationCustomException
    {
        public AddressPrefixNotFoundException(string message, string className, string actionName)
            : base(message, className, actionName)
        {
            HttpStatusCode = HttpStatusCode.NotFound;
        }
    }
}