using ASP_NET_CRM.App_Start;
using ASP_NET_CRM.Controllers;
using ASP_NET_CRM.Controllers.API;
using ASP_NET_CRM.Interfaces.Services;
using ASP_NET_CRM.Interfaces.UnitOfWork;
using ASP_NET_CRM.Interfaces.Utilities.Controllers;
using ASP_NET_CRM.Interfaces.Utilities.MVC.Controllers;
using ASP_NET_CRM.Models;
using ASP_NET_CRM.Repositories.UnitOfWork;
using ASP_NET_CRM.Services;
using ASP_NET_CRM.Utilities.Controllers;
using ASP_NET_CRM.Utilities.MVC.Controllers;
using ASP_NET_CRM.Utilities.Paging;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Data.Entity;
using System.Web;
using Unity;
using Unity.Injection;

namespace ASP_NET_CRM
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>();
            container.RegisterType<DbContext, ApplicationDbContext>();
            container.RegisterType<UserManager<ApplicationUser>>();
            container.RegisterFactory<IAuthenticationManager>(
                    o => System.Web.HttpContext.Current.GetOwinContext().Authentication
            );
            container.RegisterType<AccountController>();
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<IClientService, ClientService>();
            container.RegisterType<IConsentService, ConsentService>();
            container.RegisterType<IInteractionService, InteractionService>();
            container.RegisterType<IInteractionTypeService, InteractionTypeService>();
            container.RegisterType<IInteractionClassService, InteractionClassService>();
            container.RegisterType<IInteractionControllerHelper, InteractionControllerHelper>();
            container.RegisterType<IApplicationErrorService, ApplicationErrorService>();
            container.RegisterType<IClientControllerHelper, ClientControllerHelper>();
            container.RegisterType<IAddressService, AddressService>();
            container.RegisterType<IAddressPrefixService, AddressPrefixService>();
            container.RegisterType<IAddressControllerHelper, AddressControllerHelper>();
            container.RegisterType<INoteService, NoteService>();
            container.RegisterType<INoteControllerHelper, NoteControllerHelper>();
            container.RegisterType<IRoleService, RoleService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IUserControllerHelper, UserControllerHelper>();
            container.RegisterType<IConsentControllerHelper, ConsentControllerHelper>();
            container.RegisterType<IInteractionClassControllerHelper, InteractionClassControllerHelper>();
            container.RegisterType<IAddressPrefixControllerHelper, AddressPrefixControllerHelper>();
            container.RegisterType<IInteractionTypeControllerHelper, InteractionTypeControllerHelper>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });

            container.RegisterInstance<IMapper>(config.CreateMapper());
        }
    }
}