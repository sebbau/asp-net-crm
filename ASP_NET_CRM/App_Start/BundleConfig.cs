﻿using System.Web;
using System.Web.Optimization;

namespace ASP_NET_CRM
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                    "~/Scripts/jquery-{version}.js",
                    "~/Scripts/umd/popper.min.js",
                    "~/Scripts/bootstrap.min.js"                   
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                    "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/cssjqryUi").Include(
                   "~/Content/themes/base/jquery-ui.css"));

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/Site.css",
                      "~/Content/Fontawesome/css/all.css"));

            bundles.Add(new ScriptBundle("~/Shared/SearchClientModal").Include(
                      "~/Scripts/CustomScripts/Shared/SearchClientModal/SearchClientModalVariables.js",
                      "~/Scripts/CustomScripts/Shared/SearchClientModal/SearchClientModalOpen.js",
                      "~/Scripts/CustomScripts/Shared/SearchClientModal/SearchClientModalHide.js",
                      "~/Scripts/CustomScripts/Shared/SearchClientModal/SearchClientModalBodyScroll.js",
                      "~/Scripts/CustomScripts/Shared/SearchClientModal/SearchClientModalSearchByPropertyClick.js",
                      "~/Scripts/CustomScripts/Shared/SearchClientModal/SearchClientModalSearchAllClick.js",
                      "~/Scripts/CustomScripts/Shared/SearchClientModal/SearchClientModalDisplayValuesUtility.js",
                      "~/Scripts/CustomScripts/Shared/SearchClientModal/SearchClientModalClientChooseClick.js",
                      "~/Scripts/CustomScripts/Shared/SearchClientModal/SearchClientModalSearchStringKeyup.js"
                      ));

            bundles.Add(new ScriptBundle("~/Shared/RecordsPerPage").Include(
                      "~/Scripts/CustomScripts/Shared/RecordsPerPage/RecordsPerPageScript.js"));

            bundles.Add(new ScriptBundle("~/Client/DeleteClientAsync").Include(
                      "~/Scripts/CustomScripts/Views/Client/DeleteClient/DeleteClientModalOpen.js",
                      "~/Scripts/CustomScripts/Views/Client/DeleteClient/DeleteClientAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/Interaction/DeleteInteraction").Include(
                      "~/Scripts/CustomScripts/Views/Interaction/DeleteInteraction/DeleteInteractionModalOpen.js",
                      "~/Scripts/CustomScripts/Views/Interaction/DeleteInteraction/DeleteInteractionAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/Address/DeleteAddress").Include(
                      "~/Scripts/CustomScripts/Views/Address/DeleteAddress/DeleteAddressModalOpen.js",
                      "~/Scripts/CustomScripts/Views/Address/DeleteAddress/DeleteAddressAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/Note/DeleteNote").Include(
                      "~/Scripts/CustomScripts/Views/Note/DeleteNote/DeleteNoteModalOpen.js",
                      "~/Scripts/CustomScripts/Views/Note/DeleteNote/DeleteNoteAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/User/DeleteUser").Include(
                      "~/Scripts/CustomScripts/Views/User/DeleteUser/DeleteUserModalOpen.js",
                      "~/Scripts/CustomScripts/Views/User/DeleteUser/DeleteUserAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/Consent/DeleteConsent").Include(
                      "~/Scripts/CustomScripts/Views/Consent/DeleteConsent/DeleteConsentModalOpen.js",
                      "~/Scripts/CustomScripts/Views/Consent/DeleteConsent/DeleteConsentAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/InteractionClass/DeleteInteractionClass").Include(
                      "~/Scripts/CustomScripts/Views/InteractionClass/DeleteInteractionClass/DeleteInteractionClassModalOpen.js",
                      "~/Scripts/CustomScripts/Views/InteractionClass/DeleteInteractionClass/DeleteInteractionClassAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/AddressPrefix/DeleteAddressPrefix").Include(
                      "~/Scripts/CustomScripts/Views/AddressPrefix/DeleteAddressPrefix/DeleteAddressPrefixModalOpen.js",
                      "~/Scripts/CustomScripts/Views/AddressPrefix/DeleteAddressPrefix/DeleteAddressPrefixAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/InteractionType/DeleteInteractionType").Include(
                      "~/Scripts/CustomScripts/Views/InteractionType/DeleteInteractionType/DeleteInteractionTypeModalOpen.js",
                      "~/Scripts/CustomScripts/Views/InteractionType/DeleteInteractionType/DeleteInteractionTypeAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/Messages").Include(
                      "~/Scripts/CustomScripts/Messages/SuccessMessageScript.js",
                      "~/Scripts/CustomScripts/Messages/ErrorMessageScript.js",
                      "~/Scripts/CustomScripts/Messages/WarningMessageAsyncScript.js",
                      "~/Scripts/CustomScripts/Messages/GlobalMessageAsyncScript.js"
                      ));

            bundles.Add(new ScriptBundle("~/Utilities").Include(
                      "~/Scripts/CustomScripts/Utilities/GoBackScript.js"));

            bundles.Add(new ScriptBundle("~/Readonly").Include(
                      "~/Scripts/CustomScripts/Utilities/ReadonlyScript.js"));

            bundles.Add(new ScriptBundle("~/ReadonlyClass").Include(
                      "~/Scripts/CustomScripts/Utilities/ReadonlyClassScript.js"));

            bundles.Add(new ScriptBundle("~/ClearAlerts").Include(
                      "~/Scripts/CustomScripts/Utilities/ClearAlertsScript.js"));

            bundles.Add(new ScriptBundle("~/Tooltip").Include(
                      "~/Scripts/CustomScripts/Utilities/TooltipScript.js"));

            bundles.Add(new ScriptBundle("~/TooltipSearchClientModal").Include(
                      "~/Scripts/CustomScripts/Utilities/TooltipForSearchClientModal.js"));

            bundles.Add(new ScriptBundle("~/Views/Interaction/NewView").Include(
                      "~/Scripts/CustomScripts/Views/Interaction/NewViewScripts.js"));

            bundles.Add(new ScriptBundle("~/Views/Client/IndividualClientEditView").Include(
                      "~/Scripts/CustomScripts/Views/Client/IndividualClientEditViewScript.js"));

            bundles.Add(new ScriptBundle("~/Views/Client/NewView").Include(
                      "~/Scripts/CustomScripts/Views/Client/NewViewScript.js"));

            bundles.Add(new ScriptBundle("~/Views/Address/NewView").Include(
                      "~/Scripts/CustomScripts/Views/Address/NewViewScript.js"));

            bundles.Add(new ScriptBundle("~/Views/Address/EditView").Include(
                      "~/Scripts/CustomScripts/Views/Address/EditViewScript.js"));

            bundles.Add(new ScriptBundle("~/WebAPIGetToken").Include(
                      "~/Scripts/CustomScripts/Shared/WebAPI/WebAPIGetToken.js"));

            bundles.Add(new ScriptBundle("~/WebAPIRemoveToken").Include(
                      "~/Scripts/CustomScripts/Shared/WebAPI/WebAPIRemoveToken.js"));

            bundles.Add(new ScriptBundle("~/Views/User/Register").Include(
                      "~/Scripts/CustomScripts/Views/User/RegisterScript.js"));

            bundles.Add(new ScriptBundle("~/Views/User/EditView").Include(
                      "~/Scripts/CustomScripts/Views/User/EditViewScript.js"));

            bundles.Add(new ScriptBundle("~/Views/InteractionClass/NewView").Include(
          "~/Scripts/CustomScripts/Views/InteractionClass/NewViewScript.js"));

        }
    }
}
