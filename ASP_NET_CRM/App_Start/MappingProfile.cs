﻿using ASP_NET_CRM.DTO.Address;
using ASP_NET_CRM.DTO.AddressPrefix;
using ASP_NET_CRM.DTO.ApplicationError;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.DTO.Consent;
using ASP_NET_CRM.DTO.Interaction;
using ASP_NET_CRM.DTO.InteractionClass;
using ASP_NET_CRM.DTO.InteractionType;
using ASP_NET_CRM.DTO.Note;
using ASP_NET_CRM.DTO.Role;
using ASP_NET_CRM.DTO.User;
using ASP_NET_CRM.Exceptions;
using ASP_NET_CRM.Models;
using ASP_NET_CRM.Models.Entities;
using ASP_NET_CRM.Models.Utilities.Paging;
using ASP_NET_CRM.ViewModels;
using ASP_NET_CRM.ViewModels.Addresses;
using ASP_NET_CRM.ViewModels.AddressPrefixes;
using ASP_NET_CRM.ViewModels.ApplicationErrors;
using ASP_NET_CRM.ViewModels.Clients;
using ASP_NET_CRM.ViewModels.Consents;
using ASP_NET_CRM.ViewModels.InteractionClasses;
using ASP_NET_CRM.ViewModels.Interactions;
using ASP_NET_CRM.ViewModels.InteractionTypes;
using ASP_NET_CRM.ViewModels.Notes;
using ASP_NET_CRM.ViewModels.Roles;
using ASP_NET_CRM.ViewModels.Users;
using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.App_Start
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            var config = new MapperConfiguration(cfg =>
            {
                // Destination : Client

                CreateMap<ClientSaveDto, Client>();
                CreateMap<Client, ClientDto>();
                CreateMap<ClientUpdateDto, Client>().ForMember(c => c.ClientType, opt => opt.Ignore());
                CreateMap<ClientDto, IndividualClientDetailsViewModel>();
                CreateMap<ClientDto, BusinessClientDetailsViewModel>();
                CreateMap<ClientDto, IndividualClientEditViewModel>();
                CreateMap<ClientDto, BusinessClientEditViewModel>();
                CreateMap<AddressDto, IndividualClientDetailsViewModel>()
                    .ForMember(vm => vm.Id, opt => opt.Ignore())
                    .ForMember(vm => vm.MainAddressFullName, opt => opt.MapFrom(src => src.FullAddressName))
                    .ForMember(vm => vm.MainAddressPostalCode, opt => opt.MapFrom(src => src.PostalCode))
                    .ForMember(vm => vm.MainAddressCity, opt => opt.MapFrom(src => src.City))
                    .ForMember(vm => vm.MainAddressState, opt => opt.MapFrom(src => src.State))
                    .ForMember(vm => vm.Created, opt => opt.Ignore())
                    .ForMember(vm => vm.CreatedBy, opt => opt.Ignore())
                    .ForMember(vm => vm.LastUpdated, opt => opt.Ignore())
                    .ForMember(vm => vm.LastUpdatedBy, opt => opt.Ignore());
                CreateMap<AddressDto, BusinessClientDetailsViewModel>()
                    .ForMember(vm => vm.Id, opt => opt.Ignore())
                    .ForMember(vm => vm.MainAddressFullName, opt => opt.MapFrom(src => src.FullAddressName))
                    .ForMember(vm => vm.MainAddressPostalCode, opt => opt.MapFrom(src => src.PostalCode))
                    .ForMember(vm => vm.MainAddressCity, opt => opt.MapFrom(src => src.City))
                    .ForMember(vm => vm.MainAddressState, opt => opt.MapFrom(src => src.State))
                    .ForMember(vm => vm.Created, opt => opt.Ignore())
                    .ForMember(vm => vm.CreatedBy, opt => opt.Ignore())
                    .ForMember(vm => vm.LastUpdated, opt => opt.Ignore())
                    .ForMember(vm => vm.LastUpdatedBy, opt => opt.Ignore());

                // Destination : Consent

                CreateMap<Consent, ConsentDto>();
                CreateMap<ConsentSaveDto, Consent>();
                CreateMap<ConsentUpdateDto, Consent>();
                CreateMap<ConsentDto, ConsentClientUpdateDto>().ForMember(c => c.Selected, opt => opt.Ignore());
                CreateMap<ConsentClientUpdateDto, ConsentDto>();
                CreateMap<ClientDto, ConsentsClientDetailsViewModel>().ForMember(c => c.ClientConsents, opt => opt.MapFrom(src => src.Consents));
                CreateMap<ClientDto, ConsentsClientEditViewModel>().ForMember(c => c.Consents, opt => opt.Ignore());
                CreateMap<ConsentDto, ConsentDetailsViewModel>();
                CreateMap<ConsentDto, ConsentEditViewModel>();

                // Destination : Interaction

                CreateMap<Interaction, InteractionDto>();
                CreateMap<InteractionSaveDto, Interaction>();
                CreateMap<InteractionUpdateDto, Interaction>();
                CreateMap<ClientDto, InteractionsClientDetailsViewModel>();
                CreateMap<ClientDto, NewInteractionViewModel>()
                    .ForMember(vm => vm.ClientId, opt => opt.MapFrom(src => src.Id))
                    .ForMember(vm => vm.ClientName, opt => opt.MapFrom(src => src.Name));
                CreateMap<InteractionDto, InteractionDetailsViewModel>()
                    .ForMember(vm => vm.ClientId, opt => opt.MapFrom(src => src.Client.Id))
                    .ForMember(vm => vm.ClientName, opt => opt.MapFrom(src => src.Client.Name));
                CreateMap<InteractionDto, InteractionEditViewModel>().ForMember(vm => vm.ClientName, opt => opt.MapFrom(src => src.Client.Name));

                // Destination : InteractionType

                CreateMap<InteractionType, InteractionTypeDto>();
                CreateMap<InteractionTypeSaveDto, InteractionType>();
                CreateMap<InteractionTypeUpdateDto, InteractionType>();
                CreateMap<InteractionTypeDto, InteractionTypeDetailsViewModel>();
                CreateMap<InteractionTypeDto, InteractionTypeEditViewModel>();

                // Destination : InteractionClass

                CreateMap<InteractionClass, InteractionClassDto>();
                CreateMap<InteractionClassDto, InteractionClassDetailsViewModel>()
                    .ForMember(vm => vm.InteractionTypeName, opt => opt.MapFrom(it => it.InteractionType.Name));
                CreateMap<InteractionClassDto, InteractionClassEditViewModel>()
                    .ForMember(vm => vm.InteractionTypeName, opt => opt.MapFrom(it => it.InteractionType.Name));
                CreateMap<InteractionClassSaveDto, InteractionClass>();
                CreateMap<InteractionClassUpdateDto, InteractionClass>();

                // Destination : ApplicationError

                CreateMap<ApplicationError, ApplicationErrorDto>();
                CreateMap<ApplicationErrorSaveDto, ApplicationError>();
                CreateMap<ApplicationCustomException, ApplicationErrorSaveDto>();
                CreateMap<ApplicationErrorDto, ApplicationErrorDetailsViewModel>();

                // Destination : AddressPrefix

                CreateMap<AddressPrefix, AddressPrefixDto>();
                CreateMap<AddressPrefixSaveDto, AddressPrefix>();
                CreateMap<AddressPrefixUpdateDto, AddressPrefix>();
                CreateMap<AddressPrefixDto, AddressPrefixDetailsViewModel>();
                CreateMap<AddressPrefixDto, AddressPrefixEditViewModel>();

                // Destination : Address

                CreateMap<ClientDto, AddressesClientDetailsViewModel>();
                CreateMap<Address, AddressDto>();
                CreateMap<AddressSaveDto, Address>();
                CreateMap<AddressUpdateDto, Address>();
                CreateMap<AddressDto, AddressLightweightDto>();
                CreateMap<AddressDto, AddressDetailsViewModel>()
                    .ForMember(vm => vm.ClientId, opt => opt.MapFrom(src => src.Client.Id))
                    .ForMember(vm => vm.ClientName, opt => opt.MapFrom(src => src.Client.Name));
                CreateMap<ClientDto, NewAddressViewModel>()
                    .ForMember(vm => vm.ClientId, opt => opt.MapFrom(src => src.Id))
                    .ForMember(vm => vm.ClientName, opt => opt.MapFrom(src => src.Name));
                CreateMap<AddressDto, AddressEditViewModel>()
                    .ForMember(vm => vm.ClientName, opt => opt.MapFrom(src => src.Client.Name));

                // Destination : Note

                CreateMap<ClientDto, NotesClientDetailsViewModel>();
                CreateMap<Note, NoteDto>();
                CreateMap<NoteSaveDto, Note>();
                CreateMap<NoteUpdateDto, Note>();
                CreateMap<NoteDto, NoteDetailsViewModel>()
                    .ForMember(vm => vm.ClientId, opt => opt.MapFrom(src => src.Client.Id))
                    .ForMember(vm => vm.ClientName, opt => opt.MapFrom(src => src.Client.Name));
                CreateMap<ClientDto, NewNoteViewModel>()
                    .ForMember(vm => vm.ClientId, opt => opt.MapFrom(src => src.Id))
                    .ForMember(vm => vm.ClientName, opt => opt.MapFrom(src => src.Name));
                CreateMap<NoteDto, NoteEditViewModel>()
                    .ForMember(vm => vm.ClientName, opt => opt.MapFrom(src => src.Client.Name));

                // Destination : Role

                CreateMap<Role, RoleDto>();
                CreateMap<RoleDto, RoleDetailsViewModel>();

                // Destination : User

                CreateMap<ApplicationUser, UserDto>()
                    .ForMember(u => u.RolesId, opt => opt.MapFrom(au => au.Roles));
                CreateMap<UserSaveDto, RegisterViewModel>()
                    .ForMember(vm => vm.Password, opt => opt.Ignore())
                    .ForMember(vm => vm.ConfirmPassword, opt => opt.Ignore());
                CreateMap<UserDto, UserDetailsViewModel>();
                CreateMap<UserDto, UserEditViewModel>();
                CreateMap<UserUpdateDto, ApplicationUser>();

                // Destination : UserRole

                CreateMap<UserDto, UserRolesDetailsViewModel>()
                    .ForMember(vm => vm.UserRoles, opt => opt.MapFrom(u => u.RolesId));
                CreateMap<UserDto, UserRolesEditViewModel>();
                CreateMap<IdentityUserRole, string>()
                    .ConstructUsing(ur => ur.RoleId);
                CreateMap<RoleDto, UserRoleUpdateDto>().ReverseMap();

                // Destination : Pagination

                CreateMap<Pagination, PaginationExtended>()
                    .ForMember(pe => pe.CurrentPage, opt => opt.Ignore());
                CreateMap<Pagination, AddressPagination>()
                    .ForMember(ap => ap.CurrentPage, opt => opt.Ignore());
                CreateMap<Pagination, PaginationExtendedLightweight>()
                    .ForMember(pel => pel.CurrentPage, opt => opt.Ignore());
            });
        }
    }
}