﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.WebAPI.Messages
{
    public class GlobalWebAPIResponseMessages
    {
        public const string InternalServerErrorMessage = "Something gone wrong.";
    }
}