﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.Interaction
{
    public class InteractionAlertMessages
    {
        public const string InteractionAddSuccessMessage = "Interaction added correctly";
        public const string InteractionUpdateSuccessMessage = "Interaction updated correctly";
        public const string InteractionDeleteSuccessMessage = "Interaction deleted correctly";
    }
}