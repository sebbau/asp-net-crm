﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.Address
{
    public class AddressAlertMessages
    {
        public const string AddressAddSuccessMessage = "Address added correctly.";
        public const string AddressUpdateSuccessMessage = "Address updated correctly.";
        public const string AddressDeleteSuccessMessage = "Address deleted correctly.";
    }
}