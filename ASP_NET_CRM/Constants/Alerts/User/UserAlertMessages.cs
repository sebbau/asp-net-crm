﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.User
{
    public class UserAlertMessages
    {
        public const string UserAddSuccessMessage = "User added correctly.";
        public const string UserUpdateSuccessMessage = "User updated correctly.";
        public const string UserDeleteSuccessMessage = "User deleted correctly.";
        public const string UserResetPasswordSendLinkSuccessMessage = "Password change link sent correctly.";
    }
}