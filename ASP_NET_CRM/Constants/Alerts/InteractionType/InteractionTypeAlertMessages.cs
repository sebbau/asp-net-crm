﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.InteractionType
{
    public class InteractionTypeAlertMessages
    {
        public const string InteractionTypeAddSuccessMessage = "Interaction Type added correctly.";
        public const string InteractionTypeUpdateSuccessMessage = "Interaction Type updated correctly.";
        public const string InteractionTypeDeleteSuccessMessage = "Interaction Type deleted correctly.";
    }
}