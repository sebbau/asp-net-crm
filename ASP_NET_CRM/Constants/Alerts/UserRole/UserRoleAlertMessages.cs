﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.UserRole
{
    public class UserRoleAlertMessages
    {
        public const string UserRolesUpdateSuccessMessage = "User Roles updated correctly.";
    }
}