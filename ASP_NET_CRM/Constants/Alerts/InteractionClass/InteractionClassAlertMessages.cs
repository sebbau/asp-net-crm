﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.InteractionClass
{
    public class InteractionClassAlertMessages
    {
        public const string InteractionClassAddSuccessMessage = "Interaction Class added correctly.";
        public const string InteractionClassUpdateSuccessMessage = "Interaction Class updated correctly.";
        public const string InteractionClassDeleteSuccessMessage = "Interaction Class deleted correctly.";
    }
}