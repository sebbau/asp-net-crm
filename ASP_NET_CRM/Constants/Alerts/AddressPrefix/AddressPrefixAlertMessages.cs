﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.AddressPrefix
{
    public class AddressPrefixAlertMessages
    {
        public const string AddressPrefixAddSuccessMessage = "Address Prefix added correctly.";
        public const string AddressPrefixUpdateSuccessMessage = "Address Prefix updated correctly.";
        public const string AddressPrefixDeleteSuccessMessage = "Address Prefix deleted correctly.";
    }
}