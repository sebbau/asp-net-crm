﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.Consent
{
    public static class ConsentAlertMessages
    {
        public const string ConsentAddSuccessMessage = "Consent added correctly.";
        public const string ConsentUpdateSuccessMessage = "Consent updated correctly.";
        public const string ConsentDeleteSuccessMessage = "Consent deleted correctly.";
        public const string consentsUpdateSuccessMessage = "Consents updated correctly.";
    }
}