﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.Client
{
    public class ClientAlertMessages
    {
        public const string clientAddSuccessMessage = "Client added correctly.";
        public const string clientUpdateSuccessMessage = "Client updated correctly.";
        public const string clientDeleteSuccessMessage = "Client deleted correctly.";
    }
}