﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Alerts.Note
{
    public class NoteAlertMessages
    {
        public const string NoteAddSuccessMessage = "Note added correctly.";
        public const string NoteUpdateSuccessMessage = "Note updated correctly.";
        public const string NoteDeleteSuccessMessage = "Note deleted correctly.";
    }
}