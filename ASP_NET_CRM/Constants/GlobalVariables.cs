﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants
{
    public static class GlobalVariables
    {
        public const int maxRecordsPerPage = 20;
        public const int defaultRecordsPerPage = 10;
        public const int pagesRangeToDisplay = 5;
    }
}