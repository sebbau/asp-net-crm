﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Roles
{
    public class RoleConstants
    {
        public const string AdministratorRole = "Administrator";
        public const string EmployeeRole = "Employee";
        public const string ReadonlyRole = "Readonly";
    }
}