﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Utilities
{
    public class UtilitiesNameConstants
    {
        public const string InteractionControllerHelper = "InteractionControllerHelper";
        public const string AddressControllerHelper = "AddressControllerHelper";
        public const string NoteControllerHelper = "NoteControllerHelper";
        public const string UserControllerHelper = "UserControllerHelper";
        public const string ConsentControllerHelper = "ConsentControllerHelper";
    }
}