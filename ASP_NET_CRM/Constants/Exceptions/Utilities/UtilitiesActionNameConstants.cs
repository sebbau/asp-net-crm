﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Utilities
{
    public class UtilitiesActionNameConstants
    {
        // InteractionControllerHelper

        public const string InteractionControllerHelperGetViewModelWhenModelStateIsInvalidForUpdateMethod = "GetViewModelWhenModelStateIsInvalidForUpdateMethod";

        // AddressControllerHelper

        public const string AddressControllerHelperGetAddressEditViewModelWhenModelStateIsInvalidForUpdateMethod = "GetAddressEditViewModelWhenModelStateIsInvalidForUpdateMethod";

        // NoteControllerHelper

        public const string NoteControllerHelperGetViewModelWhenModelStateIsInvalidForUpdateMethod = "GetViewModelWhenModelStateIsInvalidForUpdateMethod";

        // UserControllerHelper

        public const string UserControllerHelperGetViewModelWhenModelStateIsInvalidForUpdateMethod = "GetViewModelWhenModelStateIsInvalidForUpdateMethod";

        // ConsentControllerHelper

        public const string ConsentControllerHelperGetViewModelWhenModelStateIsInvalidForUpdateMethod = "GetViewModelWhenModelStateIsInvalidForUpdateMethod";
    }
}