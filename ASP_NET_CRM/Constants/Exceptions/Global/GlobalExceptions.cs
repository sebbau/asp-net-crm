﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions
{
    public static class GlobalExceptions
    {
        public const string InternalServerErrorMessage = "Something gone wrong.";
    }
}