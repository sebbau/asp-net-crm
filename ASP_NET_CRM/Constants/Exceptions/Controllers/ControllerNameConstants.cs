﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Controllers
{
    public class ControllerNameConstants
    {
        public const string ClientController = "ClientController";
        public const string InteractionController = "InteractionController";
        public const string AddressController = "AddressController";
        public const string NoteController = "NoteController";
        public const string ConsentController = "ConsentController";
        public const string UserRoleController = "UserRoleController";
    }
}