﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Controllers
{
    public class ControllerActionNameConstants
    {
        // AddressController

        public const string AddressControllerDetailsView = "DetailsView";
        public const string AddressControllerEditView = "EditView";

        // NoteController

        public const string NoteControllerDetailsView = "DetailsView";
        public const string NoteControllerEditView = "EditView";

        // ConsentController

        public const string ConsentControllerUpdateConsentsForClient = "UpdateConsentsForClient";

        // UserRoleController

        public const string UserRoleControllerUpdateUserRoles = "UpdateUserRoles";

        // ClientController

        public const string ClientControllerDetailsView = "DetailsView";
        public const string ClientControllerEditView = "EditView";
        public const string ClientControllerUpdate = "Update";

        // InteractionController

        public const string InteractionControllerDetailsView = "DetailsView";
        public const string InteractionControllerEditView = "EditView";
    }
}