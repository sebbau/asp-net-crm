﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.User
{
    public class UserExceptionMessages
    {
        public const string UserNotFoundByIdExceptionMessage = "User with the given 'Id' not exists.";
        public const string UserRolesInvalidExceptionMessage = "Given user roles are invalid.";
    }
}