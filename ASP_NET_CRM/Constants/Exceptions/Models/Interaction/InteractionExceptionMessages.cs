﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.Interaction
{
    public class InteractionExceptionMessages
    {
        // Service Validations Messages

        public const string InteractionNotFoundByIdExceptionMessage = "Interaction with the given 'Id' not exists.";
        public const string InteractionStatusEditExceptionMessage = "Interaction has status closed or canceled and cannot be edited";
    }
}