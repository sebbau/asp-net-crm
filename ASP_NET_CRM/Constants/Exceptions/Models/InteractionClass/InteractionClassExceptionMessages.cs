﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.InteractionClass
{
    public class InteractionClassExceptionMessages
    {
        // Model State Keys

        public const string InteractionClassKey = "InteractionClass";

        // Service Validations Messages

        public const string InteractionClassNotFoundByIdExceptionMessage = "Interaction Class with the given 'Id' not exists.";
        public const string InteractionClassNameUniqueExceptionMessage = "Interaction Class with the given 'Name' exists for given 'Type'.";
    }
}