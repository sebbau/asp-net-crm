﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.Consent
{
    public class ConsentExceptionMessages
    {
        // Model State Keys

        public const string ConsentKey = "Consent";

        // Service Validations Messages

        public const string ConsentInvalidExceptionMessage = "Given consents are invalid.";
        public const string ConsentNameUniqueExceptionMessage = "Consent with the given 'Name' exists.";
        public const string ConsentNotFoundByIdExceptionMessage = "Consent with the given 'Id' not exists.";
    }
}