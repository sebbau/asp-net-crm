﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.Note
{
    public class NoteExceptionMessages
    {
        public const string NoteNotFoundByIdExceptionMessage = "Note with the given 'Id' not exists.";
    }
}