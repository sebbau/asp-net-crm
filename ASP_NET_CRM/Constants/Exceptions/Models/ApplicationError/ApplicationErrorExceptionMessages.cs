﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.ApplicationError
{
    public class ApplicationErrorExceptionMessages
    {
        // Service Exception Messages

        public const string ApplicationErrorNotFoundByIdExceptionMessage = "Application Error with the given 'Id' not exists.";
    }
}