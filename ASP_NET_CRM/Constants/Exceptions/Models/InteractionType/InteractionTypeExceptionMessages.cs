﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.InteractionType
{
    public class InteractionTypeExceptionMessages
    {
        // Model State Keys

        public const string InteractionTypeKey = "InteractionType";

        // Service Validations Messages

        public const string InteractionTypeNotFoundByIdExceptionMessage = "Interaction Type with the given 'Id' not exists.";
        public const string InteractionTypeNameUniqueExceptionMessage = "Interaction Type with the given 'Name' exists.";
    }
}