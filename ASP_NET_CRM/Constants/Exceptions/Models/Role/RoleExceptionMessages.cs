﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.Role
{
    public class RoleExceptionMessages
    {
        public const string RoleNotFoundByIdExceptionMessage = "Role with the given 'Id' not exists.";
    }
}