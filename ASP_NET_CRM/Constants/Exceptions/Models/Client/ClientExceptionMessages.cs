﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.Client
{
    public class ClientExceptionMessages
    {
        // Model State Keys

        public const string PeselKey = "Pesel";
        public const string NipKey = "NIP";
        public const string ClientKey = "Client";

        // Service Exception Messages

        public const string PeselUniqueExceptionMessage = "Client with the given 'Pesel' exists.";
        public const string NipUniqueExceptionMessage = "Client with the given 'NIP' exists.";
        public const string ClientNotFoundByIdExceptionMessage = "Client with the given 'Id' not exists.";
        public const string ClientTypeNotFoundExceptionMessage = "Client Type not exists.";
    }
}