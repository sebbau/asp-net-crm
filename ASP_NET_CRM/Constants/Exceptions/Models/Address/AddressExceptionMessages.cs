﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.Address
{
    public class AddressExceptionMessages
    {
        // Model State Keys

        public const string AddressKey = "Address";

        // Service Exception Messages

        public const string AddressNotFoundByIdExceptionMessage = "Address with the given 'Id' not exists.";
        public const string AddressMainForClientExistsExceptionMessage = "Client already has main address.";
    }
}