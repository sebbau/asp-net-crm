﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions.Models.AddressPrefix
{
    public class AddressPrefixExceptionMessages
    {
        // Model State Keys

        public const string AddressPrefixKey = "AddressPrefix";

        // Service Validations Messages

        public const string AddressPrefixNotFoundByIdExceptionMessages = "Address Prefix with the given 'Id' not exists.";
        public const string AddressPrefixNameUniqueExceptionMessage = "Address Prefix with the given 'Name' exists.";
    }
}