﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions
{
    public class ServiceNameConstants
    {
        public const string ClientService = "ClientService";
        public const string InteractionService = "InteractionService";
        public const string ConsentService = "ConsentService";
        public const string AddressService = "AddressService";
        public const string NoteService = "NoteService";
        public const string ApplicationErrorService = "ApplicationErrorService";
        public const string RoleService = "RoleService";
        public const string UserService = "UserService";
        public const string InteractionClassService = "InteractionClassService";
        public const string AddressPrefixService = "AddressPrefixService";
        public const string InteractionTypeService = "InteractionTypeService";
    }
}