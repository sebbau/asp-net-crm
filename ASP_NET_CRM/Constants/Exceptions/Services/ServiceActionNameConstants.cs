﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Exceptions
{
    public class ServiceActionNameConstants
    {
        // ClientService

        public const string ClientServiceUpdateClientAsync = "UpdateClientAsync";
        public const string ClientServiceGetClientAsync = "GetClientAsync";
        public const string ClientServiceGetClientWithIncludesAsync = "GetClientWithIncludesAsync";
        public const string ClientServiceAddClientAsync = "AddClientAsync";
        public const string ClientServiceDeleteClientAsync = "DeleteClientAsync";

        // InteractionService

        public const string InteractionServiceGetInteractionWithIncludesAsync = "GetInteractionWithIncludesAsync";
        public const string InteractionServiceAddInteractionAsync = "AddInteractionAsync";
        public const string InteractionServiceUpdateInteractionAsync = "UpdateInteractionAsync";
        public const string InteractionServiceDeleteInteractionAsync = "DeleteInteractionAsync";

        // ConsentService

        public const string ConsentServiceUpdateConsentsForClient = "UpdateConsentsForClient";
        public const string ConsentServiceAddConsentAsync = "AddConsentAsync";
        public const string ConsentServiceGetConsentAsync = "GetConsentAsync";
        public const string ConsentServiceUpdateConsentAsync = "UpdateConsentAsync";
        public const string ConsentServiceDeleteConsentAsync = "DeleteConsentAsync";

        // AddressService

        public const string AddressServiceGetAddressWithIncludesAsync = "GetAddressWithIncludesAsync";
        public const string AddressServiceAddAddressAsync = "AddAddressAsync";
        public const string AddressServiceUpdateAddressAsync = "UpdateAddressAsync";
        public const string AddressServiceDeleteAddressAsync = "DeleteAddressAsync";

        // NoteService

        public const string NoteServiceGetNoteWithIncludesAsync = "GetNoteWithIncludesAsync";
        public const string NoteServiceAddNoteAsync = "AddNoteAsync";
        public const string NoteServiceUpdateNoteAsync = "UpdateNoteAsync";
        public const string NoteServiceDeleteNoteAsync = "DeleteNoteAsync";

        // ApplicationErrorService

        public const string ApplicationErrorServiceGetApplicationErrorAsync = "GetApplicationErrorAsync";

        // RoleService

        public const string RoleServiceGetRoleAsync = "GetRoleAsync";

        // UserService

        public const string UserServiceGetUserAsync = "GetUserAsync";
        public const string UserServiceGetUserWithIncludesAsync = "GetUserWithIncludesAsync";
        public const string UserServiceUpdateUserAsync = "UpdateUserAsync";
        public const string UserServiceDeleteUserAsync = "DeleteUserAsync";
        public const string UserServiceUpdateUserRolesAsync = "UpdateUserRolesAsync";

        // InteractionClassService

        public const string InteractionClassServiceGetInteractionClassWithIncludesAsync = "GetInteractionClassWithIncludesAsync";
        public const string InteractionClassAddInteractionClassAsync = "AddInteractionClassAsync";
        public const string InteractionClassUpdateInteractionClassAsync = "UpdateInteractionClassAsync";
        public const string InteractionClassDeleteInteractionClassAsync = "DeleteInteractionClassAsync";

        // AddressPrefixService

        public const string AddressPrefixServiceGetAddressPrefixAsync = "GetAddressPrefixAsync";
        public const string AddressPrefixServiceAddAddressPrefixAsync = "AddAddressPrefixAsync";
        public const string AddressPrefixServiceUpdateAddressPrefixAsync = "UpdateAddressPrefixAsync";
        public const string AddressPrefixServiceDeleteAddressPrefixAsync = "DeleteAddressPrefixAsync";

        // InteractionTypeService

        public const string InteractionTypeServiceGetInteractionTypeAsync = "GetInteractionTypeAsync";
        public const string InteractionTypeServiceAddInteractionTypeAsync = "AddInteractionTypeAsync";
        public const string InteractionTypeServiceUpdateInteractionTypeAsync = "UpdateInteractionTypeAsync";
        public const string InteractionTypeServiceDeleteInteractionTypeAsync = "DeleteInteractionTypeAsync";
    }
}