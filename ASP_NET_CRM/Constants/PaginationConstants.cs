﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants
{
    public static class PaginationConstants
    {
        public const int tenRecords = 10;
        public const int fifteenRecords = 15;
        public const int twentyRecords = 20;
    }
}