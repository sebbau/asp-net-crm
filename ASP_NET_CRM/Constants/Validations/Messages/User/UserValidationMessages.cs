﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Validations.Messages.User
{
    public class UserValidationMessages
    {
        // Fluent Validation Messages

        public const string FirstNameRegExValidationMessage = "'First Name' must contain only letters. The first letter must be uppercase.";
        public const string SurnameRegExValidationMessage = "'Surname' must contain only letters. The first letter must be uppercase.";
        public const string ConfirmPasswordEqualToPasswordValidationMessage = "'Confirm Password' must be equal to 'Password'.";
        public const string PasswordRegExValidationMessage = "'Password' must contain one uppercase, one lowercase, one digit and one special character.";
    }
}