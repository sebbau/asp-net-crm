﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Validations.Messages.Interaction
{
    public static class InteractionValidationMessages
    {
        // Fluent Validation Messages

        public const string interactionClassIdNotEmptyValidationMessage = "'Class' must not be empty.";
        public const string interactionTypeIdNotEmptyValidationMessage = "'Type' must not be empty.";
        public const string clientIdNotEmptyValidationMessage = "'Client' must not be empty.";
    }
}