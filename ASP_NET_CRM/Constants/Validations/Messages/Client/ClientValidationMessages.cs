﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Validations.Messages.Client
{
    public class ClientValidationMessages
    {
        // Fluent Validation Messages

        public const string FirstNameRegExValidationMessage = "'First Name' must contain only letters. The first letter must be uppercase.";
        public const string SurnameRegExValidationMessage = "'Surname' must contain only letters. The first letter must be uppercase.";
        public const string PeselRegExValidationMessage = "'Pesel' must be exactly 11 digits.";
        public const string NIPRegExValidationMessage = "'NIP' must be exactly 10 digits.";
        public const string IndividualClientNameValidationMessage = "'Name' must contains 'First Name' and 'Surname'";
        public const string DocumentTypeNotEmptyValidationMessage = "'Document Type' cannot be empty if 'Document Number', 'Document Issue Date' or 'Document Issued By' has value.";
        public const string DocumentNumberNotEmptyValidationMessage = "'Document Number' cannot be empty if 'Document Type', 'Document Issue Date' or 'Document Issued By' has value.";
        public const string DocumentIssueDateNotEmptyValidationMessage = "'Document Issue Date' cannot be empty if 'Document Type', 'Document Number' or 'Document Issued By' has value.";
        public const string DocumentIssuedByNotEmptyValidationMessage = "'Document Issued By' cannot be empty if 'Document Type', 'Document Number' or 'Document Issue Date' has value.";
        public const string RegonRegExValidationMessage = "'Regon' must be between 9 to 14 digits.";
        public const string KRSRegExValidationMessage = "'KRS' must be exactly 10 digits.";
        public const string PhoneNumberRegExValidationMessage = "'Phone Number' can contain 1 optional '+' character and 7 to 11 digits.";
    }
}