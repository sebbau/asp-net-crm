﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Validations.Messages.Note
{
    public class NoteValidationMessages
    {
        // Fluent Validation Messages

        public const string ClientIdNotEmptyValidationMessage = "'Client' must not be empty.";
    }
}