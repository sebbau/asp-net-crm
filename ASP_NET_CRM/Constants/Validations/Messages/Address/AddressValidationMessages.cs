﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Validations.Messages.Address
{
    public class AddressValidationMessages
    {
        // Fluent Validation Messages

        public const string TypeRequiredValidationMessage = "'Type' is required";
        public const string PrefixNotEmptyWhenStreetNameNotEmptyValidationMessage = "'Prefix' must be not empty when 'Street Name' is not empty";
        public const string PrefixEmptyWhenStreetNameEmptyValidationMessage = "'Prefix' must be empty when 'Street Name' is empty";
        public const string StreetNameRegExValidationMessage = "'Street name' first letter must be uppercase. 'Street name' must contains only digits and letters";
        public const string HouseNumberRegExValidationMessage = "'House Number' must start with digit and contains only digits and letters";
        public const string ApartmentNumberRegExValidationMessage = "'Apartment Number' must start with digit and contains only digits and letters or contains only 'Dz. ' and digits and letters";
        public const string PostalCodeRegExValidationMessage = "'Postal Code' must be in format xx-xxx and contains only digits";
        public const string CityRegExValidationMessage = "'City' first letter must be uppercase. 'City' must contains only letters";
        public const string FullAddressNameInvalidValidationMessage = "'Full Address Name' is invalid";
        public const string StateRequiredValidationMessage = "'State' is required";
        public const string ClientIdNotEmptyValidationMessage = "'Client' must not be empty.";
    }
}