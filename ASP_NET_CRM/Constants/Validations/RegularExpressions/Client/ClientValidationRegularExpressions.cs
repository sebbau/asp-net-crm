﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Validations.RegularExpressions.Client
{
    public class ClientValidationRegularExpressions
    {
        public const string firstNameRegEx = "^[A-Z]{1}[a-zżźćńółęąś]{2,49}$";
        public const string surnameRegEx = "^[A-ZŻŹĆĄŚĘŁÓŃ]{1}[a-zżźćńółęąś]{1,74}(-[A-ZŻŹĆĄŚĘŁÓŃ]{1}[a-zżźćńółęąś]{1,74})*$";
        public const string peselRegEx = "^\\d{11}$";
        public const string nipRegEx = "^\\d{10}$";
        public const string RegonRegEx = "^\\d{9,14}$";
        public const string KRSRegEx = "^\\d{10}$";
        public const string PhoneNumberRegEx = "^\\+?[\\d]{7,11}$";
    }
}