﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Validations.RegularExpressions.Address
{
    public class AddressValidationRegularExpressions
    {
        public const string StreetNameRegEx = "^[A-Z]{1}[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9 ]+$";
        public const string HouseNumberRegEx = "^[\\d]+[A-Za-z0-9]*$";
        public const string ApartmentNumberRegEx = "^(Dz.[ ])?([\\d]+[A-Za-z0-9]*)?$";
        public const string PostalCodeRegEx = "^[\\d]{2}-[\\d]{3}$";
        public const string CityRegEx = "^[A-ZŻŹĆŚŁ]{1}[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ ]+$";
    }
}