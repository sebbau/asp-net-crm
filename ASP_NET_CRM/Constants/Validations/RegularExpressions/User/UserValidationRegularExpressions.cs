﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.Validations.RegularExpressions.User
{
    public class UserValidationRegularExpressions
    {
        public const string FirstNameRegEx = "^[A-Z]{1}[a-z]{2,49}$";
        public const string SurnameRegEx = "^[A-Z]{1}[a-z]{1,74}(-[A-Z]{1}[a-z]{1,74})*$";
        public const string PasswordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[!@#$%^&*()_\\-+={[}\\]:;<,>.?/])[A-Za-z\\d!@#$%^&*()_\\-+={[}\\]:;<,>.?/]{6,100}$";
    }
}