﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.SearchConstants
{
    public class InteractionConstants
    {
        public const string Id = "Id";
        public const string ClientName = "Client Name";
        public const string Type  = "Type";
        public const string Class = "Class";
    }
}