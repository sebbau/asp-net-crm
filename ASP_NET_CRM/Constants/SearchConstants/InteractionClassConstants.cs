﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.SearchConstants
{
    public class InteractionClassConstants
    {
        public const string Id = "Id";
        public const string Type = "Type";
        public const string Name = "Name";
    }
}