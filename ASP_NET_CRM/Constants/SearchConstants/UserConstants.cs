﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.SearchConstants
{
    public class UserConstants
    {
        public const string Email = "Email";
        public const string FirstName = "First Name";
        public const string Surname = "Surname";
        public const string FullName = "Full Name";
    }
}