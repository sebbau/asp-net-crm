﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Constants.SearchConstants
{
    public static class ClientConstants
    {
        public const string id = "Id";
        public const string name = "Name";
        public const string pesel = "Pesel";
        public const string nip = "Nip";
    }
}