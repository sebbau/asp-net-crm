﻿var containerDiv = "#container";

$(document).ready(function () {
    $(containerDiv + " input").attr("readonly", "readonly");
    $(containerDiv + " textarea").attr("readonly", "readonly");
    clearAlertsDiv();
})