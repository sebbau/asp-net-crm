﻿$(document).on("mousemove", ".customInputTooltip", function () {
    if ($(this)[0].scrollWidth > $(this)[0].clientWidth) {
        if (!$(this).data('tooltip')) {
            $(this).tooltip({ 'data-placement': 'top', 'title': $(this).val() }).triggerHandler("mouseover");
        }
    }
});

