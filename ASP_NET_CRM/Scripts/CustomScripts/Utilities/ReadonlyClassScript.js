﻿var readonlyClass = ".readonly";

$(document).ready(function () {
    $(readonlyClass).attr("readonly", "readonly");
});