﻿var loginFormId = "#loginForm";
var emailInput = "#Email";
var passwordInput = "#Password";
var validationSummaryDiv = "#validationSummary";
var loginButton = "#loginButton";

$(loginFormId).submit(function (e) {
    e.preventDefault();
    $(validationSummaryDiv).empty();
    $(validationSummaryDiv).attr("hidden", true);
    $(loginButton).attr('disabled', true);
    $("body").css("cursor", "progress");

    $.ajax({
        url: "/Api/Authentication/Token",
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        data: 'username=' + $(emailInput).val() + '&password=' + $(passwordInput).val() + '&grant_type=password',
        success: function (data) {
            localStorage.setItem("ASPNETCRMAccessToken", data.access_token);
            e.target.submit();          
        },
        error: function () {
            $(validationSummaryDiv).removeAttr("hidden");
            $(validationSummaryDiv).append("<ul><li>Invalid login attempt.</li></ul>");
            $(loginButton).attr('disabled', false);
            $("body").css("cursor", "default");
        }

    })
});