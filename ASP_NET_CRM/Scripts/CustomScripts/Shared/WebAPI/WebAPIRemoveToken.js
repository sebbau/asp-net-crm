﻿function Logout() {
    var logoutFormId = "#logoutForm";

    localStorage.removeItem("ASPNETCRMAccessToken");

    $(logoutFormId).submit();
}