﻿$(searchStringInput).keyup(function (event) {
    if ($(searchStringInput).length > 0)
        $(searchClientsByPropertyButton).prop('disabled', false);

    if ((event.keyCode == 8 || event.keyCode == 46) && $(searchStringInput).val().length == 0)
        $(searchClientsByPropertyButton).prop('disabled', true);
});