﻿// View variables
var clientIdInput = "#ClientId";
var clientNameInput = "#ClientName";

// Choose Client button
var chooseClientButtonClass = ".chooseClientButton";

// Spinner
var spinnerDiv = "#spinnerDiv";
var spinnerIcon = "<i class='fas fa-circle-notch fa-spin' style='font-size: 40px'></i>";

// Modal main elemenrs
var searchClientModal = "#searchClientModal";
var searchClientModalBodyDiv = "#searchClientModalBody";
var searchClientModalTableBody = "#tableBody";

// Modal Client Id and Name
var modalClientNameIdName = "#ModalClientName_";
var modalClientId = 0;
var modalClientName;
var modalClientNameDiv;

// Search elements
var searchBySelect = "#searchBy";
var searchStringInput = "#searchString";
var searchAllClientsButton = "#searchAllClientsButton";
var searchClientsByPropertyButton = "#searchClientsByProperty";
var errorDiv = "#errorDiv";
var errorBox = "#errorBox";

// Query elements
var isSendNextRequests = true;
var searchBy = null;
var searchString = null;
var currentPage = 1;
var query = "?CurrentPage=" + currentPage;

var pagination = {
    "CurrentPage": currentPage,
    "RecordsPerPage": 10,
    "SearchString": searchString,
    "SearchBy": searchBy
}