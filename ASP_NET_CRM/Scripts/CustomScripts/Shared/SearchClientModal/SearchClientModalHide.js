﻿$(searchClientModal).on("hide.bs.modal", function () {
    isSendNextRequests = false;
    $(searchClientModalTableBody).empty("");
    pagination.CurrentPage = 1;
    pagination.SearchBy = null;
    pagination.SearchString = null;
    $(searchAllClientsButton).prop('disabled', true);
    $(searchClientsByPropertyButton).prop('disabled', true);
    $(searchStringInput).val("");
    $(spinnerDiv).empty();
    $(errorDiv).hide();
    $(errorBox).empty();
});