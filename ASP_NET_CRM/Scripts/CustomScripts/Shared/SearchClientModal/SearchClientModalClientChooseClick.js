﻿$(document).ready(function () {
    $(document).on("click", ".chooseClientButton", function () {
        modalClientId = $(this)[0].id;
        $(clientIdInput).val(modalClientId);

        modalClientName = $(modalClientNameIdName + modalClientId)[0].defaultValue;
        $(clientNameInput).val(modalClientName);

        $(searchClientModal).modal('hide');
    });
});