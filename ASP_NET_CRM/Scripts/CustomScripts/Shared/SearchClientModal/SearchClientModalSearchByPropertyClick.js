﻿$(searchClientsByPropertyButton).on("click", function () {
    $(searchAllClientsButton).prop('disabled', false);
    $(searchClientModalTableBody).empty("");

    isSendNextRequests = false;

    $(spinnerDiv).append(spinnerIcon);

    searchBy = $(searchBySelect).val();
    searchString = $(searchStringInput).val();
    pagination.CurrentPage = 1;
    pagination.SearchBy = searchBy;
    pagination.SearchString = searchString;

    $.ajax({
        url: "/Api/Client",
        method: "GET",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("ASPNETCRMAccessToken")
        },
        data: pagination,
        success: function (response) {
            $(errorDiv).hide();
            $(errorBox).empty();
            $(spinnerDiv).empty();

            if (response.Data.length == 10) {
                isSendNextRequests = true;

                pagination.CurrentPage = pagination.CurrentPage + 1;
            }

            $.each(response.Data, function () {
                $(searchClientModalTableBody).append(
                    "<tr>" +
                    "<td class='align-middle'><input type='text' id='ModalClientId_" + this.Id + "' class='customInput' value='" + this.Id + "' readonly='readonly'></td>" +
                    "<td class='align-middle'><input type='text' id='ModalClientName_" + this.Id + "' class='customInput customInputTooltip' value='" + this.Name + "' readonly='readonly'></td>" +
                    "<td class='align-middle'>" + this.ClientType + "</td>" +
                    "<td class='align-middle'>" + DisplayValueOrEmptyString(this.Pesel) + "</td>" +
                    "<td class='align-middle'>" + DisplayValueOrEmptyString(this.NIP) + "</td>" +
                    "<td class='text-center'>" +
                    "<button type='button' class='btn btn-success chooseClientButton' id='" + this.Id + "'><i class='fas fa-check'></i> Choose Client</button>" +
                    "</td>" +
                    "</tr>"
                );
            });
        },
        error: function (data) {
            isSendNextRequests = false;
            $(spinnerDiv).empty();
            $(errorDiv).show();
            $(errorBox).text(errorData.responseJSON);
        }
    });
});