﻿var warningAlertAsyncDiv = "#warningAlertAsync";
var warningMessageDiv = "#warningMessageDiv";

function warningAlertAsyncPopup() {
    $(warningAlertAsyncDiv).animate({ "opacity": "show", right: "20px" }, 500);
    $(warningAlertAsyncDiv).delay(4000).fadeOut(1000);
};

function addTextToWarningMessageDiv(textValue) {
    $(warningMessageDiv).html(textValue);
}

function removeWarningAlertAsyncDiv() {
    $(warningAlertAsyncDiv).delay(6000).queue(function () {
        $(this).remove();
    });
}