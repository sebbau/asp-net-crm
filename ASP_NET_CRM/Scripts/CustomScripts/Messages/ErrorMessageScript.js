﻿var errorAlertDiv = "#errorAlert";
var errorMessageDiv = "#errorMessageDiv";

$(document).ready(function () {
        $(errorAlertDiv).animate({ "opacity": "show", right: "20px" }, 500);
        $(errorAlertDiv).delay(4000).fadeOut(1000);
});

function errorAlertAsyncPopup() {
    $(errorAlertDiv).animate({ "opacity": "show", right: "20px" }, 500);
    $(errorAlertDiv).delay(4000).fadeOut(1000);
}

function addTextToErrorMessageDiv(textValue) {
    $(errorMessageDiv).html(textValue);
}