﻿var successAlertDiv = "#successAlert";
var successMessageDiv = "#successMessageDiv";

$(document).ready(function () {
        $(successAlertDiv).animate({ "opacity": "show", right: "20px" }, 500);
        $(successAlertDiv).delay(4000).fadeOut(1000);
});

function successAlertAsyncPopup() {
    $(successAlertDiv).animate({ "opacity": "show", right: "20px" }, 500);
    $(successAlertDiv).delay(4000).fadeOut(1000);
}

function addTextToSuccessMessageDiv(textValue) {
    $(successMessageDiv).html(textValue);
}

