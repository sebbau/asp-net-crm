﻿var alertsDiv = "#alerts";

function clearAlertsDiv() {
    $(alertsDiv).delay(6000).queue(function () {
        $(alertsDiv).empty();
    });
}