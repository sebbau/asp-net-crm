﻿var addressTypeSelect = "#Type";
var addressStreetPrefixIdSelect = "#PrefixId";
var addressStateSelect = "#State";
var addressFullAddressNameInput = "#FullAddressName";
var addressForm = "#newAddressForm";
var addressStreetNameInput = "#StreetName";
var addressHouseNumberInput = "#HouseNumber";
var addressApartmentNumberInput = "#ApartmentNumber";
var addressCityInput = "#City";
var addressPrefixValueHiddenInput = "#PrefixValue";
var selectedStreetPrefix;

$(addressTypeSelect).on("change", function () {
    $(addressTypeSelect + " option:contains('Select Type')").remove();
})

$(addressStateSelect).on("change", function () {
    $(addressStateSelect + " option:contains('Select State')").remove();
})

$(addressForm).on("submit", function () {
    selectedStreetPrefix = $(addressStreetPrefixIdSelect + " option:selected").text();
    $(addressPrefixValueHiddenInput).val(selectedStreetPrefix);

    if ($(addressStreetNameInput).val() && $(addressApartmentNumberInput).val()) {

        $(addressFullAddressNameInput).val(
            selectedStreetPrefix +
            " " +
            $(addressStreetNameInput).val() +
            " " +
            $(addressHouseNumberInput).val() +
            "/" +
            $(addressApartmentNumberInput).val() +
            ", " +
            $(addressCityInput).val()
        )
    }
    else if (!$(addressStreetNameInput).val() && $(addressApartmentNumberInput).val()) {

        $(addressFullAddressNameInput).val(
            $(addressCityInput).val() +
            " " +
            $(addressHouseNumberInput).val() +
            "/" +
            $(addressApartmentNumberInput).val()
        )
    }
    else if ($(addressStreetNameInput).val() && !$(addressApartmentNumberInput).val()) {

        $(addressFullAddressNameInput).val(
            selectedStreetPrefix +
            " " +
            $(addressStreetNameInput).val() +
            " " +
            $(addressHouseNumberInput).val() +
            ", " +
            $(addressCityInput).val()
        )
    }
    else {
        $(addressFullAddressNameInput).val(
            $(addressCityInput).val() +
            " " +
            $(addressHouseNumberInput).val()         
        )
    }
})