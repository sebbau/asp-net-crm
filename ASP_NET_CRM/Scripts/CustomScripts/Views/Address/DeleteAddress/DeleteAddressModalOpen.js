﻿var deleteAddressIdInput = "#deleteAddressId";
var deleteAddressModalClass = ".deleteModal";

$(deleteAddressModalClass).on("click", function () {
    var addressId = $(this).data("id");
    $(deleteAddressIdInput).val(addressId);
});