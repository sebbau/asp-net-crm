﻿var deleteAddressAsyncForm = "#deleteAddressAsync";
var deleteModalDiv = "#deleteModal";
var tableRows = "#addressTable tr";
var tableMessagesDiv = "#tableMessages";
var alertsDiv = "#alerts";

$(deleteAddressAsyncForm).submit(function (e) {
    e.preventDefault();

    var deleteAddressId = $(deleteAddressIdInput).val();

    $.ajax({
        url: "/Api/Address/" + deleteAddressId,
        method: "DELETE",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("ASPNETCRMAccessToken")
        },
        success: function (data) {
            $("#address_" + deleteAddressId).remove();

            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalSuccessMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                addTextToSuccessMessageDiv("Address correctly deleted.");
                successAlertAsyncPopup();
                clearAlertsDiv();
            });

            if ($(tableRows).length == 1) {
                $(tableMessagesDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalNoDataToDisplayPartial" }, function (data) {
                    $(tableMessagesDiv).html(data);
                });
            }
        },
        error: function (errorData) {
            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalErrorMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                var errorMessage = errorData.responseJSON.Message ? "Something gone wrong" : errorData.responseJSON;
                addTextToErrorMessageDiv(errorMessage);
                errorAlertAsyncPopup();
                clearAlertsDiv();
            });
        }
    })

    $(deleteModalDiv).modal('hide');
});