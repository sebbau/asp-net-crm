﻿var interactionTypeIdSelect = "#TypeId";
var interactionClassIdSelect = "#ClassId";

$(document).ready(function () {
    $(".datepicker").datepicker({
        dateFormat: "dd/mm/yy"
    });
});

$(interactionTypeIdSelect).on("change", function (event) {
    $("#TypeId option:contains('Select Type')").remove();

    $(interactionClassIdSelect).empty();

    var interactionTypeId = $(interactionTypeIdSelect).val();

    $.ajax({
        url: "/Api/InteractionClass/InteractionType/" + interactionTypeId,
        method: "GET",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("ASPNETCRMAccessToken")
        },
        success: function (data) {
            $.each(data, function () {
                $(interactionClassIdSelect).append("<option value='" + this.Id + "'>" + this.Name + "</option>");
            });

            if (data.length == 0) {
                $("#alerts").load("/PartialView/GetPartialView", { partialViewName: "_GlobalWarningMessageForAsyncRequestsPartial" }, function (data) {
                    $("#alerts").html(data);
                    addTextToWarningMessageDiv("There aren't any Classes for selected Type.");
                    warningAlertAsyncPopup();
                    clearAlertsDiv();
                });
            };
        },
        error: function (errorData) {
            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalErrorMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                var errorMessage = errorData.responseJSON.Message ? "Something gone wrong" : errorData.responseJSON;
                addTextToErrorMessageDiv(errorMessage);
                errorAlertAsyncPopup();
                clearAlertsDiv();
            });
        }
    });
});