﻿var deleteInteractionAsyncForm = "#deleteInteractionAsync";
var deleteModalDiv = "#deleteModal";
var tableRows = "#interactionTable tr";
var tableMessagesDiv = "#tableMessages";
var alertsDiv = "#alerts";

$(deleteInteractionAsyncForm).submit(function (e) {
    e.preventDefault();

    var deleteInteractionId = $(deleteInteractionIdInput).val();

    $.ajax({
        url: "/Api/Interaction/" + deleteInteractionId,
        method: "DELETE",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("ASPNETCRMAccessToken")
        },
        success: function (data) {
            $("#interaction_" + deleteInteractionId).remove();

            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalSuccessMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                addTextToSuccessMessageDiv("Interaction correctly deleted.");
                successAlertAsyncPopup();
                clearAlertsDiv();
            });

            if ($(tableRows).length == 1) {
                $(tableMessagesDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalNoDataToDisplayPartial" }, function (data) {
                    $(tableMessagesDiv).html(data);
                });
            }
        },
        error: function (errorData) {
            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalErrorMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                var errorMessage = errorData.responseJSON.Message ? "Something gone wrong" : errorData.responseJSON;
                addTextToErrorMessageDiv(errorMessage);
                errorAlertAsyncPopup();
                clearAlertsDiv();
            });

        }
    })

    $(deleteModalDiv).modal('hide');
});