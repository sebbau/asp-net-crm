﻿var deleteInteractionIdInput = "#deleteInteractionId";
var deleteInteractionModalClass = ".deleteModal";

$(deleteInteractionModalClass).on("click", function () {
    var interactionId = $(this).data("id");
    $(deleteInteractionIdInput).val(interactionId);
});