﻿var deleteNoteIdInput = "#deleteNoteId";
var deleteNoteModalClass = ".deleteModal";

$(deleteNoteModalClass).on("click", function () {
    var noteId = $(this).data("id");
    $(deleteNoteIdInput).val(noteId);
});