﻿var deleteUserAsyncForm = "#deleteUserAsync";
var deleteModalDiv = "#deleteModal";
var tableRows = "#userTable tr";
var tableMessagesDiv = "#tableMessages";
var alertsDiv = "#alerts";

$(deleteUserAsyncForm).submit(function (e) {
    e.preventDefault();

    var deleteUserId = $(deleteUserIdInput).val();

    $.ajax({
        url: "/Api/User/" + deleteUserId,
        method: "DELETE",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("ASPNETCRMAccessToken")
        },
        success: function (data) {
            $("#user_" + deleteUserId).remove();

            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalSuccessMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                addTextToSuccessMessageDiv("User correctly deleted.");
                successAlertAsyncPopup();
                clearAlertsDiv();
            });

            if ($(tableRows).length == 1) {
                $(tableMessagesDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalNoDataToDisplayPartial" }, function (data) {
                    $(tableMessagesDiv).html(data);
                });
            }
        },
        error: function (errorData) {
            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalErrorMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                var errorMessage = errorData.responseJSON.Message ? "Something gone wrong" : errorData.responseJSON;
                addTextToErrorMessageDiv(errorMessage);
                errorAlertAsyncPopup();
                clearAlertsDiv();
            });
        }
    })

    $(deleteModalDiv).modal('hide');
});