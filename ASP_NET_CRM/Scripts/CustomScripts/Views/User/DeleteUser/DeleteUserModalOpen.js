﻿var deleteUserIdInput = "#deleteUserId";
var deleteUserModalClass = ".deleteModal";

$(deleteUserModalClass).on("click", function () {
    var userId = $(this).data("id");
    $(deleteUserIdInput).val(userId);
});