﻿var clientTypeChoiceInput = "#clientTypeChoice";

var individualClientForm = "#individualClient";
var businessClientForm = "#businessClient";

var individualClientClientTypeInput = "#IndividualClient_ClientType";
var individualClientFirstNameInput = "#FirstName";
var individualClientSurnameInput = "#Surname";
var individualClientNameInput = "#IndividualClient_Name";
var individualClientDocumentTypeSelect = "#DocumentType";

var businessClientClientTypeInput = "#BusinessClient_ClientType";
var businessClientNameInput = "#BusinessClient_Name";

$(document).ready(function () {
    $(".datepicker").datepicker({
        dateFormat: "dd/mm/yy"
    });

    var selectedOption = $("#clientTypeChoice option:selected").val();

    if (selectedOption == 1) {
        $(":input .businessClient", businessClientForm).val(null);
        $("#clientTypeChoice option:contains('Select Client Type')").remove();
        $(businessClientForm).hide();
        $(individualClientForm).show();
    }

    else if (selectedOption == 2) {
        $(":input .individualClient", individualClientForm).val(null);
        $("#clientTypeChoice option:contains('Select Client Type')").remove();
        $(individualClientForm).hide();
        $(businessClientForm).show();
    }

    else {
        $(individualClientForm).hide();
        $(businessClientForm).hide();
    }
});

$(individualClientDocumentTypeSelect).on("change", function () {
    $("#DocumentType option[value='0']").remove();
});

$("#clientTypeChoice").on("change", function (event) {
    $("#clientTypeChoice option:contains('Select Client Type')").remove();

    var selectedOption = $("#clientTypeChoice option:selected").val();

    if (selectedOption == 1) {
        $(businessClientForm).hide();
        $(businessClientForm).trigger('reset');
        $(individualClientForm).show();

        $(individualClientClientTypeInput).val(selectedOption);
    }

    else if (selectedOption == 2) {
        $(individualClientForm).hide();
        $(individualClientForm).trigger('reset');
        $(businessClientForm).show();

        $(businessClientClientTypeInput).val(selectedOption);
    }

    else {
        $(individualClientForm).hide();
        $(businessClientForm).hide();
    }
});

$(individualClientForm).on("submit", function (event) {
    $(individualClientNameInput).val($(individualClientFirstNameInput).val() + " " + $(individualClientSurnameInput).val())
})