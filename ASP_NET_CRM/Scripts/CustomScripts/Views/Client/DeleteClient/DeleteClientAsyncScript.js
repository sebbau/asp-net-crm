﻿var deleteClientAsyncForm = "#deleteClientAsync";
var deleteModalDiv = "#deleteModal";
var tableRows = "#clientTable tr";
var tableMessagesDiv = "#tableMessages";
var alertsDiv = "#alerts";

$(deleteClientAsyncForm).submit(function (e) {
    e.preventDefault();

    var deleteClientId = $(deleteClientIdInput).val();

    $.ajax({
        url: "/Api/Client/" + deleteClientId,
        method: "DELETE",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("ASPNETCRMAccessToken")
        },
        success: function (data) {
            $("#client_" + deleteClientId).remove();

            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalSuccessMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                addTextToSuccessMessageDiv("Client correctly deleted.");
                successAlertAsyncPopup();
                clearAlertsDiv();
            });

            if ($(tableRows).length == 1) {
                $(tableMessagesDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalNoDataToDisplayPartial" }, function (data) {
                    $(tableMessagesDiv).html(data);
                });
            }
        },
        error: function (errorData) {
            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalErrorMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                var errorMessage = errorData.responseJSON.Message ? "Something gone wrong" : errorData.responseJSON;
                addTextToErrorMessageDiv(errorMessage);
                errorAlertAsyncPopup();
                clearAlertsDiv();
            });          
        }
    })

    $(deleteModalDiv).modal('hide');
});