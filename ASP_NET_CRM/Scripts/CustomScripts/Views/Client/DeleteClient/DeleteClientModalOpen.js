﻿var deleteClientIdInput = "#deleteClientId";
var deleteClientModalClass = ".deleteModal";

$(deleteClientModalClass).on("click", function () {
    var clientId = $(this).data("id");
    $(deleteClientIdInput).val(clientId);
});