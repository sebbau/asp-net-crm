﻿var individualClientUpdateForm = "#individualClientUpdateForm";
var individualClientFirstNameInput = "#FirstName";
var individualClientSurnameInput = "#Surname";
var individualClientNameInput = "#Name";

$(document).ready(function () {
    $(".datepicker").datepicker({
        dateFormat: "dd/mm/yy"
    });
});

$(individualClientUpdateForm).on("submit", function (event) {
    $(individualClientNameInput).val($(individualClientFirstNameInput).val() + " " + $(individualClientSurnameInput).val())
});