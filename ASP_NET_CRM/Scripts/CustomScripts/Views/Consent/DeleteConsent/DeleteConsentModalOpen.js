﻿var deleteConsentIdInput = "#deleteConsentId";
var deleteConsentModalClass = ".deleteModal";

$(deleteConsentModalClass).on("click", function () {
    var consentId = $(this).data("id");
    $(deleteConsentIdInput).val(consentId);
});