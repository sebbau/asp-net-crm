﻿var deleteInteractionClassIdInput = "#deleteInteractionClassId";
var deleteInteractionClassModalClass = ".deleteModal";

$(deleteInteractionClassModalClass).on("click", function () {
    var interactionClassId = $(this).data("id");
    $(deleteInteractionClassIdInput).val(interactionClassId);
});