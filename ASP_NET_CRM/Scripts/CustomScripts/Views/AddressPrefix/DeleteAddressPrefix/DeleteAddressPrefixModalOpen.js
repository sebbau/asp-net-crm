﻿var deleteAddressPrefixIdInput = "#deleteAddressPrefixId";
var deleteAddressPrefixModalClass = ".deleteModal";

$(deleteAddressPrefixModalClass).on("click", function () {
    var addressPrefixId = $(this).data("id");
    $(deleteAddressPrefixIdInput).val(addressPrefixId);
});