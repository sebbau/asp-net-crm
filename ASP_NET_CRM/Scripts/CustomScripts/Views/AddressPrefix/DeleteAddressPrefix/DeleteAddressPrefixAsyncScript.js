﻿var deleteAddressPrefixAsyncForm = "#deleteAddressPrefixAsync";
var deleteModalDiv = "#deleteModal";
var tableRows = "#addressPrefixTable tr";
var tableMessagesDiv = "#tableMessages";
var alertsDiv = "#alerts";

$(deleteAddressPrefixAsyncForm).submit(function (e) {
    e.preventDefault();

    var deleteAddressPrefixId = $(deleteAddressPrefixIdInput).val();

    $.ajax({
        url: "/Api/AddressPrefix/" + deleteAddressPrefixId,
        method: "DELETE",
        headers: {
            "Authorization": "Bearer " + localStorage.getItem("ASPNETCRMAccessToken")
        },
        success: function (data) {
            $("#addressPrefix_" + deleteAddressPrefixId).remove();

            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalSuccessMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                addTextToSuccessMessageDiv("Address Prefix correctly deleted.");
                successAlertAsyncPopup();
                clearAlertsDiv();
            });

            if ($(tableRows).length == 1) {
                $(tableMessagesDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalNoDataToDisplayPartial" }, function (data) {
                    $(tableMessagesDiv).html(data);
                });
            }
        },
        error: function (errorData) {
            $(alertsDiv).load("/PartialView/GetPartialView", { partialViewName: "_GlobalErrorMessagePartial" }, function (data) {
                $(alertsDiv).html(data);
                var errorMessage = errorData.responseJSON.Message ? "Something gone wrong" : errorData.responseJSON;
                addTextToErrorMessageDiv(errorMessage);
                errorAlertAsyncPopup();
                clearAlertsDiv();
            });

        }
    })

    $(deleteModalDiv).modal('hide');
});