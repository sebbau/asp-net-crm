﻿var deleteInteractionTypeIdInput = "#deleteInteractionTypeId";
var deleteInteractionTypeModalClass = ".deleteModal";

$(deleteInteractionTypeModalClass).on("click", function () {
    var interactionTypeId = $(this).data("id");
    $(deleteInteractionTypeIdInput).val(interactionTypeId);
});