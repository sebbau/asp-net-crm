﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ASP_NET_CRM.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ClientTypeEnum
    {
        [EnumMember(Value = "Klient Indywidualny")]
        [Display(Name = "Klient Indywidualny")]
        IndividualClient = 1,

        [EnumMember(Value = "Klient Biznesowy")]
        [Display(Name = "Klient Biznesowy")]
        BusinessClient = 2
    }
}