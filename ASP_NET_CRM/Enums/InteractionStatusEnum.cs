﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Enums
{
    public enum InteractionStatusEnum
    {
        [Display(Name = "New")]
        New = 1,

        [Display(Name = "In Progress")]
        InProgress = 2,

        [Display(Name = "Canceled")]
        Canceled = 3,

        [Display(Name = "Closed")]
        Closed = 4
    }
}