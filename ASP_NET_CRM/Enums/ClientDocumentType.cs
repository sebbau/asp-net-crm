﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Enums
{
    public enum ClientDocumentType
    {
        [Display(Name = "Dowód Osobisty")]
        IdCard = 1,

        [Display(Name = "Paszport")]
        Passport,

        [Display(Name = "Inne")]
        Other
    }
}