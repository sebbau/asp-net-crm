﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Enums
{
    public enum AddressStateEnum
    {
        [Display(Name = "Warmińsko - Mazurskie")]
        WarmiaMasuria = 1,

        [Display(Name = "Pomorskie")]
        Pomerania,

        [Display(Name = "Zachodnio - Pomorskie")]
        WestPomerania,

        [Display(Name = "Lubuskie")]
        LubuszProvince,

        [Display(Name = "Wielkopolskie")]
        GreaterPoland,

        [Display(Name = "Kujawsko - Pomorskie")]
        KuyaviaPomerania,

        [Display(Name = "Mazowieckie")]
        Masovia,

        [Display(Name = "Podlaskie")]
        PodlasieProvince,

        [Display(Name = "Łódzkie")]
        LodzProvince,

        [Display(Name = "Dolnośląskie")]
        LowerSilesia,

        [Display(Name = "Opolskie")]
        OpoleProvince,

        [Display(Name = "Śląskie")]
        Silesia,

        [Display(Name = "Małopolskie")]
        LesserPoland,

        [Display(Name = "Świętokrzyskie")]
        HolyCross,

        [Display(Name = "Podkarpackie")]
        Subcarpathia,

        [Display(Name = "Lubelskie")]
        LublinProvince
    }
}