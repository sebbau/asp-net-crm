﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Enums
{
    public enum AddressType
    {
        [Display(Name = "Siedziby/Zameldowania")]
        Main = 1,

        [Display(Name = "Korespondencyjny")]
        Correspondence,

        [Display(Name = "Inne")]
        Other
    }
}