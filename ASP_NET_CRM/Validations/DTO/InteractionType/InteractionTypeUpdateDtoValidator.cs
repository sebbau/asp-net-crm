﻿using ASP_NET_CRM.DTO.InteractionType;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.InteractionType
{
    public class InteractionTypeUpdateDtoValidator : AbstractValidator<InteractionTypeUpdateDto>
    {
        public InteractionTypeUpdateDtoValidator()
        {
            RuleFor(ap => ap.Id).NotEmpty();
            RuleFor(ap => ap.Name).NotEmpty();
            RuleFor(ap => ap.Name).MaximumLength(50);
        }
    }
}