﻿using ASP_NET_CRM.DTO.Interaction;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.Interaction
{
    public class InteractionUpdateDtoValidator : AbstractValidator<InteractionUpdateDto>
    {
        public InteractionUpdateDtoValidator()
        {
            RuleFor(i => i.Id).NotEmpty();
            RuleFor(i => i.Status).IsInEnum();
            RuleFor(i => i.Description).MaximumLength(2000);
        }
    }
}