﻿using ASP_NET_CRM.Constants.Validations.Messages.Interaction;
using ASP_NET_CRM.DTO.Interaction;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.Interaction
{
    public class InteractionSaveDtoValidator : AbstractValidator<InteractionSaveDto>
    {
        public InteractionSaveDtoValidator()
        {
            RuleFor(i => i.ClientId).NotEmpty().WithMessage(InteractionValidationMessages.clientIdNotEmptyValidationMessage);
            RuleFor(i => i.TypeId).NotEmpty().WithMessage(InteractionValidationMessages.interactionTypeIdNotEmptyValidationMessage);
            RuleFor(i => i.ClassId).NotEmpty().WithMessage(InteractionValidationMessages.interactionClassIdNotEmptyValidationMessage);
            RuleFor(i => i.InteractionDate).NotEmpty();
            RuleFor(i => i.Description).MaximumLength(2000);
        }
    }
}