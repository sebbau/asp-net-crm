﻿using ASP_NET_CRM.Constants.Validations.Messages.Client;
using ASP_NET_CRM.Constants.Validations.RegularExpressions.Client;
using ASP_NET_CRM.DTO.Client;
using ASP_NET_CRM.Enums;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.Client
{
    public class ClientSaveDtoValidator : AbstractValidator<ClientSaveDto>
    {
        public ClientSaveDtoValidator()
        {
            // All types of Client Validations

            RuleFor(c => c.ClientType).IsInEnum();
            RuleFor(c => c.PhoneNumber).NotEmpty();
            RuleFor(c => c.PhoneNumber).Matches(ClientValidationRegularExpressions.PhoneNumberRegEx).WithMessage(ClientValidationMessages.PhoneNumberRegExValidationMessage);
            RuleFor(c => c.Email).EmailAddress();
            RuleFor(c => c.Email).MaximumLength(200);

            // Individual Client Validations

            RuleFor(c => c.FirstName).NotEmpty().When(c => c.ClientType == ClientTypeEnum.IndividualClient);
            RuleFor(c => c.FirstName).Length(3, 50);
            RuleFor(c => c.FirstName).Matches(ClientValidationRegularExpressions.firstNameRegEx).WithMessage(ClientValidationMessages.FirstNameRegExValidationMessage);
            RuleFor(c => c.Surname).NotEmpty().When(c => c.ClientType == ClientTypeEnum.IndividualClient);
            RuleFor(c => c.Surname).Matches(ClientValidationRegularExpressions.surnameRegEx).WithMessage(ClientValidationMessages.SurnameRegExValidationMessage);
            RuleFor(c => c.Pesel).NotEmpty().When(c => c.ClientType == ClientTypeEnum.IndividualClient);
            RuleFor(c => c.Pesel).Matches(ClientValidationRegularExpressions.peselRegEx).WithMessage(ClientValidationMessages.PeselRegExValidationMessage);
            RuleFor(c => c.Name).Length(6, 201).When(c => c.ClientType == ClientTypeEnum.IndividualClient && !String.IsNullOrEmpty(c.FirstName) && !String.IsNullOrEmpty(c.Surname) && IndividualClientNameValidation(c.FirstName, c.Surname));
            RuleFor(c => c.Name).Equal(c => c.FirstName + " " + c.Surname).When(c => c.ClientType == ClientTypeEnum.IndividualClient && !String.IsNullOrEmpty(c.FirstName) && !String.IsNullOrEmpty(c.Surname) && IndividualClientNameValidation(c.FirstName, c.Surname)).WithMessage(ClientValidationMessages.IndividualClientNameValidationMessage);
            RuleFor(c => c.NIP).Empty().When(c => c.ClientType == ClientTypeEnum.IndividualClient);
            RuleFor(c => c.DocumentType).IsInEnum().When(c => c.ClientType == ClientTypeEnum.IndividualClient && c.DocumentType.GetValueOrDefault() != 0);
            RuleFor(c => c.DocumentType).IsInEnum().When(c => !String.IsNullOrEmpty(c.DocumentNumber) || !String.IsNullOrEmpty(c.DocumentIssuedBy) || c.DocumentIssueDate.HasValue).WithMessage(ClientValidationMessages.DocumentTypeNotEmptyValidationMessage);
            RuleFor(c => c.DocumentNumber).MaximumLength(15);
            RuleFor(c => c.DocumentNumber).NotEmpty().When(c => c.DocumentType.GetValueOrDefault() != 0 || !String.IsNullOrEmpty(c.DocumentIssuedBy) || c.DocumentIssueDate.HasValue).WithMessage(ClientValidationMessages.DocumentNumberNotEmptyValidationMessage);            
            RuleFor(c => c.DocumentIssueDate).NotEmpty().When(c => c.DocumentType.GetValueOrDefault() != 0 || !String.IsNullOrEmpty(c.DocumentIssuedBy) || !String.IsNullOrEmpty(c.DocumentNumber)).WithMessage(ClientValidationMessages.DocumentIssueDateNotEmptyValidationMessage);
            RuleFor(c => c.DocumentIssuedBy).MaximumLength(200);
            RuleFor(c => c.DocumentIssuedBy).NotEmpty().When(c => c.DocumentType.GetValueOrDefault() != 0 || c.DocumentIssueDate.HasValue || !String.IsNullOrEmpty(c.DocumentNumber)).WithMessage(ClientValidationMessages.DocumentIssuedByNotEmptyValidationMessage);
            RuleFor(c => c.Regon).Empty().When(c => c.ClientType == ClientTypeEnum.IndividualClient);
            RuleFor(c => c.KRS).Empty().When(c => c.ClientType == ClientTypeEnum.IndividualClient);

            // Business Client Validations

            RuleFor(c => c.Name).NotEmpty().When(c => c.ClientType == ClientTypeEnum.BusinessClient);
            RuleFor(c => c.Name).Length(2, 250).When(c => c.ClientType == ClientTypeEnum.BusinessClient);
            RuleFor(c => c.NIP).NotEmpty().When(c => c.ClientType == ClientTypeEnum.BusinessClient);
            RuleFor(c => c.NIP).Matches(ClientValidationRegularExpressions.nipRegEx).WithMessage(ClientValidationMessages.NIPRegExValidationMessage);
            RuleFor(c => c.FirstName).Empty().When(c => c.ClientType == ClientTypeEnum.BusinessClient);
            RuleFor(c => c.Surname).Empty().When(c => c.ClientType == ClientTypeEnum.BusinessClient);
            RuleFor(c => c.Pesel).Empty().When(c => c.ClientType == ClientTypeEnum.BusinessClient);
            RuleFor(c => c.DocumentNumber).Empty().When(c => c.ClientType == ClientTypeEnum.BusinessClient);
            RuleFor(c => c.DocumentIssueDate).Empty().When(c => c.ClientType == ClientTypeEnum.BusinessClient);
            RuleFor(c => c.DocumentIssuedBy).Empty().When(c => c.ClientType == ClientTypeEnum.BusinessClient);
            RuleFor(c => c.Regon).Matches(ClientValidationRegularExpressions.RegonRegEx).WithMessage(ClientValidationMessages.RegonRegExValidationMessage);
            RuleFor(c => c.KRS).Matches(ClientValidationRegularExpressions.KRSRegEx).WithMessage(ClientValidationMessages.KRSRegExValidationMessage);
            RuleFor(c => c.WebSite).MaximumLength(250);
        }

        private bool IndividualClientNameValidation(string firstName, string surname)
        {
            if (Regex.IsMatch(firstName, ClientValidationRegularExpressions.firstNameRegEx) && Regex.IsMatch(surname, ClientValidationRegularExpressions.surnameRegEx))
                return true;
            else
                return false;
        }
    }
}