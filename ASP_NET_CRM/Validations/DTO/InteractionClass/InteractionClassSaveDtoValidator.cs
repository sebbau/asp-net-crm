﻿using ASP_NET_CRM.DTO.InteractionClass;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.InteractionClass
{
    public class InteractionClassSaveDtoValidator : AbstractValidator<InteractionClassSaveDto>
    {
        public InteractionClassSaveDtoValidator()
        {
            RuleFor(ic => ic.Name).NotEmpty();
            RuleFor(ic => ic.Name).MaximumLength(50);
            RuleFor(ic => ic.InteractionTypeId).NotEmpty();
        }
    }
}