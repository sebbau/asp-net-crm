﻿using ASP_NET_CRM.Constants.Validations.Messages.Address;
using ASP_NET_CRM.Constants.Validations.RegularExpressions.Address;
using ASP_NET_CRM.DTO.Address;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.Address
{
    public class AddressSaveDtoValidator : AbstractValidator<AddressSaveDto>
    {
        public AddressSaveDtoValidator()
        {
            RuleFor(a => a.Type).IsInEnum().WithMessage(AddressValidationMessages.TypeRequiredValidationMessage);
            RuleFor(a => a.PrefixId).Empty().When(a => String.IsNullOrEmpty(a.StreetName)).WithMessage(AddressValidationMessages.PrefixEmptyWhenStreetNameEmptyValidationMessage);
            RuleFor(a => a.PrefixId).NotEmpty().When(a => !String.IsNullOrEmpty(a.StreetName)).WithMessage(AddressValidationMessages.PrefixNotEmptyWhenStreetNameNotEmptyValidationMessage);
            RuleFor(a => a.StreetName).Matches(AddressValidationRegularExpressions.StreetNameRegEx).WithMessage(AddressValidationMessages.StreetNameRegExValidationMessage).When(a => !String.IsNullOrEmpty(a.StreetName));
            RuleFor(a => a.StreetName).MaximumLength(200);
            RuleFor(a => a.HouseNumber).NotEmpty();
            RuleFor(a => a.HouseNumber).Matches(AddressValidationRegularExpressions.HouseNumberRegEx).WithMessage(AddressValidationMessages.HouseNumberRegExValidationMessage);
            RuleFor(a => a.HouseNumber).MaximumLength(6);
            RuleFor(a => a.ApartmentNumber).Matches(AddressValidationRegularExpressions.ApartmentNumberRegEx).WithMessage(AddressValidationMessages.ApartmentNumberRegExValidationMessage).When(a => !String.IsNullOrEmpty(a.ApartmentNumber));
            RuleFor(a => a.ApartmentNumber).MaximumLength(10);
            RuleFor(a => a.FullAddressName).NotEmpty().When(a => !String.IsNullOrEmpty(a.HouseNumber) && !String.IsNullOrEmpty(a.City));
            RuleFor(a => a.FullAddressName).Equal(a => a.PrefixValue + " " + a.StreetName + " " + a.HouseNumber + "/" + a.ApartmentNumber + ", " + a.City).When(a => !String.IsNullOrEmpty(a.StreetName) && !String.IsNullOrEmpty(a.ApartmentNumber)).WithMessage(AddressValidationMessages.FullAddressNameInvalidValidationMessage);
            RuleFor(a => a.FullAddressName).Equal(a => a.City + " " + a.HouseNumber + "/" + a.ApartmentNumber).When(a => String.IsNullOrEmpty(a.StreetName) && !String.IsNullOrEmpty(a.ApartmentNumber)).WithMessage(AddressValidationMessages.FullAddressNameInvalidValidationMessage);
            RuleFor(a => a.FullAddressName).Equal(a => a.PrefixValue + " " + a.StreetName + " " + a.HouseNumber + ", " + a.City).When(a => !String.IsNullOrEmpty(a.StreetName) && String.IsNullOrEmpty(a.ApartmentNumber)).WithMessage(AddressValidationMessages.FullAddressNameInvalidValidationMessage);
            RuleFor(a => a.FullAddressName).Equal(a => a.City + " " + a.HouseNumber).When(a => String.IsNullOrEmpty(a.StreetName) && String.IsNullOrEmpty(a.ApartmentNumber)).WithMessage(AddressValidationMessages.FullAddressNameInvalidValidationMessage);
            RuleFor(a => a.FullAddressName).MaximumLength(230);
            RuleFor(a => a.PostalCode).NotEmpty();
            RuleFor(a => a.PostalCode).Matches(AddressValidationRegularExpressions.PostalCodeRegEx).WithMessage(AddressValidationMessages.PostalCodeRegExValidationMessage);
            RuleFor(a => a.City).NotEmpty();
            RuleFor(a => a.City).Matches(AddressValidationRegularExpressions.CityRegEx).WithMessage(AddressValidationMessages.CityRegExValidationMessage);
            RuleFor(a => a.State).IsInEnum().WithMessage(AddressValidationMessages.StateRequiredValidationMessage);
            RuleFor(a => a.PrefixValue).NotEmpty().When(a => !String.IsNullOrEmpty(a.StreetName) && (a.PrefixId != null && a.PrefixId != 0));
            RuleFor(a => a.ClientId).NotEmpty().WithMessage(AddressValidationMessages.ClientIdNotEmptyValidationMessage);
        }
    }
}