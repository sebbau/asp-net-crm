﻿using ASP_NET_CRM.DTO.Note;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.Note
{
    public class NoteUpdateDtoValidator : AbstractValidator<NoteUpdateDto>
    {
        public NoteUpdateDtoValidator()
        {
            RuleFor(n => n.Id).NotEmpty();
            RuleFor(n => n.Title).NotEmpty();
            RuleFor(n => n.Title).MaximumLength(200);
            RuleFor(n => n.Description).MaximumLength(2000);
        }
    }
}