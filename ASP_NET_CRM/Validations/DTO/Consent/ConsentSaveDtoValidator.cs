﻿using ASP_NET_CRM.DTO.Consent;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.Consent
{
    public class ConsentSaveDtoValidator : AbstractValidator<ConsentSaveDto>
    {
        public ConsentSaveDtoValidator()
        {
            RuleFor(c => c.Name).NotEmpty();
            RuleFor(c => c.Name).MaximumLength(50);
            RuleFor(c => c.FullName).NotEmpty();
            RuleFor(c => c.FullName).MaximumLength(250);
        }
    }
}