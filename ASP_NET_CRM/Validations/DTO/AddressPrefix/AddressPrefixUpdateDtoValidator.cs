﻿using ASP_NET_CRM.DTO.AddressPrefix;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.AddressPrefix
{
    public class AddressPrefixUpdateDtoValidator : AbstractValidator<AddressPrefixUpdateDto>
    {
        public AddressPrefixUpdateDtoValidator()
        {
            RuleFor(ap => ap.Id).NotEmpty();
            RuleFor(ap => ap.Name).NotEmpty();
            RuleFor(ap => ap.Name).MaximumLength(10);
        }
    }
}