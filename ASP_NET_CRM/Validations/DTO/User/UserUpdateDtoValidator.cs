﻿using ASP_NET_CRM.Constants.Validations.Messages.User;
using ASP_NET_CRM.Constants.Validations.RegularExpressions.User;
using ASP_NET_CRM.DTO.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.User
{
    public class UserUpdateDtoValidator : AbstractValidator<UserUpdateDto>
    {
        public UserUpdateDtoValidator()
        {
            RuleFor(u => u.Id).NotEmpty();
            RuleFor(u => u.FirstName).NotEmpty();
            RuleFor(u => u.FirstName).Length(3, 50);
            RuleFor(u => u.FirstName).Matches(UserValidationRegularExpressions.FirstNameRegEx).WithMessage(UserValidationMessages.FirstNameRegExValidationMessage);
            RuleFor(u => u.Surname).NotEmpty();
            RuleFor(u => u.Surname).Length(2, 150);
            RuleFor(u => u.Surname).Matches(UserValidationRegularExpressions.SurnameRegEx).WithMessage(UserValidationMessages.SurnameRegExValidationMessage);
            RuleFor(u => u.FullName).NotEmpty().When(u => !String.IsNullOrEmpty(u.FirstName) && !String.IsNullOrEmpty(u.Surname));
            RuleFor(u => u.FullName).Equal(u => u.FirstName + " " + u.Surname).When(u => !String.IsNullOrEmpty(u.FirstName) && !String.IsNullOrEmpty(u.Surname));
            RuleFor(u => u.FullName).Length(6, 201).When(u => !String.IsNullOrEmpty(u.FirstName) && !String.IsNullOrEmpty(u.Surname));
        }
    }
}