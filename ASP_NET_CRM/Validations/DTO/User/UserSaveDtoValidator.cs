﻿using ASP_NET_CRM.Constants.Validations.Messages.User;
using ASP_NET_CRM.Constants.Validations.RegularExpressions.User;
using ASP_NET_CRM.DTO.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.User
{
    public class UserSaveDtoValidator : AbstractValidator<UserSaveDto>
    {
        public UserSaveDtoValidator()
        {
            RuleFor(u => u.Email).NotEmpty();
            RuleFor(u => u.Email).EmailAddress();
            RuleFor(u => u.FirstName).NotEmpty();
            RuleFor(u => u.FirstName).Length(3, 50);
            RuleFor(u => u.FirstName).Matches(UserValidationRegularExpressions.FirstNameRegEx).WithMessage(UserValidationMessages.FirstNameRegExValidationMessage);
            RuleFor(u => u.Surname).NotEmpty();
            RuleFor(u => u.Surname).Length(2, 150);
            RuleFor(u => u.Surname).Matches(UserValidationRegularExpressions.SurnameRegEx).WithMessage(UserValidationMessages.SurnameRegExValidationMessage);
            RuleFor(u => u.FullName).NotEmpty().When(u => !String.IsNullOrEmpty(u.FirstName) && !String.IsNullOrEmpty(u.Surname));
            RuleFor(u => u.FullName).Equal(u => u.FirstName + " " + u.Surname).When(u => !String.IsNullOrEmpty(u.FirstName) && !String.IsNullOrEmpty(u.Surname));
            RuleFor(u => u.FullName).Length(6, 201).When(u => !String.IsNullOrEmpty(u.FirstName) && !String.IsNullOrEmpty(u.Surname));
            RuleFor(u => u.Password).NotEmpty();
            RuleFor(u => u.Password).Matches(UserValidationRegularExpressions.PasswordRegEx).WithMessage(UserValidationMessages.PasswordRegExValidationMessage);
            RuleFor(u => u.Password).Length(6, 100);
            RuleFor(u => u.ConfirmPassword).NotEmpty().When(u => !String.IsNullOrEmpty(u.Password));
            RuleFor(u => u.ConfirmPassword).Equal(u => u.Password).When(u => !String.IsNullOrEmpty(u.Password)).WithMessage(UserValidationMessages.ConfirmPasswordEqualToPasswordValidationMessage);
        }
    }
}