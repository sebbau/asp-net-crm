﻿using ASP_NET_CRM.Constants.Validations.Messages.User;
using ASP_NET_CRM.Constants.Validations.RegularExpressions.User;
using ASP_NET_CRM.DTO.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.User
{
    public class ResetPasswordDtoValidator : AbstractValidator<ResetPasswordDto>
    {
        public ResetPasswordDtoValidator()
        {
            RuleFor(u => u.Email).NotEmpty();
            RuleFor(u => u.Email).EmailAddress();
            RuleFor(u => u.Password).NotEmpty();
            RuleFor(u => u.Password).Matches(UserValidationRegularExpressions.PasswordRegEx).WithMessage(UserValidationMessages.PasswordRegExValidationMessage);
            RuleFor(u => u.Password).Length(6, 100);
            RuleFor(u => u.ConfirmPassword).NotEmpty().When(u => !String.IsNullOrEmpty(u.Password));
            RuleFor(u => u.ConfirmPassword).Equal(u => u.Password).When(u => !String.IsNullOrEmpty(u.Password)).WithMessage(UserValidationMessages.ConfirmPasswordEqualToPasswordValidationMessage);
        }
    }
}