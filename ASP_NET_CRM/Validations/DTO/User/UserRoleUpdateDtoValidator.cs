﻿using ASP_NET_CRM.DTO.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_NET_CRM.Validations.DTO.User
{
    public class UserRoleUpdateDtoValidator : AbstractValidator<UserRoleUpdateDto>
    {
        public UserRoleUpdateDtoValidator()
        {
            RuleFor(ur => ur.Id).NotEmpty();
            RuleFor(ur => ur.Name).NotEmpty();
            RuleFor(ur => ur.Name).MaximumLength(256);
            RuleFor(ur => ur.Selected).NotNull();
        }
    }
}